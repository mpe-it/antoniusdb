from django.db import models
from .base_model import BaseModel
from .storage_models import StorageLocation, StorageSublocation, StorageDrawer
from .vendor_models import Manufacturer, Supplier


class Tool(BaseModel):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)
    manufacturer = models.ForeignKey(Manufacturer,on_delete=models.SET_NULL, null=True, blank=True, related_name='tool_manufacturer')
    suppliers = models.ManyToManyField(Supplier, related_name='tools',blank=True, verbose_name='Suppliers')
    location = models.ForeignKey(StorageLocation, on_delete=models.SET_NULL, null=True, blank=True, related_name='tool_location',)
    sublocation = models.ForeignKey(StorageSublocation, on_delete=models.SET_NULL, null=True, blank=True, related_name='tool_sublocation',)
    drawer = models.ForeignKey(StorageDrawer, on_delete=models.SET_NULL, null=True, blank=True, related_name='tool_drawer',)
    borrowed_by = models.CharField(max_length=100, blank=True, null=True)
    return_by = models.DateField(blank=True,null=True)
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Tool"
        verbose_name_plural = "Tools"
        
    def __str__(self):
        return f'{self.name}'
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Tool, self).save(*args, **kwargs)