from django.db import models

class BaseModel(models.Model):
    """
    Abstract base model with value, label, and info methods
    that can be overridden by subclasses.
    """

    class Meta:
        abstract = True

    def value(self):
        """Default value method, can be overridden in subclasses."""
        return str(self)
    
    def label(self):
        description = self.description if self.description else ""
        
        description = (description[:20] + '...') if len(description) > 20 else description
        return f'{self.name} - {description}' if description else self.name
    
    def info(self):
        return self.description or "No Description Available"
