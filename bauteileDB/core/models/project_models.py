from django.db import models
from .storage_models import StorageLocation, StorageSublocation
from .base_model import BaseModel

class Project(BaseModel):
    name = models.CharField(max_length=255, unique=True)#, verbose_name="Project Name"
    description = models.TextField(blank=True, null=True, verbose_name="Project Description")
    modified_by = models.CharField(max_length=100, blank=True, null=True)


    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Project, self).save(*args, **kwargs)



class Subproject(BaseModel):
    name = models.CharField(max_length=255)#, verbose_name="Subproject Name"
    description = models.TextField(blank=True, null=True, verbose_name="Subproject Description")
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='subprojects', verbose_name="Related Project")
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Subproject"
        verbose_name_plural = "Subprojects"
        unique_together = ['name', 'project']

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Subproject, self).save(*args, **kwargs)



class Assembly(BaseModel):
    name = models.CharField(max_length=255)#, verbose_name="Assembly Name"
    description = models.TextField(blank=True, null=True, verbose_name="Assembly Description")    
    subproject = models.ForeignKey(Subproject, on_delete=models.CASCADE, related_name='subprojects', verbose_name="Related Subproject")
    supervisor = models.CharField(max_length=255, blank=True, null=True, verbose_name="Supervisor")
    storage_location = models.ForeignKey(StorageLocation, on_delete=models.SET_NULL, null=True, related_name='locations', verbose_name="Related Location")
    storage_sublocation = models.ForeignKey(StorageSublocation, on_delete=models.SET_NULL, null=True, related_name='sublocation', verbose_name='Related Sublocation')
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Assembly"
        verbose_name_plural = "Assemblies"
        unique_together = ['name', 'subproject'] 

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Assembly, self).save(*args, **kwargs)
