from django.db import models
from django.forms import ValidationError
from .vendor_models import Manufacturer, Supplier
from .project_models import Project, Subproject, Assembly
from .storage_models import StorageLocation, StorageSublocation, StorageDrawer
from django.db.models import Q
from .base_model import BaseModel
import os
from django.dispatch import receiver
from django.utils.text import slugify
from django.db.models.signals import post_delete
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Unit(BaseModel):
    unit_of_measurement = models.CharField(max_length=255, null=False, blank=False, verbose_name='Unit of Measurement')
    description = models.TextField(blank=True, null=True, verbose_name="Description")

    class Meta:
        verbose_name = "Unit"
        verbose_name_plural = "Units"

    def __str__(self):
        return f"{self.unit_of_measurement}"


class Document(BaseModel):
    file = models.FileField(upload_to='uploads/documents/', verbose_name='Document File')
    description = models.CharField(max_length=255, blank=True, null=True, verbose_name='File Description')
    uploaded_at = models.DateTimeField(auto_now_add=True, verbose_name='Uploaded At')

    def __str__(self):
        return self.file.name


@receiver(post_delete, sender=Document)
def delete_document_file(sender, instance, **kwargs):
    if instance.file and os.path.isfile(instance.file.path):
        os.remove(instance.file.path)


class Article(BaseModel):
    name = models.CharField(max_length=255, verbose_name="Article Name", unique=True)
    manufacturer = models.ForeignKey(Manufacturer,on_delete=models.SET_NULL,null=True,blank=True,related_name='articles',verbose_name="Manufacturer")
    suppliers = models.ManyToManyField(Supplier, related_name='components',blank=True, verbose_name='Suppliers')
    type_description = models.CharField(max_length=255, blank=True, null=True, verbose_name="Type Description")
    description = models.TextField(blank=True, null=True, verbose_name="Description")
    catalog_number = models.CharField(max_length=255, blank=True, null=True, verbose_name="Catalog No.")
    weight = models.CharField(max_length=255, blank=True, null=True, verbose_name="Weight")
    length = models.CharField(max_length=255, blank=True, null=True, verbose_name="Length")
    width = models.CharField(max_length=255, blank=True, null=True, verbose_name="Width")
    height = models.CharField(max_length=255, blank=True, null=True, verbose_name="Height")
    drawing_number = models.CharField(max_length=255, blank=True, null=True, verbose_name="Drawing No.")
    inventory_number = models.CharField(max_length=255, blank=True, null=True, verbose_name="Inventory No.")
    material_number = models.CharField(max_length=255, blank=True, null=True, verbose_name="Material No.")
    waz = models.CharField(max_length=255, blank=True, null=True, verbose_name="WAZ")
    unit = models.ForeignKey(Unit, on_delete=models.SET_NULL, null=True, related_name='articles', verbose_name='Unit')
    modified_by = models.CharField(max_length=100, blank=True, null=True)
    documents = models.ManyToManyField(
        Document,
        blank=True,
        related_name='articles',
        verbose_name='Documents'
    )

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"

    def __str__(self):
        return f"{self.name}"
    
    def label(self):
        # Show the article's name and optionally the manufacturer if available
        manufacturer = f" ({self.manufacturer.name})" if self.manufacturer else ""
        desc = f" - {self.description}" if self.description else ""
        return f"{self.name}{manufacturer}{desc}"

    def info(self):
        # Gather relevant details for the info
        type_desc = f"Type: {self.type_description}" if self.type_description else "Type: N/A"
        catalog_num = f"Catalog No.: {self.catalog_number}" if self.catalog_number else "Catalog No.: N/A"
        dimensions = f"Dimensions: {self.length}x{self.width}x{self.height}" if self.length and self.width and self.height else "Dimensions: N/A"
        inventory_num = f"Inventory No.: {self.inventory_number}" if self.inventory_number else "Inventory No.: N/A"

        # Construct the info string
        return f"{type_desc}, {catalog_num}, {dimensions}, {inventory_num}"
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Article, self).save(*args, **kwargs)
    


@receiver(post_delete, sender=Article)
def delete_associated_documents(sender, instance, **kwargs):
    documents = instance.documents.all()
    for document in documents:
        document.delete()
        

class Item(BaseModel):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='item', verbose_name="Article")
    serial_number = models.CharField(max_length=50, blank=True, null=True, verbose_name="Serial Number")
    storage_location = models.ForeignKey(StorageLocation, on_delete=models.SET_NULL, null=True, related_name='item', verbose_name="Location") # blank=True removed on 16.09
    storage_sublocation = models.ForeignKey(StorageSublocation, on_delete=models.SET_NULL, null=True, blank=True, related_name='item', verbose_name="Sublocation")
    storage_drawer = models.ForeignKey(StorageDrawer, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Drawer')
    quantity = models.FloatField(default=0, verbose_name=f"Quantity")
    assigned_project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True, related_name='item', verbose_name="Project")
    assigned_subproject = models.ForeignKey(Subproject, on_delete=models.SET_NULL, null=True, blank=True, related_name='item', verbose_name="Subproject")
    assigned_assembly = models.ForeignKey(Assembly, on_delete=models.SET_NULL, null=True, blank=True, related_name='item', verbose_name="Assembly")
    assigned_date = models.DateTimeField(auto_now_add=True, verbose_name="Assigned Date")
    last_changed_date = models.DateTimeField(auto_now=True, verbose_name="Last Changed")
    modified_by = models.CharField(max_length=100, blank=True, null=True)
    documents = models.ManyToManyField(
        Document,
        blank=True,
        related_name='items',
        verbose_name='Documents'
    )
    
    
    class Meta:
        verbose_name = "Item"
        verbose_name_plural = "Items"
        constraints = [
            models.UniqueConstraint(
                fields=['article', 'serial_number'],
                condition=~Q(serial_number__isnull=True),
                name='unique_article_serial_number'
            )
        ]
    
    def label(self):
        # If the item has a serial number, include it in the label
        serial_number = f" (Serial: {self.serial_number})" if self.serial_number else ""
        return f'{self.article.name}{serial_number}'

    def info(self):
        # Include quantity, project, location, and assembly
        project_info = f'Project: {self.assigned_project.name}' if self.assigned_project else "No project assigned"
        assembly_info = f'Assembly: {self.assigned_assembly.name}' if self.assigned_assembly else "No assembly assigned"
        location_info = f'Location: {self.storage_location.name}' if self.storage_location else "No location assigned"
        quantity_info = f'Quantity: {self.quantity}'

        # Construct the full info text
        return f'{quantity_info}, {project_info}, {assembly_info}, {location_info}'
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Item, self).save(*args, **kwargs)

        
    @property
    def quantity_with_unit(self):
        unit = self.article.unit.unit_of_measurement if self.article and self.article.unit else ''
        return f"{self.quantity} {unit}".strip()
    
    def __str__(self):
        if self.serial_number:
            return f"{self.article.name} - SN: {self.serial_number}"
        else:
            return f"{self.article.name} - Quantity: {self.quantity} {self.article.unit}"

    def clean(self):
        # Ensure that an Article is set
        if not self.article:
            raise ValidationError("Article must be set.")

        # Validation: If serial_number is provided, quantity must be 1
        if self.serial_number:
            if self.quantity != 1:
                raise ValidationError({
                    'quantity': "Quantity must be 1 when a serial number is provided. but is %s. SN: %s" % (self.quantity, self.serial_number)
                })
            # Enforce uniqueness of (article, serial_number) combination
            existing_items = Item.objects.filter(article=self.article, serial_number=self.serial_number)
            if self.pk:
                existing_items = existing_items.exclude(pk=self.pk)
            if existing_items.exists():
                raise ValidationError({
                    'serial_number': "An item with this serial number and article already exists."
                })
        else:
            # Ensure quantity is at least 1 when serial_number is empty
            if self.quantity < 1:
                raise ValidationError({
                    'quantity': "Quantity must be at least 1 when no serial number is provided."
                })

class Order(BaseModel):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='order', verbose_name="Item")
    quantity = models.FloatField(default=0, verbose_name=f"Quantity")
    modified_by = models.CharField(max_length=100, blank=True, null=True)
    date = models.DateTimeField(auto_now=True, verbose_name="Ordered at")
