from django.db import models
from .base_model import BaseModel

class Manufacturer(BaseModel):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Manufacturer"
        verbose_name_plural = "Manufacturers"

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Manufacturer, self).save(*args, **kwargs)


class Supplier(BaseModel):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)
    #components = models.ManyToManyField('Component', related_name='suppliers', blank=True)
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Supplier"
        verbose_name_plural = "Suppliers"

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(Supplier, self).save(*args, **kwargs)