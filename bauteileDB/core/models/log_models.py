from django.db import models
from core.models.base_model import BaseModel
from django.contrib.auth.models import User

class LogEntry(BaseModel):
    LEVEL_CHOICES = [
        ('INFO', 'Info'),
        ('WARNING', 'Warning'),
        ('ERROR', 'Error'),
        ('DEBUG', 'Debug'),
    ]
    
    level = models.CharField(max_length=10, choices=LEVEL_CHOICES, default='INFO')
    message = models.TextField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True,related_name='custom_log_entries')
    timestamp = models.DateTimeField(auto_now_add=True)
    additional_data = models.JSONField(null=True, blank=True)
    
    class Meta:
        verbose_name = "LogEntry"
        verbose_name_plural = "LogEntries"
    
    def info(self):
        return self.message or "No Message Available"
    
    def __str__(self):
        return f"{self.level} - {self.timestamp} - {self.message[:50]}"