from django.db import models
from .base_model import BaseModel


class StorageLocation(BaseModel):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)
    modified_by = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "Location"
        verbose_name_plural = "Locations"
    def __str__(self):
        return f'{self.name}'
    
    def label(self):
        description = self.description if self.description else ""
        
        description = (description[:20] + '...') if len(description) > 20 else description
        return f'{self.name} - {description}' if description else self.name
    
    def info(self):
        return self.description or "No Description Available"
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(StorageLocation, self).save(*args, **kwargs)


class StorageSublocation(BaseModel):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    location = models.ForeignKey(StorageLocation, on_delete=models.CASCADE, related_name='units',)
    main_storage = models.BooleanField(default=False)
    modified_by = models.CharField(max_length=100, blank=True, null=True)


    class Meta:
        verbose_name = "Sublocation"
        verbose_name_plural = "Sublocations"
        unique_together = ['name', 'location']
        
    def __str__(self):
        return f'{self.name}'
    
    def label(self):
        description = self.description if self.description else ""
        
        description = (description[:20] + '...') if len(description) > 20 else description
        return f'{self.name} - {description}' if description else self.name
    
    def info(self):
        return self.description or "No Description Available"
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(StorageSublocation, self).save(*args, **kwargs)


class StorageDrawer(BaseModel):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    sublocation = models.ForeignKey(StorageSublocation, on_delete=models.CASCADE, related_name='units',)
    modified_by = models.CharField(max_length=100, blank=True, null=True)


    class Meta:
        verbose_name = "Drawer"
        verbose_name_plural = "Drawers"
        unique_together = ['name', 'sublocation']
        
    def __str__(self):
        return f'{self.name}'
    
    def save(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        if request:
            self.modified_by = request.user.username
        super(StorageDrawer, self).save(*args, **kwargs)