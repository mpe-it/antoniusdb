from django.urls import reverse_lazy
import django_filters
from django import forms



def autocomplete_field(field_name, model_name, autocomplete_field_name=None, placeholder=None, depends_on=""):
    """
    Creates an autocomplete field for the filter form.
    - field_name: The field to filter by (e.g., 'name' or 'manufacturer__name'). <= ForeignKey field is also supported
    - model_name: The model name for the URL (e.g., 'Article' or 'Manufacturer').
    - autocomplete_field_name: Optional. The field to use for the autocomplete URL (e.g., 'name' or 'manufacturer'). <= Field Name in the model/ForeignKey model
    - placeholder: Custom placeholder text. If not provided, a default will be generated.
    """
    # If no autocomplete field name is provided, use the field_name
    if not autocomplete_field_name:
        autocomplete_field_name = field_name

    # If no custom placeholder, generate a default one by making it human-readable
    if not placeholder:
        cleaned_field_name = field_name.replace('__', ' ')  # Replace '__' with a space
        placeholder = f'Type to search {cleaned_field_name}...'

    class_string = 'form-control autocomplete-input'
    if depends_on != "":
        class_string = class_string + " depends-on " + depends_on

    return django_filters.CharFilter(
        field_name=field_name,
        lookup_expr='icontains',
        widget=forms.TextInput(attrs={
            'id': f'id_{field_name}_autocomplete',  # Dynamic ID for the input field
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={
                'model_name': model_name,  # The model for the autocomplete URL
                'field_name': autocomplete_field_name  # The field for the autocomplete URL
            }),
            'placeholder': placeholder,
            'class': class_string,
        })
    )