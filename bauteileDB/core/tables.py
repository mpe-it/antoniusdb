import django_tables2 as tables


class BaseTable(tables.Table):
    # Abstract base table with common templates, attributes, and optional selection column

    # Selection column for row selection via checkboxes
    # The `accessor="pk"` allows selection based on the primary key of the row.
    selection = tables.CheckBoxColumn(accessor="pk", orderable=False, attrs={
        "td": {"class": "row-checkbox"},  # Add a specific class to the table cell for customization
        "th__input": {"id": "select-all"}  # Add an ID to the header checkbox for 'select all' functionality
    })

    class Meta:
        # Set a common template for rendering all tables
        template_name = "django_tables2/bootstrap5.html"

        # Default table attributes for consistent styling
        attrs = {
            "class": "table table-striped table-hover table-bordered table-sm align-middle",  # Responsive and compact table styling
            "thead": {"class": "table-light text-nowrap"},  # Styled header with nowrap to prevent wrapping
        }
        # Default order of rows (no ordering applied initially)
        order_by = []

    def __init__(self, *args, **kwargs):
        # Optional argument to control whether the 'selection' column is displayed
        show_selection = kwargs.pop('show_selection', True)

        # Call the parent constructor to initialize the table
        super().__init__(*args, **kwargs)

        # If `show_selection` is False, remove the 'selection' column from the table
        if not show_selection and 'selection' in self.base_columns:
            del self.base_columns['selection']
