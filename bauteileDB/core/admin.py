from django.contrib import admin
from .models.vendor_models import Manufacturer
from .models.project_models import Project, Subproject, Assembly
from .models.storage_models import StorageLocation, StorageSublocation, StorageDrawer

@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ('name',)

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ('name',)

@admin.register(Subproject)
class SubprojectAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'project')
    search_fields = ('name', 'project__name')
    list_filter = ('project',)

@admin.register(Assembly)
class AssemblyAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'subproject', 'supervisor')
    search_fields = ('name', 'subproject__name', 'supervisor')
    list_filter = ('subproject',)

@admin.register(StorageLocation)
class StorageLocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ('name',)

@admin.register(StorageSublocation)
class StorageSublocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'location')
    search_fields = ('name', 'location__name')
    list_filter = ('location',)

@admin.register(StorageDrawer)
class StorageDrawerAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'sublocation')
    search_fields = ('name', 'sublocation__name')
    list_filter = ('sublocation',)