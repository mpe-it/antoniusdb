from django import forms
from django.urls import reverse_lazy


def get_model_instance_from_input(input_name, model_class, dependent_instance=None, dependent_field=None):
    """
    Retrieve a model instance based on the input name, model class, and optional dependency.

    Parameters:
    - input_name: The name entered in the CharField (string).
    - model_class: The model class to search for the instance (Project, Subproject, Assembly, etc.).
    - dependent_instance: Optional dependent instance (e.g., Project for Subproject, Subproject for Assembly).
    - dependent_field: The field to use for the dependency (e.g., 'project' or 'subproject').

    Returns:
    - instance: The retrieved model instance or None if not found.
    - error: A validation error message or None if no error.
    """
    if input_name:
        try:
            if dependent_instance and dependent_field:
                # If there's a dependency, filter based on it
                filter_kwargs = {
                    dependent_field: dependent_instance, 'name': input_name}
                return model_class.objects.get(**filter_kwargs), None
            else:
                # If no dependency, just filter by name
                return model_class.objects.get(name=input_name), None
        except model_class.DoesNotExist:
            return None, f"{model_class.__name__} '{input_name}' is not a valid choice."
    return None, None


def create_autocomplete_widget(model_name, field_name, placeholder, depends_on=None):
    """
    Helper function to create an autocomplete widget for a CharField.

    :param model_name: The name of the model for the autocomplete.
    :param field_name: The name of the field in the model.
    :param placeholder: The placeholder text for the field.
    :param depends_on: Optional, indicates dependency on another field (e.g., project -> subproject).
    :return: A TextInput widget configured for autocomplete.
    """
    classes = 'autocomplete-input'

    if depends_on:
        classes += f' depends-on {depends_on}'

    return forms.TextInput(attrs={
        'class': classes,
        'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={
            'model_name': model_name,
            'field_name': field_name
        }),
        'placeholder': placeholder,
    })
