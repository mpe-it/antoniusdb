from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, UpdateView, TemplateView
from django.shortcuts import get_object_or_404, redirect
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from django.views.generic import ListView
from urllib.parse import urlencode, urlparse, parse_qs, urlunparse
from log.utils import log_to_database
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.views import View
from django.http import HttpResponse
import csv


class HelpPageView(TemplateView):
    template_name = "help_template.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Help"
        return context


class GenericTableView(SingleTableMixin, FilterView, ListView):
    """
    A reusable table view with filtering, sorting, pagination, and dynamic bulk actions.
    """
    table_class = None  # This will be set in specific views
    filterset_class = None  # This will be set in specific views
    template_name = 'generic_table_list.html'  # Default template
    ordering = None  # To handle ordering of the table

    def get_table_class(self):
        if not self.table_class:
            model_name = self.model.__name__
            table_class_name = f'{model_name}Table'
            self.table_class = globals().get(table_class_name)
        return self.table_class
 
    def get_table(self, **kwargs):
        table = super().get_table(**kwargs)

        # Get the fields from the table class's Meta
        table_fields = getattr(table.Meta, 'fields', None)
        if table_fields:
            table_fields = list(table_fields)

            # Exclude 'actions' and 'selection' unless included via GET parameters
            include_actions = self.request.GET.get('include_actions', 'false').lower() == 'true'
            include_selection = self.request.GET.get('include_selection', 'false').lower() == 'true'

            if 'actions' in table_fields and not include_actions:
                table_fields.remove('actions')
            if 'selection' in table_fields and not include_selection:
                table_fields.remove('selection')

            # Apply additional exclusions from 'exclude_column' GET parameter
            exclude_columns = self.request.GET.getlist('exclude_column')
            for col in exclude_columns:
                if col in table_fields:
                    table_fields.remove(col)

            # Update the table's fields
            table.fields = table_fields

        return table

    def get_filterset_class(self):
        if not self.filterset_class:
            model_name = self.model.__name__
            filter_class_name = f'{model_name}Filter'
            self.filterset_class = globals().get(filter_class_name)
        return self.filterset_class

    def get_paginate_by(self, queryset):
        page_size = self.request.GET.get('page_size', 25)
        return page_size

    def get_ordering(self):
        sort = self.request.GET.get('sort')
        if sort:
            return sort.split('.')
        return super().get_ordering()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        model_meta = self.model._meta

        # Adding verbose names
        context['verbose_name'] = getattr(model_meta, 'verbose_name', self.model.__name__)
        context['verbose_name_plural'] = getattr(model_meta, 'verbose_name_plural', f'{self.model.__name__}s')

        # Add custom title if present in the URL
        custom_title = self.request.GET.get('custom_title')
        if custom_title:
            context['custom_title'] = custom_title
        else:
            context['custom_title'] = context['verbose_name_plural']
            
        # Handle back_url
        back_url = self.request.GET.get('back_url')
        if back_url:
            url_parts = urlparse(back_url)
            query_params = parse_qs(url_parts.query)
            query_params.pop('selection', None)
            query_params.pop('page', None)  # Remove 'page' parameter
            filtered_query = urlencode(query_params, doseq=True)
            filtered_back_url = urlunparse((
                url_parts.scheme,
                url_parts.netloc,
                url_parts.path,
                url_parts.params,
                filtered_query,
                url_parts.fragment
            ))
            context['back_url'] = filtered_back_url

        # Handle bulk actions
        bulk_actions_modal= kwargs.get('bulk_actions_modal', [])
        for action in bulk_actions_modal:
            action.setdefault('modal_template', None)
            action.setdefault('modal_js', None)

        context['bulk_actions_modal'] = bulk_actions_modal
        
        bulk_actions= kwargs.get('bulk_actions', [])
        context['bulk_actions'] = bulk_actions
        return context
    
    def dispatch(self, request, *args, **kwargs):
    # Check for the 'page' parameter and any other query params
        page_param = request.GET.get('page')
        other_params = {key: val for key, val in request.GET.items() if key != 'page'}

        # If there's only a 'page' parameter and no other query parameters, skip logging
        if not page_param or other_params:
            user = request.user if request.user.is_authenticated else None
            view_name = self.__class__.__name__
            log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
            log_to_database('DEBUG', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)


class GenericExportView(View):
    model = None  # Set this in the subclass
    filterset_class = None  # Set this in the subclass
    table_class = None  # Set this in the subclass

    def get(self, request, *args, **kwargs):
        # Get the queryset with the applied filters
        queryset = self.get_filtered_queryset()

        # Generate the CSV response
        response = HttpResponse(content_type='text/csv')
        filename = self.get_filename()
        response['Content-Disposition'] = f'attachment; filename="{filename}"'

        # Write the CSV data
        writer = csv.writer(response)

        # Get the field names
        field_names = self.get_field_names()

        # Write the header row
        writer.writerow(field_names)

        for obj in queryset:
            row = [self.get_field_value(obj, field) for field in field_names]
            writer.writerow(row)

        return response

    def get_filtered_queryset(self):
        queryset = self.model.objects.all()
        # Apply filtering using the filterset_class
        if self.filterset_class:
            filterset = self.filterset_class(self.request.GET, queryset=queryset)
            queryset = filterset.qs

        # Additional filtering for specific article
        article_id = self.kwargs.get('article_id')
        if article_id:
            queryset = queryset.filter(article_id=article_id)

        return queryset

    def get_field_names(self):
        if self.table_class:
            # Use the fields from the table's Meta class
            table_fields = getattr(self.table_class.Meta, 'fields', None)
            if table_fields:
                # Exclude 'selection' and 'actions' if present
                fields = [field for field in table_fields if field not in ('selection', 'actions')]
                return fields
        # Default to all fields
        return [field.name for field in self.model._meta.fields]

    def get_field_value(self, obj, field_name):
        # Handle related fields and method fields
        try:
            value = getattr(obj, field_name)
            if callable(value):
                value = value()
            return str(value)
        except AttributeError:
            return ''

    def get_filename(self):
        return f"{self.model._meta.verbose_name_plural}.csv"
    
    def dispatch(self, request, *args, **kwargs):
    # Log access to this view using your custom logging function
        user = request.user if request.user.is_authenticated else None
        view_name = self.__class__.__name__  # Get the name of the view class
        log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        
        # Call log_to_database function to save the log entry
        log_to_database('INFO', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)


class GenericAddView(LoginRequiredMixin,CreateView):
    template_name = 'generic_form.html'  # Default form template
    page_title = "Add New"  # Default title for adding a model
    login_url = '/login'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if 'parent_model' in self.kwargs:
            parent_model = self.kwargs.get('parent_model')
            parent_id = self.kwargs.get('parent_id')
            parent = get_object_or_404(parent_model, pk=parent_id)
            kwargs['initial'] = {self.kwargs.get('parent_field'): parent}
            kwargs[self.kwargs.get('parent_field')] = parent
        return kwargs

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy(self.success_url_name or '/')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy(self.cancel_url_name or self.success_url_name or '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = self.page_title
        context['next'] = self.request.GET.get('next', '/')
        context['cancel_url'] = self.get_cancel_url()
        
        custom_title = self.request.GET.get('custom_title')
        if custom_title:
            context['custom_title'] = custom_title

        return context
    
    def log_addition(self, obj):
        # Get the name of the newly added object
        object_name = str(obj)
        view_name = obj.__class__.__name__
        # Log the addition of the object
        log_message = f"New {view_name} added: {object_name} by {self.request.user} from IP {self.request.META.get('REMOTE_ADDR')}"
        # Log the information to the database
        log_to_database('INFO', log_message, user=self.request.user)

    def form_valid(self, form):
        #save the form instance
        response = super().form_valid(form)

        # Log the addition of the newly created object
        self.log_addition(form.instance)

        action = self.request.POST.get('action')

        if action == 'save_and_exit':
            return redirect(self.get_success_url())
        elif action == 'save_and_create_another':
            return redirect(self.request.get_full_path())  # Redirect to the same form to create another item

        return response
    
    def dispatch(self, request, *args, **kwargs):
        # Log access to this view using custom logging function
        user = request.user if request.user.is_authenticated else None
        view_name = self.__class__.__name__  # Get the name of the view class
        log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        
        # Call log_to_database function to save the log entry
        log_to_database('DEBUG', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)


class GenericUpdateView(LoginRequiredMixin,UpdateView):
    template_name = 'generic_edit_form.html'  # Default form template
    page_title = "Update"  # Default title for updating a model
    login_url = '/login'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy(self.success_url_name or '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = self.page_title
        context['next'] = self.request.GET.get('next', '/')

        cancel_url_name = getattr(self, 'cancel_url_name', None)
        success_url_name = getattr(self, 'success_url_name', None)

        context['cancel_url'] = self.request.GET.get('next', reverse_lazy(
            cancel_url_name or success_url_name or '/'))
        
        custom_title = self.request.GET.get('custom_title')
        if custom_title:
            context['custom_title'] = custom_title
        
        return context
    
    def save_with_logging(self, form):
        # Retrieve the original state before saving
        self.object = self.get_object()
        original_data = {field.name: getattr(self.object, field.name) for field in self.object._meta.fields}

        # Save the updated object
        obj = form.save(commit=False)
        obj.save(request=self.request)  # Custom save that uses request for updated_by
        self.object = obj  # Ensure self.object reflects the saved instance

        # Log changes if any
        self.log_changes(original_data, form.instance)

    def log_changes(self, original_data, updated_instance):
        updated_data = {field.name: getattr(updated_instance, field.name) for field in updated_instance._meta.fields}

        changes = []
        for field, original_value in original_data.items():
            if field == 'last_changed_date':
                continue  # Skip logging this field
            updated_value = updated_data[field]
            if original_value != updated_value:
                changes.append(f"{field} updated from \"{original_value}\" to \"{updated_value}\"")

        if changes:
            user = self.request.user if self.request.user.is_authenticated else None
            view_name = self.__class__.__name__
            change_message = "; ".join(changes)
            log_message = f"{view_name} changes made by {user}: {change_message}"
            log_to_database('INFO', log_message, user=user)

    def form_valid(self, form):
        # Save the instance with logging
        self.save_with_logging(form)
        return super().form_valid(form)
    
    def dispatch(self, request, *args, **kwargs):
        # Log access to this view using custom logging function
        user = request.user if request.user.is_authenticated else None
        view_name = self.__class__.__name__  # Get the name of the view class
        log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        
        # Call log_to_database function to save the log entry
        log_to_database('DEBUG', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)


class GenericDeleteView(LoginRequiredMixin,DeleteView):
    # Default delete confirmation template
    template_name = 'generic_confirm_delete.html'
    page_title = "Delete"  # Default title for deleting a model
    login_url = '/login'

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy(self.success_url_name or '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = self.page_title
        context['submit_button_text'] = 'Delete'
        context['cancel_url'] = self.request.GET.get('next', reverse_lazy(
            self.cancel_url_name or self.success_url_name or '/'))
        
        custom_title = self.request.GET.get('custom_title')
        if custom_title:
            context['custom_title'] = custom_title
        else:
            context['custom_title'] = context['verbose_name_plural']
            
        return context
    
    def dispatch(self, request, *args, **kwargs):
    # Log access to this view using your custom logging function
        user = request.user if request.user.is_authenticated else None
        view_name = self.__class__.__name__  # Get the name of the view class
        log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        
        # Call log_to_database function to save the log entry
        log_to_database('INFO', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)
