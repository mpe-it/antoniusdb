from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.http import Http404, JsonResponse
from django.apps import apps
from urllib.parse import urlparse, parse_qsl, urlencode, urlunparse
from django.urls import reverse
from urllib.parse import urlencode, urlparse, parse_qs
from django.contrib.auth.decorators import login_required
from django.views import View
from django.http import HttpResponse, HttpResponseBadRequest
import qrcode
from log.utils import log_to_database
from core.models.project_models import Project, Subproject,Assembly


class QRCodeView(View):
    def get(self, request, *args, **kwargs):
        # Get the URL to encode from the 'url' query parameter
        url_to_encode = request.GET.get('url')

        if not url_to_encode:
            # If no URL is provided, return a bad request response
            user = request.user if request.user.is_authenticated else None
            log_message = f"QRCodeView had no URL provided to generate QR code for user: {user}"
            log_to_database('ERROR', log_message,user=user)
            return HttpResponseBadRequest("No URL provided to generate QR code.")

        # Generate the QR code
        img = qrcode.make(url_to_encode)

        # Prepare HTTP response with the image
        response = HttpResponse(content_type='image/png')
        img.save(response, 'PNG')
        return response
    
    def dispatch(self, request, *args, **kwargs):
    # Log access to this view using your custom logging function
        user = request.user if request.user.is_authenticated else None
        view_name = self.__class__.__name__  # Get the name of the view class
        log_message = f"{view_name} accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        
        # Call log_to_database function to save the log entry
        log_to_database('DEBUG', log_message, user=user)

        # Proceed with the normal dispatch method
        return super().dispatch(request, *args, **kwargs)


def get_assembly_location(request):
    project_name = request.GET.get('project')
    subproject_name = request.GET.get('subproject')
    assembly_name = request.GET.get('assembly')

    # Validate parameters
    if not (project_name and subproject_name and assembly_name):
        user = request.user if request.user.is_authenticated else None
        log_message = f"Get_Assembly_Location accessed by {user} has Missing parameters"
        log_to_database('DEBUG',log_message, user=user)
        return JsonResponse({'error': 'Missing parameters'}, status=400)

    try:
        # Get the relevant objects based on names
        project = get_object_or_404(Project, name=project_name)
        subproject = get_object_or_404(Subproject, name=subproject_name, project=project)
        assembly = get_object_or_404(Assembly, name=assembly_name, subproject=subproject)

        # Return location and sublocation if available
        return JsonResponse({
            'location': assembly.storage_location.name if assembly.storage_location else '',
            'sublocation': assembly.storage_sublocation.name if assembly.storage_sublocation else '',
        })
    except Project.DoesNotExist:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Get_Assembly_Location accessed by {user}: Project not found"
        log_to_database('DEBUG', log_message, user=user)
        return JsonResponse({'error': 'Project not found'}, status=404)
    except Subproject.DoesNotExist:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Get_Assembly_Location accessed by {user}: Subproject not found"
        log_to_database('DEBUG', log_message, user=user)
        return JsonResponse({'error': 'Subproject not found'}, status=404)
    except Assembly.DoesNotExist:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Get_Assembly_Location accessed by {user}: Assembly not found"
        log_to_database('DEBUG', log_message, user=user)
        return JsonResponse({'error': 'Assembly not found'}, status=404)


@login_required(login_url='/login')
def bulk_delete(request, model_name, app_name='core'):
    # Get the model dynamically using both the app name and model name
    try:
        Model = apps.get_model(app_name, model_name)
    except LookupError:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Bulk_Delete accessed by {user}: Model {model_name} does not exist in app {app_name}"
        log_to_database('ERROR', log_message, user=user)
        raise Http404(f"Model {model_name} does not exist in app {app_name}.")

    # Get verbose names from the model's Meta class
    verbose_name = getattr(Model._meta, 'verbose_name', model_name)
    verbose_name_plural = getattr(Model._meta, 'verbose_name_plural', f"{model_name}s")

    if request.method == "POST":
        # Handle the selection parameter from POST data
        selection = request.POST.getlist('selection')
        #print("Selection:", selection)
        
        # Process selected IDs
        valid_ids = []
        for id in selection:
            id = id.strip()
            if id == 'on' or not id:  # Skip invalid 'on' checkbox value or empty strings
                continue
            try:
                valid_ids.append(int(id))
            except ValueError:
                user = request.user if request.user.is_authenticated else None
                log_message = f"Bulk_Delete accessed by {user}: Invalid ID '{id}' encountered"
                log_to_database('ERROR', log_message, user=user)
                messages.error(request, f"Invalid ID '{id}' encountered. Skipping invalid selections.")
                continue
        #print(valid_ids)
        #print(model_name)
        if valid_ids:
            # Fetch the objects before deletion to use their __str__ method
            items_to_delete = Model.objects.filter(pk__in=valid_ids)
            #print(items_to_delete)
            # Collect the string representation of each item (calling __str__ method)
            deleted_items_info = [str(item) for item in items_to_delete]
            
            # Delete the selected items
            items_to_delete.delete()
            
            # Create a success message listing the deleted items
            if deleted_items_info:
                deleted_items_str = ', '.join(deleted_items_info)
                messages.success(request, f"Deleted {verbose_name_plural}: {deleted_items_str}")
                user = request.user if request.user.is_authenticated else None
                log_message = f"Model: {model_name}; Entries: {deleted_items_str} deleted by {user} from IP {request.META.get('REMOTE_ADDR')}"
                log_to_database('INFO', log_message, user=user)
            else:
                messages.success(request, f"Selected {verbose_name_plural} deleted.")
                user = request.user if request.user.is_authenticated else None
                log_message = f"Model: {model_name}; Entries: {deleted_items_str} deleted by {user} from IP {request.META.get('REMOTE_ADDR')}"
                log_to_database('INFO', log_message, user=user)
        else:
            messages.error(request, f"No valid {verbose_name_plural} selected for deletion.")
    else:
        messages.error(request, "Invalid request method. Please submit the form correctly.")

    # Use 'next' parameter from POST data or default to '/'
    redirect_url = request.POST.get('next', '/')
    
    # Parse the URL to remove 'page' and 'selection' parameters
    parsed_url = urlparse(redirect_url)
    query_params = parse_qs(parsed_url.query)

    # Remove 'page' and 'selection' parameters
    query_params.pop('page', None)
    query_params.pop('selection', None)

    # Rebuild the URL without the 'page' and 'selection' parameters
    cleaned_query = urlencode(query_params, doseq=True)
    new_url = f"{parsed_url.path}?{cleaned_query}" if cleaned_query else parsed_url.path

    return redirect(new_url)


def generic_autocomplete(request, model_name, field_name):
    try:
        # Get the model dynamically based on the model_name (e.g., Subproject)
        Model = apps.get_model('core', model_name)
        
        # Get the search term for the autocomplete (e.g., subproject name)
        term = request.GET.get('term', '')
        
        # Prepare filter arguments for the main query
        filter_kwargs = {f'{field_name}__icontains': term}
        
        # Check for any dependent fields like 'project_name'
        for key, value in request.GET.items():
            if '_' in key and value:
                model_part, field_part = key.split('_')
                dependent_model_class = apps.get_model('core', model_part)
                # Handle specific replacements (e.g., storagelocation -> location)
                if model_part == 'storagelocation':
                    model_part = 'location'
                
                if model_part == 'storagesublocation':
                    model_part = 'sublocation'

                # Get the dependent instance
                dependent_instance = dependent_model_class.objects.filter(**{field_part: value}).first()
                if dependent_instance:
                    filter_kwargs[f'{model_part.lower()}_id'] = dependent_instance.id
                else:
                    return JsonResponse([], safe=False)

        # Perform the query and get the full queryset
        queryset = Model.objects.filter(**filter_kwargs).distinct()
        # Check if the model has the methods required for autocomplete.
        if not all(hasattr(Model, method) for method in ['label', 'value', 'info']):
            raise AttributeError(f"Model {model_name} must implement 'label', 'value', and 'info' methods.")

        # Build the response data with name and additional info
        data = [
            {
                "label": instance.label(),  # This will be used as the main display
                "value": instance.value(),  # This will be the value filled into the input field
                "info": instance.info()  # Additional information (e.g., description, extra details)
            }
            for instance in queryset
        ]

        return JsonResponse(data, safe=False)

    except LookupError:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Generic_Autocomplete accessed by {user}: Model {model_name} not found"
        log_to_database('ERROR', log_message, user=user)
        return JsonResponse({'error': f'Model {model_name} not found'}, status=400)
    except AttributeError as e:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Generic_Autocomplete accessed by {user}: {e}"
        log_to_database('ERROR', log_message, user=user)
        return JsonResponse({'error': str(e)}, status=400)
    except Exception as e:
        user = request.user if request.user.is_authenticated else None
        log_message = f"Generic_Autocomplete accessed by {user}: {e}"
        log_to_database('ERROR', log_message, user=user)
        #print(f"Error: {e}")
        return JsonResponse({'error': str(e)}, status=500)
