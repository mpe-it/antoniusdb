# Generated by Django 4.2.16 on 2024-10-15 10:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_rename_location_assembly_storage_location_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='name',
            field=models.CharField(max_length=255, unique=True, verbose_name='Article Name'),
        ),
    ]
