# Generated by Django 4.2.16 on 2024-12-16 11:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_tool_is_borrowed_alter_tool_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tool',
            name='is_borrowed',
        ),
        migrations.AddField(
            model_name='item',
            name='storage_drawer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.storagedrawer', verbose_name='Drawer'),
        ),
        migrations.AddField(
            model_name='tool',
            name='drawer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tool_drawer', to='core.storagedrawer'),
        ),
        migrations.AddField(
            model_name='tool',
            name='manufacturer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tool_manufacturer', to='core.manufacturer'),
        ),
        migrations.AddField(
            model_name='tool',
            name='sublocation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tool_sublocation', to='core.storagesublocation'),
        ),
        migrations.AlterField(
            model_name='tool',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tool_location', to='core.storagelocation'),
        ),
    ]
