# Generated by Django 5.1.2 on 2025-01-31 10:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0029_article_material_number_article_waz"),
    ]

    operations = [
        migrations.AlterField(
            model_name="article",
            name="catalog_number",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Catalog No."
            ),
        ),
        migrations.AlterField(
            model_name="article",
            name="drawing_number",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Drawing No."
            ),
        ),
        migrations.AlterField(
            model_name="article",
            name="inventory_number",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Inventory No."
            ),
        ),
        migrations.AlterField(
            model_name="article",
            name="material_number",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Material No."
            ),
        ),
        migrations.CreateModel(
            name="Order",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("quantity", models.FloatField(default=0, verbose_name="Quantity")),
                (
                    "modified_by",
                    models.CharField(blank=True, max_length=100, null=True),
                ),
                (
                    "date",
                    models.DateTimeField(auto_now=True, verbose_name="Ordered at"),
                ),
                (
                    "item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="order",
                        to="core.item",
                        verbose_name="Item",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
