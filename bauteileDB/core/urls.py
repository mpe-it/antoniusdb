from django.urls import path

from django.conf.urls.static import static
from bauteileDB import settings


from .views.api_views import bulk_delete, generic_autocomplete, get_assembly_location, QRCodeView
from .views.generic_views import HelpPageView

app_name = 'core'

urlpatterns = [
    path('<str:model_name>/<str:app_name>/bulk-delete/', bulk_delete, name='bulk_delete'),
    path('autocomplete/<str:model_name>/<str:field_name>/', generic_autocomplete, name='generic-autocomplete'),
    path('api/assembly-location/', get_assembly_location, name='assembly-location'),
    path('qr-code/', QRCodeView.as_view(), name='qr_code'),
    path('help/', HelpPageView.as_view(), name='help'),
]
