from core.models.component_models import Article, Item
from core.models.vendor_models import Manufacturer
from core.models.project_models import Project, Subproject, Assembly
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.tables import BaseTable
import django_tables2 as tables
from django.urls import reverse


def generate_link(object, url_name, name_field='name', back_url=None):
    """
    Helper function to generate HTML links for table columns.
    """
    if not object:
        return ""

    # Build the link with back_url if available
    link_url = reverse(url_name, args=[object.pk])
    if back_url:
        link_url += f'?back_url={back_url}'

    # Get the name field value to display
    display_name = getattr(object, name_field, str(object))

    # Return the HTML link
    return f'<a href="{link_url}">{display_name}</a>'


class ArticleTable(BaseTable):
    # Reuse BaseTable, and add model-specific fields and attributes
    actions = tables.TemplateColumn(
        template_name='components/article_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )
    
    stock = tables.Column(
        accessor='total_stock',
        verbose_name='Total Stock',
        order_by='total_stock'
    )
    
    class Meta(BaseTable.Meta):  # Inherit from BaseTable.Meta
        model = Article
        fields = ('selection', 'name', 'manufacturer', 'suppliers', 'stock', 'unit', 'material_number','inventory_number', 'waz', 'catalog_number', 'actions')


class ItemTable(BaseTable):
    #last_changed_date = tables.Column(
    #    accessor='last_changed_date.date',
    #    verbose_name='Last Changed'
    #)
    # Reuse BaseTable, and add model-specific fields and attributes
    actions = tables.TemplateColumn(
        template_name='components/item_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )
    quantity_with_unit = tables.Column(
        accessor='quantity_with_unit',
        verbose_name='Quantity',
        order_by='quantity'  # Sorting will be based on the 'quantity' field
    )
    article = tables.TemplateColumn(
        template_name='components/article_name_column.html',
        verbose_name='Article Name',
        order_by='article.name'  # Optional: Enable sorting by article name
    )
    storage_location = tables.Column(
        accessor='storage_location.name',
        verbose_name='Location',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.storage_location else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.storage_location else None,
                   "title": lambda record: record.storage_location.info if record.storage_location else None}
        }
    )
    storage_sublocation = tables.Column(
        accessor='storage_sublocation.name',
        verbose_name='Sublocation',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.storage_sublocation else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.storage_sublocation else None,
                   "title": lambda record: record.storage_sublocation.info if record.storage_sublocation else None}
        }
    )
    storage_drawer = tables.Column(
        accessor='storage_drawer.name',
        verbose_name='Drawer',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.storage_drawer else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.storage_drawer else None,
                   "title": lambda record: record.storage_drawer.info if record.storage_drawer else None}
        }
    )
    assigned_project = tables.Column(
        accessor='assigned_project.name',
        verbose_name='Project',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.assigned_project else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.assigned_project else None,
                   "title": lambda record: record.assigned_project.info if record.assigned_project else None}
        }
    )
    assigned_subproject = tables.Column(
        accessor='assigned_subproject.name',
        verbose_name='Subproject',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.assigned_subproject else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.assigned_subproject else None,
                   "title": lambda record: record.assigned_subproject.info if record.assigned_subproject else None}
        }
    )
    assigned_assembly = tables.Column(
        accessor='assigned_assembly.name',
        verbose_name='Assembly',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.assigned_assembly else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.assigned_assembly else None,
                   "title": lambda record: record.assigned_assembly.info if record.assigned_assembly else None}
        }
    )
    
    class Meta(BaseTable.Meta):
        model = Item
        fields = ('selection', 'article', 'serial_number', 'quantity_with_unit', 'storage_location', 'storage_sublocation', 'storage_drawer', 'assigned_project', 'assigned_subproject', 'assigned_assembly', 'actions')#'last_changed_date'
        attrs = {
            'class': 'table table-bordered table-hover',
        }
        

class AllItemsTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='components/item_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    #last_changed_date = tables.DateColumn(
    #    accessor='last_changed_date',
    #    verbose_name='Last Changed Date',
    #    format='Y-m-d'
    #)

    serial_number = tables.Column()
    storage_location = tables.Column()
    storage_sublocation = tables.Column()
    storage_drawer = tables.Column()
    assigned_project = tables.Column()
    assigned_subproject = tables.Column()
    assigned_assembly = tables.Column()

    # Article fields with link to the article
    article_name = tables.TemplateColumn(
        template_name='components/article_name_column.html',
        verbose_name='Article Name',
        order_by='article.name'  # Optional: Enable sorting by article name
    )
    
    article_manufacturer = tables.Column(accessor='article.manufacturer.name', verbose_name='Manufacturer')

    quantity_with_unit = tables.Column(
        accessor='quantity_with_unit',
        verbose_name='Quantity',
        order_by='quantity'
    )

    class Meta:
        model = Item
        template_name = 'django_tables2/bootstrap5.html'
        fields = [
            'selection', 'article_name', 'article_manufacturer', 'serial_number', 'quantity_with_unit',
            'storage_location', 'storage_sublocation', 'storage_drawer', 'assigned_project', 'assigned_subproject',
            'assigned_assembly', 'actions'
        ]#'last_changed_date', 
