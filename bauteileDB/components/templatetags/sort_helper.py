from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def toggle_sort(context, column_order):
    """
    Toggles between ascending and descending sorting for the column.
    Clicking the same column toggles between ascending and descending without removing it.
    """
    request = context['request']
    params = request.GET.copy()  # Get current query parameters

    # Get current sorting order
    current_sort = params.getlist('sort')

    # If the column is sorted as descending, switch it back to ascending
    if f'-{column_order}' in current_sort:
        current_sort[current_sort.index(f'-{column_order}')] = column_order  # Toggle back to ascending
    
    # If the column is sorted as ascending, toggle to descending
    elif column_order in current_sort:
        current_sort[current_sort.index(column_order)] = f'-{column_order}'  # Toggle to descending
    
    # If the column is not in the sorting list yet, add it as ascending
    else:
        current_sort.append(column_order)  # Add as ascending
    
    # Clean up any invalid '--name' cases (ensure no double `--`)
    current_sort = [s if not s.startswith('--') else s.lstrip('-') for s in current_sort]

    # Update the 'sort' parameter in the query string
    params.setlist('sort', current_sort)

    return params.urlencode() 

@register.simple_tag(takes_context=True)
def querystring_remove_column(context, column_alias):
    """
    Removes only the specified column from the 'sort' query parameter.
    """
    request = context['request']
    params = request.GET.copy()

    # Get the current sort order
    sort_columns = params.getlist('sort')

    # Remove the specific column (both ascending and descending versions)
    sort_columns = [col for col in sort_columns if col != column_alias and col != f'-{column_alias}']

    # Update the 'sort' query parameter
    if sort_columns:
        params.setlist('sort', sort_columns)
    else:
        del params['sort']  # Remove 'sort' if no columns remain

    return params.urlencode()

