from django.contrib import admin
from core.models.component_models import Article, Item, Unit

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('name', 'manufacturer', 'inventory_number', 'catalog_number')
    search_fields = ('name', 'manufacturer__name', 'inventory_number')


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('article', 'serial_number', 'storage_location', 'storage_sublocation', 'quantity', 'assigned_project', 'assigned_subproject', 'assigned_assembly', 'assigned_date')
    search_fields = ('article__name', 'serial_number', 'storage_location__name', 'storage_sublocation__name')
    list_filter = ('assigned_project', 'assigned_subproject', 'assigned_assembly', 'storage_location', 'storage_sublocation')

@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('unit_of_measurement', 'description')  # Displays the 'unit_of_measurement' field in the list view
    search_fields = ('unit_of_measurement', 'description')  # Adds a search box for 'unit_of_measurement'