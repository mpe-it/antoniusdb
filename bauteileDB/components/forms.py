from django import forms
from django.urls import reverse_lazy
from core.models.vendor_models import Manufacturer, Supplier
from core.models.component_models import Article, Item, Document,  Order
from core.models.project_models import Project, Subproject, Assembly
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.forms import get_model_instance_from_input
from core.forms import create_autocomplete_widget


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True  # Allow multiple file selection


class MultipleFileField(forms.FileField):
    widget = MultipleFileInput

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            return [single_file_clean(d, initial) for d in data]
        else:
            return [single_file_clean(data, initial)]


class ArticleForm(forms.ModelForm):
    name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Article', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search or enter a new name...'
        })
    )

    manufacturer_input = forms.CharField(
        required=False,
        widget=create_autocomplete_widget(
            model_name='Manufacturer',
            field_name='name',
            placeholder='Start typing to search for manufacturer...'
        )
    )

    manufacturer = forms.ModelChoiceField(
        queryset=Manufacturer.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    suppliers = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Supplier', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search and separate suppliers by commas...'
        }),
        label="Suppliers (comma-separated)"
    )

    files = MultipleFileField(required=False, label='Upload Documents')

    class Meta:
        model = Article
        fields = [
            'name',
            'manufacturer_input',
            'manufacturer',
            'suppliers',
            'type_description',
            'description',
            'catalog_number',
            'weight',
            'width',
            'height',
            'length',
            'drawing_number',
            'inventory_number',
            'material_number',
            'waz',
            'unit',
            'modified_by',
            'files'  # Include 'files' to render the field in the form
        ]
        widgets = {
            'description': forms.Textarea(attrs={'rows': 3}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

    def clean(self):
        cleaned_data = super().clean()

        # Process the manufacturer_input and set manufacturer
        manufacturer_name = cleaned_data.get('manufacturer_input')
        manufacturer, error = get_model_instance_from_input(manufacturer_name, Manufacturer)
        if error:
            self.add_error('manufacturer_input', error)
        else:
            cleaned_data['manufacturer'] = manufacturer


        suppliers_input = self.data.get('suppliers')  # Raw input from the form
        if suppliers_input:
            supplier_names = [name.strip() for name in suppliers_input.split(',') if name.strip()]
            suppliers = Supplier.objects.filter(name__in=supplier_names)
            
            # Validate suppliers
            missing_suppliers = set(supplier_names) - set(suppliers.values_list('name', flat=True))
            if missing_suppliers:
                self.add_error('suppliers', f"These suppliers do not exist: {', '.join(missing_suppliers)}")
            else:
                cleaned_data['suppliers'] = suppliers 


        # Check if an Article with the same name already exists
        name = cleaned_data.get('name')
        if name:
            existing_articles = Article.objects.filter(name=name)
            if self.instance.pk:
                existing_articles = existing_articles.exclude(pk=self.instance.pk)
            if existing_articles.exists():
                self.add_error('name', 'An Article with this name already exists.')

        return cleaned_data

    def save(self, commit=True):
        instance = super().save(commit=False)

        if commit:
            instance.save()
        
        suppliers = self.cleaned_data.get('suppliers', [])
        instance.suppliers.set(suppliers) 

        # Process the uploaded files
        files = self.cleaned_data.get('files')
        if files:
            for file in files:
                document = Document(file=file)
                document.save()
                instance.documents.add(document)
        
        if 'suppliers' in self.cleaned_data:
            instance.suppliers.set(self.cleaned_data['suppliers'])

        return instance


class BaseItemForm(forms.ModelForm):
    # Autocomplete input fields with Bootstrap 'form-control' class
    storage_location_input = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageLocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for location...',
        })
    )
    
    # Hidden ModelChoiceFields with proper naming
    storage_location = forms.ModelChoiceField(
        queryset=StorageLocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    storage_sublocation_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on StorageLocation_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageSublocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for sublocation...',
        })
    )
    
    storage_sublocation = forms.ModelChoiceField(
        queryset=StorageSublocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    storage_drawer_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on StorageSublocation_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageDrawer', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for drawer...',
        })
    )
    
    storage_drawer = forms.ModelChoiceField(
        queryset=StorageDrawer.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    project_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'Project', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for project...',
        })
    )
    
    assigned_project = forms.ModelChoiceField(
        queryset=Project.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    subproject_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on Project_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'Subproject', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for subproject...',
        })
    )
    
    assigned_subproject = forms.ModelChoiceField(
        queryset=Subproject.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    assembly_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on Subproject_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'Assembly', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for assembly...',
        })
    )
    
    assigned_assembly = forms.ModelChoiceField(
        queryset=Assembly.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    files = MultipleFileField(required=False, label='Upload Documents')
    
    class Meta:
        model = Item
        fields = [
            'article',
            'serial_number',
            'quantity',
            'storage_location',
            'storage_location_input',
            'storage_sublocation',
            'storage_sublocation_input',
            'storage_drawer',
            'storage_drawer_input',
            'assigned_project',
            'project_input',
            'assigned_subproject',
            'subproject_input',
            'assigned_assembly',
            'assembly_input',
            'modified_by',
            'files',
        ]
        widgets = {
            'serial_number': forms.TextInput(attrs={'placeholder': 'Enter Serial Number', 'class': 'form-control'}),
            'quantity': forms.NumberInput(attrs={'min': 0, 'class': 'form-control'}),
            'article': forms.Select(attrs={'class': 'form-control'}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }
    
    def clean(self):
        cleaned_data = super().clean()

        # Process storage_location_input and set storage_location
        location_name = cleaned_data.get('storage_location_input')
        storage_location, error = get_model_instance_from_input(location_name, StorageLocation)
        if error:
            self.add_error('storage_location_input', error)
        else:
            cleaned_data['storage_location'] = storage_location

        # Process storage_sublocation_input and set storage_sublocation
        sublocation_name = cleaned_data.get('storage_sublocation_input')
        storage_sublocation, error = get_model_instance_from_input(
            sublocation_name, StorageSublocation, storage_location, 'location'
        )
        if error:
            self.add_error('storage_sublocation_input', error)
        else:
            cleaned_data['storage_sublocation'] = storage_sublocation
        
        # Process storage_drawer_input and set storage_drawer
        drawer_name = cleaned_data.get('storage_drawer_input')
        storage_drawer, error = get_model_instance_from_input(
            drawer_name, StorageDrawer, storage_sublocation, 'sublocation'
        )
        if error:
            self.add_error('storage_drawer_input', error)
        else:
            cleaned_data['storage_drawer'] = storage_drawer

        # Process project_input and set assigned_project
        project_name = cleaned_data.get('project_input')
        assigned_project, error = get_model_instance_from_input(project_name, Project)
        if error:
            self.add_error('project_input', error)
        else:
            cleaned_data['assigned_project'] = assigned_project

        # Process subproject_input and set assigned_subproject
        subproject_name = cleaned_data.get('subproject_input')
        assigned_subproject, error = get_model_instance_from_input(
            subproject_name, Subproject, assigned_project, 'project'
        )
        if error:
            self.add_error('subproject_input', error)
        else:
            cleaned_data['assigned_subproject'] = assigned_subproject

        # Process assembly_input and set assigned_assembly
        assembly_name = cleaned_data.get('assembly_input')
        assigned_assembly, error = get_model_instance_from_input(
            assembly_name, Assembly, assigned_subproject, 'subproject'
        )
        if error:
            self.add_error('assembly_input', error)
        else:
            cleaned_data['assigned_assembly'] = assigned_assembly

        # Dependency validations
        if storage_sublocation and not storage_location:
            self.add_error('storage_sublocation_input', "A sublocation cannot be assigned without a valid location.")
        if storage_drawer and not storage_sublocation:
            self.add_error('storage_drawer_input', "A drawer cannot be assigned without a valid sublocation.")
        if assigned_subproject and not assigned_project:
            self.add_error('subproject_input', "A subproject cannot be assigned without a valid project.")
        if assigned_assembly and not assigned_subproject:
            self.add_error('assembly_input', "An assembly cannot be assigned without a valid subproject.")

        # Ensure relationships match
        if storage_sublocation and storage_sublocation.location != storage_location:
            self.add_error(
                'storage_sublocation_input',
                f"The sublocation '{sublocation_name}' does not belong to the location '{location_name}'."
            )
            
        if storage_drawer and storage_drawer.sublocation != storage_sublocation:
            self.add_error(
                'storage_drawer_input',
                f"The drawer '{drawer_name}' does not belong to the sublocation '{sublocation_name}'."
            )

        if assigned_subproject and assigned_subproject.project != assigned_project:
            self.add_error(
                'subproject_input',
                f"The subproject '{subproject_name}' does not belong to the project '{project_name}'."
            )

        if assigned_assembly and assigned_assembly.subproject != assigned_subproject:
            self.add_error(
                'assembly_input',
                f"The assembly '{assembly_name}' does not belong to the subproject '{subproject_name}'."
            )

        return cleaned_data
    
    def save(self, commit=True):
        instance = super().save(commit=False)

        if commit:
            instance.save()

        # Process the uploaded files
        files = self.cleaned_data.get('files')
        if files:
            for file in files:
                document = Document(file=file)
                document.save()
                instance.documents.add(document)

        return instance
    
    def __init__(self, *args, **kwargs):
        self.article = kwargs.pop('article', None)
        super().__init__(*args, **kwargs)

        # Initialize the autocomplete fields with instance data when editing
        if self.instance and self.instance.pk:
            if self.instance.storage_location:
                self.fields['storage_location_input'].initial = self.instance.storage_location.name
            if self.instance.storage_sublocation:
                self.fields['storage_sublocation_input'].initial = self.instance.storage_sublocation.name
            if self.instance.storage_drawer:
                self.fields['storage_drawer_input'].initial = self.instance.storage_drawer.name
            if self.instance.assigned_project:
                self.fields['project_input'].initial = self.instance.assigned_project.name
            if self.instance.assigned_subproject:
                self.fields['subproject_input'].initial = self.instance.assigned_subproject.name
            if self.instance.assigned_assembly:
                self.fields['assembly_input'].initial = self.instance.assigned_assembly.name


class ItemForm(BaseItemForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['article'].widget = forms.Select(
            attrs={'class': 'form-control'})


class ItemFormHiddenArticle(BaseItemForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['article'].widget = forms.HiddenInput()


class ItemCreationForm(forms.Form):
    article = forms.ModelChoiceField(
        queryset=Article.objects.all(),
        widget=forms.HiddenInput()
    )
    serial_number = forms.CharField(
        required=False, help_text="Enter serial numbers separated by commas. These are subtracted from the quantity and added as individual items automatically.",
        widget=forms.TextInput(attrs={
            'placeholder': 'Enter Serial Numbers: 123, 124, 125...',
        })
    )
    quantity = forms.IntegerField(min_value=1)
    
    # Autocomplete input fields
    storage_location_input = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageLocation', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for location...',
        })
    )

    storage_sublocation_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on StorageLocation_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageSublocation', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for sublocation...',
        })
    )

    storage_drawer_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on StorageSublocation_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageDrawer', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for drawer...',
        })
    )

    project_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Project', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for project...',
        })
    )

    subproject_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on Project_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Subproject', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for subproject...',
        })
    )

    assembly_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on Subproject_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Assembly', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for assembly...',
        })
    )
    
    files = MultipleFileField(required=False, label='Upload Documents')

    def __init__(self, *args, **kwargs):
        self.article = kwargs.pop('article', None)
        if not self.article:
            raise ValueError("Article must be provided to the form.")
        super().__init__(*args, **kwargs)
        self.fields['article'].initial = self.article.pk
        
        if self.article.unit.unit_of_measurement != 'pcs':
            self.fields.pop('serial_number')

    def clean(self):
        cleaned_data = super().clean()
        serial_number = cleaned_data.get('serial_number')
        quantity = cleaned_data.get('quantity')

        # Process storage_location_input
        storage_location_input = cleaned_data.get('storage_location_input')
        storage_location = None
        if storage_location_input:
            try:
                storage_location = StorageLocation.objects.get(name=storage_location_input)
            except StorageLocation.DoesNotExist:
                self.add_error('storage_location_input', f"Storage Location '{storage_location_input}' does not exist.")
        else:
            self.add_error('storage_location_input', "This field is required.")

        # Process storage_sublocation_input
        storage_sublocation_input = cleaned_data.get('storage_sublocation_input')
        storage_sublocation = None
        if storage_sublocation_input:
            if storage_location is None:
                self.add_error('storage_sublocation_input', "A sublocation cannot be assigned without a valid location.")
            else:
                try:
                    storage_sublocation = StorageSublocation.objects.get(
                        name=storage_sublocation_input,
                        location=storage_location
                    )
                except StorageSublocation.DoesNotExist:
                    self.add_error(
                        'storage_sublocation_input',
                        f"Sublocation '{storage_sublocation_input}' does not exist in location '{storage_location.name}'."
                    )

        # Process storage_drawer_input
        storage_drawer_input = cleaned_data.get('storage_drawer_input')
        storage_drawer = None
        if storage_drawer_input:
            if storage_sublocation is None:
                self.add_error('storage_drawer_input', "A drawer cannot be assigned without a valid sublocation.")
            else:
                try:
                    storage_drawer = StorageDrawer.objects.get(
                        name=storage_drawer_input,
                        sublocation=storage_sublocation
                    )
                except StorageDrawer.DoesNotExist:
                    self.add_error(
                        'storage_drawer_input',
                        f"Drawer '{storage_drawer_input}' does not exist in sublocation '{storage_sublocation.name}'."
                    )

        # Process project_input
        project_input = cleaned_data.get('project_input')
        assigned_project = None
        if project_input:
            try:
                assigned_project = Project.objects.get(name=project_input)
            except Project.DoesNotExist:
                self.add_error('project_input', f"Project '{project_input}' does not exist.")

        # Process subproject_input
        subproject_input = cleaned_data.get('subproject_input')
        assigned_subproject = None
        if subproject_input:
            if assigned_project is None:
                self.add_error('subproject_input', "A subproject cannot be assigned without a valid project.")
            else:
                try:
                    assigned_subproject = Subproject.objects.get(
                        name=subproject_input,
                        project=assigned_project
                    )
                except Subproject.DoesNotExist:
                    self.add_error(
                        'subproject_input',
                        f"Subproject '{subproject_input}' does not exist under project '{assigned_project.name}'."
                    )

        # Process assembly_input
        assembly_input = cleaned_data.get('assembly_input')
        assigned_assembly = None
        if assembly_input:
            if assigned_subproject is None:
                self.add_error('assembly_input', "An assembly cannot be assigned without a valid subproject.")
            else:
                try:
                    assigned_assembly = Assembly.objects.get(
                        name=assembly_input,
                        subproject=assigned_subproject
                    )
                except Assembly.DoesNotExist:
                    self.add_error(
                        'assembly_input',
                        f"Assembly '{assembly_input}' does not exist under subproject '{assigned_subproject.name}'."
                    )

        # Validate serial_number and quantity
        if serial_number:
            serial_numbers = [s.strip() for s in serial_number.split(',') if s.strip()]
            num_serial_numbers = len(serial_numbers)

            if num_serial_numbers > quantity:
                self.add_error('serial_number', "Number of serial numbers cannot exceed the quantity.")

            if len(serial_numbers) != len(set(serial_numbers)):
                self.add_error('serial_number', "Duplicate serial numbers detected in the input.")

            # Check for existing serial numbers
            for sn in serial_numbers:
                existing_items = Item.objects.filter(article=self.article, serial_number=sn)
                if existing_items.exists():
                    self.add_error('serial_number', f"An item with serial number '{sn}' for this article already exists.")
        else:
            if quantity < 1:
                self.add_error('quantity', "Quantity must be at least 1 when no serial number is provided.")

        # Store the model instances for use in the view
        self.cleaned_data['storage_location'] = storage_location
        self.cleaned_data['storage_sublocation'] = storage_sublocation
        self.cleaned_data['assigned_project'] = assigned_project
        self.cleaned_data['assigned_subproject'] = assigned_subproject
        self.cleaned_data['assigned_assembly'] = assigned_assembly
        self.cleaned_data['storage_drawer'] = storage_drawer

        return cleaned_data
    def save(self, commit=True):
        instance = super().save(commit=False)

        if commit:
            instance.save()

        # Process the uploaded files
        files = self.cleaned_data.get('files')
        if files:
            for file in files:
                document = Document(file=file)
                document.save()
                instance.documents.add(document)

        return instance
    

class BulkTransferForm(forms.Form):

    # Autocomplete fields for Project, Subproject, Assembly, Storage Location, Sublocation
    project_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Project', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for project...',
        })
    )
    subproject_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on Project_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Subproject', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for subproject...',
        })
    )
    assembly_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on Subproject_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Assembly', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for assembly...',
        })
    )
    storage_location_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageLocation', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for location...',
        })
    )
    storage_sublocation_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on StorageLocation_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageSublocation', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for sublocation...',
        })
    )
    storage_drawer_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on StorageSublocation_name',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'StorageDrawer', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search for drawer...',
        })
    )

    def clean(self):
        cleaned_data = super().clean()



        # Process project_input
        project_input = cleaned_data.get('project_input')
        assigned_project = None
        if project_input:
            try:
                assigned_project = Project.objects.get(name=project_input)
            except Project.DoesNotExist:
                self.add_error('project_input', f"Project '{project_input}' does not exist.")

        # Process subproject_input
        subproject_input = cleaned_data.get('subproject_input')
        assigned_subproject = None
        if subproject_input:
            if assigned_project is None:
                self.add_error('subproject_input', "A subproject cannot be assigned without a valid project.")
            else:
                try:
                    assigned_subproject = Subproject.objects.get(
                        name=subproject_input,
                        project=assigned_project
                    )
                except Subproject.DoesNotExist:
                    self.add_error(
                        'subproject_input',
                        f"Subproject '{subproject_input}' does not exist under project '{assigned_project.name}'."
                    )

        # Process assembly_input
        assembly_input = cleaned_data.get('assembly_input')
        assigned_assembly = None
        if assembly_input:
            if assigned_subproject is None:
                self.add_error('assembly_input', "An assembly cannot be assigned without a valid subproject.")
            else:
                try:
                    assigned_assembly = Assembly.objects.get(
                        name=assembly_input,
                        subproject=assigned_subproject
                    )
                except Assembly.DoesNotExist:
                    self.add_error(
                        'assembly_input',
                        f"Assembly '{assembly_input}' does not exist under subproject '{assigned_subproject.name}'."
                    )

        # Process storage_location_input
        storage_location_input = cleaned_data.get('storage_location_input')
        storage_location = None
        if storage_location_input:
            try:
                storage_location = StorageLocation.objects.get(name=storage_location_input)
            except StorageLocation.DoesNotExist:
                self.add_error('storage_location_input', f"Storage Location '{storage_location_input}' does not exist.")

        # Process storage_sublocation_input
        storage_sublocation_input = cleaned_data.get('storage_sublocation_input')
        storage_sublocation = None
        if storage_sublocation_input:
            if storage_location is None:
                self.add_error('storage_sublocation_input', "A sublocation cannot be assigned without a valid location.")
            else:
                try:
                    storage_sublocation = StorageSublocation.objects.get(
                        name=storage_sublocation_input,
                        location=storage_location
                    )
                except StorageSublocation.DoesNotExist:
                    self.add_error(
                        'storage_sublocation_input',
                        f"Sublocation '{storage_sublocation_input}' does not exist in location '{storage_location.name}'."
                    )

        # Process storage_drawer_input
        storage_drawer_input = cleaned_data.get('storage_drawer_input')
        storage_drawer = None
        if storage_drawer_input:
            if storage_sublocation is None:
                self.add_error('storage_drawer_input', "A drawer cannot be assigned without a valid sublocation.")
            else:
                try:
                    storage_drawer = StorageDrawer.objects.get(
                        name=storage_drawer_input,
                        sublocation=storage_sublocation
                    )
                except StorageDrawer.DoesNotExist:
                    self.add_error(
                        'storage_drawer_input',
                        f"Drawer '{storage_drawer_input}' does not exist in sublocation '{storage_sublocation.name}'."
                    )

        # Assign cleaned data
        cleaned_data['assigned_project'] = assigned_project
        cleaned_data['assigned_subproject'] = assigned_subproject
        cleaned_data['assigned_assembly'] = assigned_assembly
        cleaned_data['storage_location'] = storage_location
        cleaned_data['storage_sublocation'] = storage_sublocation
        cleaned_data['storage_drawer'] = storage_drawer

        return cleaned_data

 
class ItemSplitForm(forms.Form):
    serial_number = forms.CharField(
        required=False,
        help_text="Enter serial numbers separated by commas. These are subtracted from the quantity and added as individual items automatically.",
        widget=forms.TextInput(attrs={
            'placeholder': 'Enter Serial Numbers: 123, 124, 125...',
            'class': 'serial-number-field',
        })
    )
    quantity = forms.DecimalField(
        required=True,
        max_digits=10,
        decimal_places=4,
        help_text="Enter the total quantity to split from this item."
    )

    def __init__(self, *args, **kwargs):
        self.item = kwargs.pop('item', None)
        super().__init__(*args, **kwargs)

        # Customize the quantity input based on the item's unit type
        if self.item and str(self.item.article.unit) == "pcs":
            self.fields['quantity'].widget = forms.NumberInput(attrs={
                'step': 1,  # Only allow whole numbers
                'min': 1,
            })
        else:
            self.fields['quantity'].widget = forms.NumberInput(attrs={
                'step': '0.0001',  # Allow fractional numbers with up to 4 decimal places
                'min': 0.0001,  # Ensure at least a small positive quantity
            })
            # Hide the serial number field for non-"pcs" items
            self.fields['serial_number'].widget = forms.HiddenInput()

    def clean(self):
        """
        Final validation logic after individual field cleaning.
        """
        cleaned_data = super().clean()
        serial_number = cleaned_data.get('serial_number')
        quantity = cleaned_data.get('quantity')

        # Ensure quantity is valid
        if self.item and quantity:
            if quantity > self.item.quantity:
                self.add_error('quantity', f"Quantity exceeds available ({self.item.quantity}).")

        # Validate serial numbers if provided
        if serial_number:
            serial_numbers = [s.strip() for s in serial_number.split(',') if s.strip()]
            num_serial_numbers = len(serial_numbers)

            # Perform the comparison between serial numbers and quantity
            if quantity is not None and num_serial_numbers > quantity:
                self.add_error('serial_number', "The number of serial numbers cannot exceed the quantity.")

            # Check for duplicate serial numbers
            if len(serial_numbers) != len(set(serial_numbers)):
                self.add_error('serial_number', "Duplicate serial numbers detected in the input.")

        return cleaned_data



class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['quantity']
        labels = {
            'quantity': 'Order Quantity'
        }
        widgets = {
            'quantity': forms.NumberInput(attrs={'step': 'any', 'min': '0'}),
        }