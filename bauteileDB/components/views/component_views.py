import csv
import qrcode
import io
from decimal import Decimal
from urllib.parse import unquote, urlencode
from django import apps
from django.contrib import messages
from django.forms import ValidationError
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from core.views.generic_views import GenericAddView, GenericUpdateView, GenericDeleteView, GenericTableView, GenericExportView
from django.views import View
from core.models.component_models import Article, Item, Unit, Document, Order
from ..filters import AllItemsFilter, ArticleFilter, ItemFilter, ItemStockFilter
from ..tables import AllItemsTable, ArticleTable, ItemTable  # Import all necessary tables
from ..forms import ArticleForm, ItemFormHiddenArticle, ItemCreationForm, BulkTransferForm, ItemSplitForm, OrderForm
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.db import transaction, models
from urllib.parse import urlparse, parse_qs, urlencode
from django.db import IntegrityError
from urllib.parse import urlencode, quote, urlparse, parse_qs, urlunparse
from django.utils.text import slugify
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.base import ContentFile
from log.utils import log_to_database
from django.views.generic import DetailView
from django.http import HttpResponse
from django.core.mail import send_mail
from django.conf import settings



# Article views
class ArticleItemsExportView(View):
    def get(self, request, article_id, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="items_of_article_{article_id}.csv"'

        writer = csv.writer(response)
        writer.writerow(['Name', 'Serial Number', 'Quantity', 'Location', 'Assembly'])

        items = Item.objects.filter(article_id=article_id)
        for item in items:
            writer.writerow([item.name, item.serial_number, item.quantity, item.location, item.assembly])

        return response


class ArticleListView(GenericTableView):
    model = Article
    table_class = ArticleTable
    filterset_class = ArticleFilter
    ordering = ['name']


    def get_queryset(self):
        queryset = Article.objects.annotate(
            total_stock=Coalesce(Sum('item__quantity'), 0.0)
        )
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'article',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        # Add 'Add Article' button information
        context['add_button_url'] = reverse_lazy('components:article-add')
        context['add_button_label'] = 'Add Article'
        
        context['export_button_url'] = reverse_lazy('components:article_export')
        context['export_button_label'] = 'Export'
        
        return context


class ArticleAddView(GenericAddView):
    model = Article
    form_class = ArticleForm
    page_title = "Add New Article"
    success_url_name = 'components:article_list'
    cancel_url_name = 'components:article_list'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.method == 'GET':
            default_measurement, created = Unit.objects.get_or_create(
                unit_of_measurement="pcs",
                defaults={'description': 'Standard unit of measurement'}
            )
            kwargs['initial'] = {'unit': default_measurement}
        return kwargs
 
    def form_valid(self, form):

        form.instance.modified_by = self.request.user.username 

        response = super().form_valid(form)
        action = self.request.POST.get('action')

        if action == 'save_and_exit':
            return redirect(self.get_success_url())
        elif action == 'save_and_create_another':
            return redirect(self.request.get_full_path())

        return response


class ArticleUpdateView(GenericUpdateView):
    model = Article
    form_class = ArticleForm
    page_title = "Edit Article"
    success_url_name = 'components:article_list'
    cancel_url_name = 'components:article_list'
    template_name = 'components/article_edit.html'

    def get_form_kwargs(self):
        # Get the default form kwargs
        kwargs = super().get_form_kwargs()

        # Get the current article instance
        article = self.get_object()

        # If this is an update (i.e., the article instance exists)
        if article.pk:
            # Set initial value for suppliers field as a comma-separated string of supplier names
            supplier_names = ', '.join(article.suppliers.values_list('name', flat=True))
            kwargs['initial'] = kwargs.get('initial', {})
            kwargs['initial']['suppliers'] = supplier_names

        if article.manufacturer:
            kwargs['initial']['manufacturer_input'] = article.manufacturer.name

        return kwargs

    def form_valid(self, form):
        form.instance.modified_by = self.request.user.username 
        
        # Save with logging but only once
        self.save_with_logging(form)
        article_instance = form.instance

        # Handle deletion of documents
        for document in article_instance.documents.all():
            if self.request.POST.get(f'delete_document_{document.pk}'):
                document.delete()

        # Handle user actions: 'save_and_exit' or 'save_and_create_another'
        action = self.request.POST.get('action')
        if action == 'save_and_exit':
            return redirect(self.get_success_url())
        elif action == 'save_and_create_another':
            return redirect(self.request.get_full_path())

        return super().form_valid(form)


class ArticleDeleteView(GenericDeleteView):
    model = Article
    page_title = "Delete Article"
    success_url_name = 'components:article_list'
    cancel_url_name = 'components:article_list'


class ArticleExportView(GenericExportView):
    model = Article
    filterset_class = ArticleFilter
    table_class = ArticleTable


class ArticleItemsExportView(GenericExportView):
    model = Item
    filterset_class = ItemStockFilter
    table_class = AllItemsTable

    def get_filtered_queryset(self):
        queryset = self.model.objects.all()
        if self.filterset_class:
            filterset = self.filterset_class(self.request.GET, queryset=queryset)
            queryset = filterset.qs

        # Filter by article_id
        article_id = self.kwargs.get('article_id')
        if article_id:
            queryset = queryset.filter(article_id=article_id)

        return queryset


# Item views


class ItemListView(GenericTableView):
    model = Item
    table_class = ItemTable
    filterset_class = ItemFilter
    ordering = ['article__name']  # Default ordering by article name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'item',
                        'app_name': 'core' 
                    })
                },
            ],
            bulk_actions=[
                {
                    'name': 'Transfer Selected',
                    'class': 'btn btn-primary',
                    'bulk_action_url': reverse_lazy('components:bulk_transfer'),
                    'method': 'GET',
                }
                ],
            **kwargs
        )
        
        # Export Button
        context['export_button_url'] = reverse_lazy('components:item_export')
        context['export_button_label'] = 'Export'
        
        return context


class ItemAddView(LoginRequiredMixin,View):
    form_class = ItemCreationForm
    template_name = 'components/add_item_form.html'
    page_title = "Add New Item"
    login_url = '/login'

    def dispatch(self, request, *args, **kwargs):
        article_id = self.kwargs.get('article_id') or self.kwargs.get('pk')
        self.article = get_object_or_404(Article, pk=article_id)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class(article=self.article)
        context = {
            'form': form,
            'page_title': self.page_title,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)

    def log_addition_serial_numbers(self, obj, quant, serial_nums):
        # Get the name of the newly added object
        object_name = str(obj)
        # Log the addition of the object
        log_message = f"{quant} new {object_name} added: {serial_nums} by {self.request.user} from IP {self.request.META.get('REMOTE_ADDR')}"
        # Log the information to the database
        log_to_database('INFO', log_message, user=self.request.user)
    
    def log_addition_no_serial_numbers(self, obj, quant):
        # Get the name of the newly added object
        object_name = str(obj)
        # Log the addition of the object
        log_message = f"{quant} new {object_name} added by {self.request.user} from IP {self.request.META.get('REMOTE_ADDR')}"
        # Log the information to the database
        log_to_database('INFO', log_message, user=self.request.user)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, article=self.article)
        if form.is_valid():
            serial_number = form.cleaned_data.get('serial_number')
            quantity = form.cleaned_data.get('quantity')
            storage_location = form.cleaned_data.get('storage_location')
            storage_sublocation = form.cleaned_data.get('storage_sublocation')
            assigned_project = form.cleaned_data.get('assigned_project')
            assigned_subproject = form.cleaned_data.get('assigned_subproject')
            assigned_assembly = form.cleaned_data.get('assigned_assembly')

            created_items = []
            try:
                with transaction.atomic():
                    if serial_number:
                        serial_numbers = [s.strip() for s in serial_number.split(',') if s.strip()]
                        num_serial_numbers = len(serial_numbers)

                        # Create items with serial numbers
                        for sn in serial_numbers:
                            item = Item(
                                article=self.article,
                                serial_number=sn,
                                quantity=1,
                                storage_location=storage_location,
                                storage_sublocation=storage_sublocation,
                                assigned_project=assigned_project,
                                assigned_subproject=assigned_subproject,
                                assigned_assembly=assigned_assembly,
                                modified_by=request.user.username,
                            )
                            item.full_clean()
                            item.save()
                            created_items.append(item)

                        # Remaining quantity
                        remaining_quantity = quantity - num_serial_numbers
                        if remaining_quantity > 0:
                            # Check for existing bulk item
                            existing_item = Item.objects.filter(
                                article=self.article,
                                serial_number__isnull=True,
                                storage_location=storage_location,
                                storage_sublocation=storage_sublocation,
                                assigned_project=assigned_project,
                                assigned_subproject=assigned_subproject,
                                assigned_assembly=assigned_assembly
                            ).first()

                            if existing_item:
                                # Update the quantity of the existing item
                                existing_item.quantity += remaining_quantity
                                existing_item.modified_by=request.user.username
                                existing_item.full_clean()
                                existing_item.save()
                            else:
                                # Create new bulk item
                                item = Item(
                                    article=self.article,
                                    quantity=remaining_quantity,
                                    storage_location=storage_location,
                                    storage_sublocation=storage_sublocation,
                                    assigned_project=assigned_project,
                                    assigned_subproject=assigned_subproject,
                                    assigned_assembly=assigned_assembly,
                                    modified_by=request.user.username,
                                )
                                item.full_clean()
                                item.save()
                                created_items.append(item)
                        self.log_addition_serial_numbers(self.article, quantity, serial_numbers)
                    else:
                        # Handle bulk item when no serial numbers are provided
                        existing_item = Item.objects.filter(
                            article=self.article,
                            serial_number__isnull=True,
                            storage_location=storage_location,
                            storage_sublocation=storage_sublocation,
                            assigned_project=assigned_project,
                            assigned_subproject=assigned_subproject,
                            assigned_assembly=assigned_assembly
                        ).first()

                        if existing_item:
                            # Update the quantity of the existing item
                            existing_item.quantity += quantity
                            existing_item.modified_by=request.user.username
                            existing_item.full_clean()
                            existing_item.save()
                        else:
                            # Create new bulk item
                            item = Item(
                                article=self.article,
                                quantity=quantity,
                                storage_location=storage_location,
                                storage_sublocation=storage_sublocation,
                                assigned_project=assigned_project,
                                assigned_subproject=assigned_subproject,
                                assigned_assembly=assigned_assembly,
                                modified_by=request.user.username,
                            )
                            item.full_clean()
                            item.save()
                            created_items.append(item)
                        self.log_addition_no_serial_numbers(self.article,quantity)
            except ValidationError as e:
                user = request.user if request.user.is_authenticated else None
                log_message = f"ItemAddView accessed by {user}: {e}"
                log_to_database('ERROR', log_message,user=user)
                form.add_error(None, e)
                context = {
                    'form': form,
                    'page_title': self.page_title,
                    'cancel_url': self.get_cancel_url(),
                }
                return render(request, self.template_name, context)

            # Handle redirection based on action
            action = request.POST.get('action')
            if action == 'save_and_exit':
                return redirect(self.get_success_url())
            elif action == 'save_and_create_another':
                return redirect(self.request.get_full_path())
            else:
                return redirect(self.get_success_url())
        else:
            # Form is invalid, render with errors
            context = {
                'form': form,
                'page_title': self.page_title,
                'cancel_url': self.get_cancel_url(),
            }
            return render(request, self.template_name, context)

    def get_success_url(self):
        next_url = self.request.POST.get('next') or self.request.GET.get('next')
        if next_url:
            return next_url
        else:
            return reverse_lazy('components:article_stock', kwargs={'pk': self.article.pk})

    def get_cancel_url(self):
        next_url = self.request.POST.get('next') or self.request.GET.get('next')
        if next_url:
            return next_url
        else:
            return reverse_lazy('components:article_stock', kwargs={'pk': self.article.pk})

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(ItemAddView, self).form_valid(form)


class ItemUpdateView(GenericUpdateView):
    model = Item
    form_class = ItemFormHiddenArticle
    page_title = "Edit Item"
    success_url_name = 'components:all_items_list'
    cancel_url_name = 'components:all_items_list'
    template_name = 'components/item_edit.html'

    def log_update_changes(self, original_data, updated_instance):
        """
        Logs changes made to an item during update.
        Only logs fields that have changed, excluding certain fields if needed.
        """
        changes = []
        for field, original_value in original_data.items():
            updated_value = getattr(updated_instance, field, None)

            # Skip logging for fields that should not be tracked
            if field == 'last_changed_date':
                continue

            if original_value != updated_value:
                changes.append(f"{field} updated from '{original_value}' to '{updated_value}'")

        if changes:
            change_message = "; ".join(changes)
            log_message = f"Item update by {self.request.user}: {change_message} from IP {self.request.META.get('REMOTE_ADDR')}"
            log_to_database('INFO', log_message, user=self.request.user)

    def form_valid(self, form):
        # Retrieve the original state of the object before saving
        self.object = self.get_object()
        original_data = {field.name: getattr(self.object, field.name) for field in self.object._meta.fields}

        # Save the updated object with the request for fields like `updated_by`
        obj = form.save(commit=False)
        obj.save(request=self.request)  # Custom save using request
        self.object = obj  # Update self.object to reflect the saved instance

        # Log any changes made to the object
        self.log_update_changes(original_data, form.instance)

        # Handle deletion of documents if requested
        for document in form.instance.documents.all():
            if self.request.POST.get(f'delete_document_{document.pk}'):
                document.delete()
                # Optionally log document deletion
                log_to_database('INFO', f"Document {document.pk} deleted by {self.request.user}", user=self.request.user)

        # Redirect based on the action chosen by the user
        action = self.request.POST.get('action')
        if action == 'save_and_exit':
            return redirect(self.get_success_url())
        elif action == 'save_and_create_another':
            return redirect(self.request.get_full_path())

        # Default response in case no specific action is set
        return redirect(self.get_success_url())


class ItemDeleteView(GenericDeleteView):
    model = Item
    page_title = "Delete Item"
    success_url_name = 'components:all_items_list'
    cancel_url_name = 'components:all_items_list'


class ArticleStockView(ItemListView):
    filterset_class = ItemStockFilter
    template_name = 'components/article_stock_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        article_id = self.kwargs.get('pk')
        # Fetch the article using the ID
        article = get_object_or_404(Article, pk=article_id)
        context['article'] = article
        
        # Build the 'Add Item' button
        context['add_button_url'] = reverse_lazy('components:item_add', kwargs={'article_id': article_id})
        context['add_button_label'] = 'Add Item'
        # Export Button
        context['export_button_url'] = reverse_lazy('components:article_items_export', kwargs={'article_id': article_id})
        context['export_button_label'] = 'Export'
        
        return context

    def get_queryset(self):
        # Get the article ID from the URL
        article_id = self.kwargs.get('pk')
        # Filter items based on the article ID
        return Item.objects.filter(article_id=article_id)


class AllItemsView(GenericTableView):
    model = Item
    table_class = ItemTable
    filterset_class = AllItemsFilter

    """def get_queryset(self):
        # Return all items and their related articles
        return Item.objects.select_related('article')"""
    def get_queryset(self):
        # Initialize the filter with request GET parameters and the base queryset
        filterset = AllItemsFilter(self.request.GET, queryset=Item.objects.select_related('article'))
        
        # Ensure the queryset respects the filters applied in the filterset
        if filterset.is_valid():
            return filterset.qs

        # Return the base queryset if the filter is invalid
        return Item.objects.none()

    
    def get_table_kwargs(self):
        kwargs = super().get_table_kwargs()
        # Disable checkboxes in this particular view
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'item',
                        'app_name': 'core' 
                    })
                },
            ],
            bulk_actions=[
                {
                    'name': 'Transfer Selected',
                    'class': 'btn btn-primary',
                    'bulk_action_url': reverse_lazy('components:bulk_transfer'),
                    'method': 'GET', 
                }
                ],
            **kwargs
        )
        
        context['export_button_url'] = reverse_lazy('components:item_export')
        
        return context


class ItemExportView(GenericExportView):
    model = Item
    filterset_class = AllItemsFilter
    table_class = AllItemsTable


class BulkTransferView(LoginRequiredMixin,View):
    form_class = BulkTransferForm
    template_name = 'components/bulk_transfer.html'
    login_url = '/login'
    
    def log_bulk_transfer_changes(self, items, updates):
        """
        Logs changes for each item in the bulk transfer. Only logs fields that were changed.
        """
        for item in items:
            changes = []
            for field, new_value in updates.items():
                original_value = getattr(item, field, None)

                if original_value != new_value:
                    changes.append(f"{field} changed from '{original_value}' to '{new_value}'")
            
            if changes:
                change_message = "; ".join(changes)
                log_message = (
                    f"Bulk transfer update for Item of Article {item.article.name} by {self.request.user} from IP "
                    f"{self.request.META.get('REMOTE_ADDR')}: {change_message}"
                )
                log_to_database('INFO', log_message, user=self.request.user)

    def get_success_url(self):
        next_url = self.request.POST.get('next') or self.request.GET.get('next')
        if next_url:
            # Remove 'selection' parameters from the next_url
            parsed_url = urlparse(unquote(next_url))
            query_params = parse_qs(parsed_url.query)
            query_params.pop('selection', None)
            filtered_query = urlencode(query_params, doseq=True)
            filtered_url = urlunparse((
                parsed_url.scheme, parsed_url.netloc, parsed_url.path,
                parsed_url.params, filtered_query, parsed_url.fragment
            ))
            return filtered_url
        else:
            return reverse_lazy('components:item_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next') or self.request.META.get('HTTP_REFERER', '')
        if next_url:
            # Remove 'selection' parameters from the next_url
            parsed_url = urlparse(unquote(next_url))
            query_params = parse_qs(parsed_url.query)
            query_params.pop('selection', None)
            filtered_query = urlencode(query_params, doseq=True)
            filtered_url = urlunparse((
                parsed_url.scheme, parsed_url.netloc, parsed_url.path,
                parsed_url.params, filtered_query, parsed_url.fragment
            ))
            return filtered_url
        else:
            return reverse_lazy('components:item_list')

    def get(self, request, *args, **kwargs):
        selected_ids = request.GET.getlist('selection')
        if not selected_ids:
            messages.error(request, "No items selected for bulk transfer.")
            return redirect(reverse_lazy('components:item_list'))

        if len(selected_ids) == 1 and ',' in selected_ids[0]:
            selected_ids = selected_ids[0].split(',')
        try:
            # Convert the selection to a list of integers
            selected_ids = [int(id) for id in selected_ids]
        except ValueError:
            messages.error(request, "Invalid item IDs provided.")
            return redirect(reverse_lazy('components:item_detail'))

        # Retrieve items based on selected IDs
        items = Item.objects.filter(pk__in=selected_ids)
        self.items = items

        if not items.exists():
            messages.error(request, "No valid items found.")
            return redirect(reverse_lazy('components:item_list'))

        form = self.form_class()
        context = {
            'form': form,
            'items': items,
            'selected_ids': selected_ids,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        selected_ids = request.POST.getlist('selection')
        
        if len(selected_ids) == 1 and ',' in selected_ids[0]:
                selected_ids = selected_ids[0].split(',')
        try:
            # Convert the selection to a list of integers
            selected_ids = [int(id) for id in selected_ids]
        except ValueError:
            messages.error(request, "Invalid item IDs provided.")
            return redirect(reverse_lazy('components:item_detail'))

        items = Item.objects.filter(pk__in=selected_ids)
        self.items = items

        if not items.exists():
            messages.error(request, "No valid items found.")
            return redirect(reverse_lazy('components:item_list'))

        if form.is_valid():
            project = form.cleaned_data.get('assigned_project')
            subproject = form.cleaned_data.get('assigned_subproject')
            assembly = form.cleaned_data.get('assigned_assembly')
            location = form.cleaned_data.get('storage_location')
            sublocation = form.cleaned_data.get('storage_sublocation')

            if form.is_valid():
                updates = {
                    'assigned_project': form.cleaned_data.get('assigned_project'),
                    'assigned_subproject': form.cleaned_data.get('assigned_subproject'),
                    'assigned_assembly': form.cleaned_data.get('assigned_assembly'),
                    'storage_location': form.cleaned_data.get('storage_location'),
                    'storage_sublocation': form.cleaned_data.get('storage_sublocation'),}

            try:
                with transaction.atomic():
                    # Update all items with the new attributes
                    self.log_bulk_transfer_changes(items, updates)
                    items.update(
                        assigned_project=project,
                        assigned_subproject=subproject,
                        assigned_assembly=assembly,
                        storage_location=location,
                        storage_sublocation=sublocation,
                        modified_by=request.user.username  # Set modified_by explicitly
                    )
                    messages.success(request, f"Successfully transferred {items.count()} items.")
                    return redirect(self.get_success_url())

            except Exception as e:
                user = request.user if request.user.is_authenticated else None
                log_message = f"BulkTransferView accessed by {user}: {e}"
                log_to_database('ERROR', log_message,user=user)
                messages.error(request, f"An error occurred during the transfer: {e}")

        context = {
            'form': form,
            'items': items,
            'selected_ids': selected_ids,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)


class ItemSplitView(LoginRequiredMixin,View):
    form_class = ItemSplitForm
    template_name = 'components/item_split.html'
    login_url = '/login'

    def dispatch(self, request, *args, **kwargs):
        self.item = get_object_or_404(Item, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class(item=self.item)
        context = {
            'form': form,
            'item': self.item,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)
    
    def log_item_split(self, new_items, remaining_quantity):
        """
        Logs the creation of new items during a split and the update or deletion of the original item.
        """
        for new_item in new_items:
            log_message = (
                f"New item created with Serial Number: '{new_item.serial_number or 'N/A'}', "
                f"Quantity: {new_item.quantity}, created by {self.request.user} from IP {self.request.META.get('REMOTE_ADDR')}"
            )
            log_to_database('INFO', log_message, user=self.request.user)

        if remaining_quantity > 0:
            log_message = (
                f"Original item of Article {self.item.article.name} updated with remaining quantity {remaining_quantity} by "
                f"{self.request.user} from IP {self.request.META.get('REMOTE_ADDR')}"
            )
        else:
            log_message = (
                f"Original item of Article {self.item.article.name} deleted after split by {self.request.user} from IP "
                f"{self.request.META.get('REMOTE_ADDR')}"
            )
        log_to_database('INFO', log_message, user=self.request.user)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, item=self.item)
        if form.is_valid():
            serial_number = form.cleaned_data.get('serial_number')
            quantity = form.cleaned_data.get('quantity')
            new_item_ids = []  # Track new item IDs for transfer
            new_items = []
            num_serial_numbers = 0
            try:
                with transaction.atomic():
                    # Initialize the remaining quantity to be adjusted
                    remaining_quantity = Decimal(self.item.quantity or 0)

                    # If serial numbers are provided
                    if serial_number:
                        serial_numbers = [s.strip() for s in serial_number.split(',') if s.strip()]
                        num_serial_numbers = len(serial_numbers)

                        # Split items by serial number
                        for sn in serial_numbers:
                            try:
                                new_item = Item(
                                    article=self.item.article,
                                    serial_number=sn,
                                    quantity=Decimal(1),  # Serial numbers always have quantity of 1
                                    storage_location=self.item.storage_location,
                                    storage_sublocation=self.item.storage_sublocation,
                                    assigned_project=self.item.assigned_project,
                                    assigned_subproject=self.item.assigned_subproject,
                                    assigned_assembly=self.item.assigned_assembly,
                                    modified_by=request.user.username  # Set modified_by explicitly
                                )
                                new_item.save()
                                new_item_ids.append(new_item.id)  # Append the new item's ID for transfer
                                new_items.append(new_item)
                            except IntegrityError:
                                form.add_error('serial_number', f"A duplicate item with serial number '{sn}' already exists.")
                                return self.render_form_with_errors(form)

                        # Subtract serial numbers quantity from the remaining quantity
                        remaining_quantity -= Decimal(num_serial_numbers)

                    # Handle bulk items if there is any remaining quantity
                    bulk_quantity = Decimal(quantity) - Decimal(num_serial_numbers)  # Remaining bulk quantity
                    if bulk_quantity > 0:
                        new_bulk_item = Item(
                            article=self.item.article,
                            quantity=bulk_quantity,
                            storage_location=self.item.storage_location,
                            storage_sublocation=self.item.storage_sublocation,
                            assigned_project=self.item.assigned_project,
                            assigned_subproject=self.item.assigned_subproject,
                            assigned_assembly=self.item.assigned_assembly,
                            modified_by=request.user.username  # Set modified_by explicitly
                        )
                        new_bulk_item.save()
                        new_item_ids.append(new_bulk_item.id)  # Append the new bulk item's ID for transfer
                        new_items.append(new_bulk_item)
                        remaining_quantity -= bulk_quantity

                    # Update the original item’s quantity or delete it if the remaining quantity is 0
                    if remaining_quantity > 0:
                        self.item.quantity = remaining_quantity
                        self.item.modified_by = request.user.username  # Update modified_by for original item
                        self.item.save()
                    else:
                        self.item.delete()

                    self.log_item_split(new_items, remaining_quantity)

                    # Handle Split and Transfer functionality
                    if 'split_and_transfer' in request.POST:
                        # Assume new_item_ids is a list of integers obtained from form processing
                        transfer_url = reverse('components:bulk_transfer')
                        selection_str = ','.join(map(str, new_item_ids))
                        transfer_url += f"?selection={selection_str}"

                        # Process the 'next' parameter if it exists
                        next_param = request.GET.get('next')
                        if next_param:
                            parsed_next = urlparse(next_param)
                            query_params = parse_qs(parsed_next.query)
                            back_url = query_params.get('back_url', [None])[0]
                            if back_url:
                                encoded_back_url = quote(quote(back_url, safe=''), safe='')
                                query_params['back_url'] = [encoded_back_url]
                                new_query = urlencode(query_params, doseq=True)
                                new_next_param = urlunparse((parsed_next.scheme, parsed_next.netloc, parsed_next.path, parsed_next.params, new_query, parsed_next.fragment))
                            else:
                                new_next_param = next_param
                            encoded_next = quote(new_next_param, safe='')
                            transfer_url += f"&next={encoded_next}"
                        
                        log_to_database('DEBUG', f"Redirecting to bulk transfer with item IDs {selection_str} by {self.request.user}", user=self.request.user)
                        return redirect(transfer_url)

                    # Success message for split only
                    messages.success(request, "Items split successfully.")
                    return redirect(self.get_success_url())

            except IntegrityError as e:
                user = request.user if request.user.is_authenticated else None
                log_message = f"ItemSplitView accessed by {user}: {e}"
                log_to_database('ERROR', log_message,user=user)
                form.add_error(None, f"An error occurred while processing: {str(e)}")
                return self.render_form_with_errors(form)

        return self.render_form_with_errors(form)

    def render_form_with_errors(self, form):
        """
        Utility function to re-render the form with errors.
        """
        context = {
            'form': form,
            'item': self.item,
            'cancel_url': self.get_cancel_url(),
        }
        return render(self.request, self.template_name, context)

    def get_success_url(self):
        next_url = self.request.POST.get('next') or self.request.GET.get('next')
        if next_url:
            return next_url
        else:
            return reverse_lazy('components:article_stock', kwargs={'pk': self.item.article.pk})

    def get_cancel_url(self):
        next_url = self.request.POST.get('next') or self.request.GET.get('next')
        if next_url:
            return next_url
        else:
            return reverse_lazy('components:article_stock', kwargs={'pk': self.item.article.pk})


class ItemDetailView(DetailView):
    model = Item
    template_name = 'components/item_detail.html'
    context_object_name = 'item'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Add the article related to the item
        context['article'] = self.object.article
        
        # Add associated documents
        context['documents'] = self.object.documents.all()
        
        # Add back_url to context
        back_url = self.request.GET.get('next', self.request.META.get('HTTP_REFERER', '/'))
        context['back_url'] = back_url
        
        return context

# Ordering view
# views/component_views.py



class ItemOrderView(LoginRequiredMixin, View):
    template_name = 'components/item_order.html'
    form_class = OrderForm
    login_url = '/login'

    def dispatch(self, request, *args, **kwargs):
        # Log access
        user = request.user if request.user.is_authenticated else None
        log_message = f"ItemOrderView accessed by {user} from IP {request.META.get('REMOTE_ADDR')}"
        log_to_database('INFO', log_message, user=user)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk, *args, **kwargs):
        """
        Display the item, its related article, all orders, plus a form to create an order.
        """
        item = get_object_or_404(Item, pk=pk)
        orders = item.order.all().order_by('-date')  # Show newest first

        form = self.form_class()
        context = {
            'item': item,
            'article': item.article,
            'orders': orders,
            'form': form,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)

    def post(self, request, pk, *args, **kwargs):
        """
        Handle form submission to create a new order for this item.
        """
        item = get_object_or_404(Item, pk=pk)
        orders = item.order.all().order_by('-date')

        form = self.form_class(request.POST)
        if form.is_valid():
            new_order = form.save(commit=False)
            new_order.item = item
            new_order.modified_by = request.user.username  # optional tracking

            # Wrap in atomic transaction for safety
            try:
                with transaction.atomic():
                    new_order.save()

                    # 1) Show user success feedback
                    messages.success(request, f"Order placed successfully (Qty: {new_order.quantity}).")

                    # 2) Log creation
                    log_message = (
                        f"Created order for Item #{item.pk} ({item.article.name}), "
                        f"Quantity: {new_order.quantity}, by {request.user}."
                    )
                    log_to_database('INFO', log_message, user=request.user)

                    # 3) Send email to brara@mpe.mpg.de
                    self.send_order_email(item, new_order)

                # 4) Redirect back to the order page
                return redirect('components:item_order', pk=item.pk)

            except Exception as e:
                messages.error(request, f"Error placing order: {e}")

        # If form invalid or if an error occurred, re-render
        context = {
            'item': item,
            'article': item.article,
            'orders': orders,
            'form': form,
            'cancel_url': self.get_cancel_url(),
        }
        return render(request, self.template_name, context)

    def send_order_email(self, item, order):
        """
        Builds and sends an email containing details about the newly created order.
        """
        article = item.article
        subject = f"New Order Created for Article: {article.name}"

        # Build a message with relevant details
        message = (
            f"An order has been created for:\n"
            f"Article: {article.name}\n"
            f"Item Serial Number: {item.serial_number or '(none)'}\n"
            f"Location: {item.storage_location.name if item.storage_location else '(none)'}"
        )
        # If sublocation/drawer exist, append them
        if item.storage_sublocation:
            message += f" / {item.storage_sublocation.name}"
        if item.storage_drawer:
            message += f" / {item.storage_drawer.name}"

        # Assigned project, subproject, assembly (if present)
        if item.assigned_project:
            message += f"\nProject: {item.assigned_project.name}"
        if item.assigned_subproject:
            message += f"\nSubproject: {item.assigned_subproject.name}"
        if item.assigned_assembly:
            message += f"\nAssembly: {item.assigned_assembly.name}"

        # Include the newly ordered quantity
        message += (
            f"\n\nOrdered Quantity: {order.quantity}\n"
            f"Modified By: {order.modified_by or 'Unknown'}"
        )

        # Optionally, link to the item detail page
        item_detail_url = self.request.build_absolute_uri(
            reverse_lazy('components:item_detail', kwargs={'pk': item.pk})
        )
        message += f"\n\nItem Detail: {item_detail_url}"

        recipient_list = settings.RECIPIENT_LIST

        try:
            send_mail(
                subject,
                message,
                settings.DEFAULT_FROM_EMAIL,  
                recipient_list,
                fail_silently=False,
            )
            # Log email sending
            log_to_database('INFO', f"Order email sent to {recipient_list}", user=self.request.user)
        except Exception as e:
            # If email fails, we log an error (and possibly show user a warning).
            log_to_database('ERROR', f"Failed to send order email: {e}", user=self.request.user)

    def get_cancel_url(self):
        """
        If there's a 'next' param, use it. Otherwise, go back to item detail or anywhere you prefer.
        """
        next_url = self.request.GET.get('next') or self.request.POST.get('next')
        if next_url:
            return next_url
        return reverse_lazy('components:item_detail', kwargs={'pk': self.kwargs['pk']})


class OrderDeleteView(LoginRequiredMixin, View):
    """
    Deletes an Order if it belongs to the specified Item.
    """
    login_url = '/login'

    def post(self, request, item_pk, order_pk):
        item = get_object_or_404(Item, pk=item_pk)
        order = get_object_or_404(Order, pk=order_pk, item=item)

        try:
            with transaction.atomic():
                order.delete()
            messages.success(request, f"Order #{order_pk} was deleted.")
            # Log the action
            log_message = f"User {request.user} deleted order #{order_pk} for item #{item_pk}"
            log_to_database('INFO', log_message, user=request.user)

        except Exception as e:
            messages.error(request, f"Error deleting order: {e}")
            log_to_database('ERROR', f"Error deleting order #{order_pk}: {e}", user=request.user)

        return redirect('components:item_order', pk=item_pk)


class OrderFinishView(LoginRequiredMixin, View):
    """
    Finishes an order: adds the order’s quantity to the item’s quantity, then deletes the order.
    """
    login_url = '/login'

    def post(self, request, item_pk, order_pk):
        item = get_object_or_404(Item, pk=item_pk)
        order = get_object_or_404(Order, pk=order_pk, item=item)

        try:
            with transaction.atomic():
                # Add the order quantity to the item’s quantity
                item.quantity += order.quantity
                item.modified_by = request.user.username
                item.save()

                # Then remove the order
                order.delete()

            messages.success(request, 
                f"Order #{order_pk} finished. Added quantity {order.quantity} to the item.")
            # Log the action
            log_message = (f"User {request.user} finished order #{order_pk}: "
                           f"added {order.quantity} to item #{item_pk}")
            log_to_database('INFO', log_message, user=request.user)

        except Exception as e:
            messages.error(request, f"Error finishing order: {e}")
            log_to_database('ERROR', f"Error finishing order #{order_pk}: {e}", user=request.user)

        return redirect('components:item_order', pk=item_pk)
    
class ItemQRDownloadView(LoginRequiredMixin, View):
    """
    Generates a QR code for the Item Detail page URL and returns a PNG file download.
    """
    login_url = '/login'

    def get(self, request, pk, *args, **kwargs):
        # 1) Look up the item
        item = get_object_or_404(Item, pk=pk)

        # 2) Build the absolute URL to the item's detail page
        #    e.g., https://example.com/items/123/
        item_url = request.build_absolute_uri(
            reverse('components:item_detail', args=[item.pk])
        )

        # 3) Generate the QR code using the 'qrcode' library
        qr_img = qrcode.make(item_url)

        # 4) Save the QR code image to an in-memory buffer as PNG
        buffer = io.BytesIO()
        qr_img.save(buffer, format='PNG')
        buffer.seek(0)  # reset file pointer to start

        # 5) Create an HTTP response with the image data
        #    and force it as an attachment download
        filename = f"item_qr_{item.pk}.png"
        response = HttpResponse(buffer, content_type='image/png')
        response['Content-Disposition'] = f'attachment; filename="{filename}"'

        # Optional: log the download action
        log_message = (f"User {request.user} generated QR code download "
                       f"for Item #{item.pk} from IP {request.META.get('REMOTE_ADDR')}")
        log_to_database('INFO', log_message, user=request.user)

        return response