from core.models.component_models import Article, Item
from core.models.vendor_models import Manufacturer, Supplier
from core.models.project_models import Project, Subproject, Assembly
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.filters import autocomplete_field

from django.urls import reverse_lazy
import django_filters
from django.db.models import Q
from django import forms


# ArticleFilter class: Provides filtering options for Article objects.
# Includes general search functionality and specific field-based filters for attributes such as
# manufacturer name, type description, inventory number, and catalog number.
class ArticleFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    name = autocomplete_field(
        'name',
        'Article'
    )
    manufacturer__name = autocomplete_field(
        'manufacturer__name',
        'Manufacturer',
        autocomplete_field_name='name'
    )
    type_description = autocomplete_field(
        'type_description',
        'Article',
        placeholder='Type to search type description...'
    )
    inventory_number = autocomplete_field(
        'inventory_number',
        'Article',
        placeholder='Type to search inventory number...'
    )
    catalog_number = autocomplete_field(
        'catalog_number',
        'Article',
        placeholder='Type to search catalog number...'
    )
    description = autocomplete_field(
        'description',
        'Article',
        placeholder='Type to search description...'
    )

    def filter_search(self, queryset, name, value):
        # Custom search logic: Filters articles by multiple attributes using a case-insensitive substring match.
        return queryset.filter(
            Q(name__icontains=value) |
            Q(manufacturer__name__icontains=value) |
            Q(type_description__icontains=value) |
            Q(inventory_number__icontains=value) |
            Q(catalog_number__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = Article
        fields = [
            'name',
            'manufacturer__name',
            'type_description',
            'inventory_number',
            'catalog_number',
            'description'
        ]


# ItemFilter class: Provides filtering options for Item objects.
# Allows detailed filtering through related fields such as article names, serial numbers, storage locations, 
# assigned projects, and assigned assemblies.
class ItemFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )
    article__name = autocomplete_field(
        'article__name',
        'Article',
        autocomplete_field_name='name',
        placeholder='Type to search article name...'
    )
    serial_number = autocomplete_field(
        'serial_number',
        'Item',
        placeholder='Type to search serial number...'
    )
    quantity = django_filters.NumberFilter(
        field_name='quantity',
        lookup_expr='exact',
        widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Quantity'})
    )
    storage_location__name = autocomplete_field(
        'storage_location__name',
        'StorageLocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage location...'
    )
    storage_sublocation__name = autocomplete_field(
        'storage_sublocation__name',
        'StorageSublocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage sublocation...',
        depends_on='StorageLocation_name'
    )
    storage_drawer__name = autocomplete_field(
        'storage_drawer__name',
        'StorageDrawer',
        autocomplete_field_name='name',
        placeholder='Type to search storage drawer...',
        depends_on='StorageSublocation_name'
    )
    assigned_project__name = autocomplete_field(
        'assigned_project__name',
        'Project', autocomplete_field_name='name',
        placeholder='Type to search project...'
    )
    assigned_subproject__name = autocomplete_field(
        'assigned_subproject__name',
        'Subproject',
        autocomplete_field_name='name',
        placeholder='Type to search subproject...',
        depends_on='Project_name'
    )
    assigned_assembly__name = autocomplete_field(
        'assigned_assembly__name',
        'Assembly',
        autocomplete_field_name='name',
        placeholder='Type to search assembly...',
        depends_on='Subproject_name'
    )

    def filter_search(self, queryset, name, value):
        # Custom search logic: Filters items by various related fields such as article names, storage locations,
        # assigned projects, and serial numbers using a case-insensitive substring match.
        return queryset.filter(
            Q(article__name__icontains=value) |
            Q(serial_number__icontains=value) |
            Q(quantity__icontains=value) |
            Q(storage_location__name__icontains=value) |
            Q(storage_sublocation__name__icontains=value) |
            Q(storage_drawer__name__icontains=value) |
            Q(assigned_project__name__icontains=value) |
            Q(assigned_subproject__name__icontains=value) |
            Q(assigned_assembly__name__icontains=value)
        )

    class Meta:
        model = Item
        fields = [
            'article__name', 'serial_number', 'quantity', 'storage_location__name',
            'storage_sublocation__name', 'storage_drawer__name', 'assigned_project__name',
            'assigned_subproject__name', 'assigned_assembly__name', 'last_changed_date'
        ]


# ItemStockFilter class: Specialized filter for stock-related attributes of Item objects.
# Includes filters for serial numbers, quantities, storage locations, and project assignments.
class ItemStockFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    serial_number = autocomplete_field(
        'serial_number',
        'Item',
        placeholder='Type to search serial number...'
        )
    quantity = django_filters.NumberFilter(
        field_name='quantity',
        lookup_expr='exact', widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Quantity'})
    )
    storage_location__name = autocomplete_field(
        'storage_location__name',
        'StorageLocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage location...'
    )
    storage_sublocation__name = autocomplete_field(
        'storage_sublocation__name',
        'StorageSublocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage sublocation...',
        depends_on='StorageLocation_name'
    )
    storage_drawer__name = autocomplete_field(
        'storage_drawer__name',
        'StorageDrawer',
        autocomplete_field_name='name',
        placeholder='Type to search storage drawer...',
        depends_on='StorageSublocation_name'
    )
    assigned_project__name = autocomplete_field(
        'assigned_project__name',
        'Project',
        autocomplete_field_name='name',
        placeholder='Type to search project...'
    )
    assigned_subproject__name = autocomplete_field(
        'assigned_subproject__name',
        'Subproject',
        autocomplete_field_name='name',
        placeholder='Type to search subproject...',
        depends_on='Project_name'
    )
    assigned_assembly__name = autocomplete_field(
        'assigned_assembly__name',
        'Assembly', autocomplete_field_name='name',
        placeholder='Type to search assembly...',
        depends_on='Subproject_name'
    )

    def filter_search(self, queryset, name, value):
        # Custom search logic: Filters items by attributes such as storage locations, quantities, 
        # and serial numbers using a case-insensitive substring match.
        return queryset.filter(
            Q(article__name__icontains=value) |
            Q(serial_number__icontains=value) |
            Q(quantity__icontains=value) |
            Q(storage_location__name__icontains=value) |
            Q(storage_sublocation__name__icontains=value) |
            Q(storage_drawer__name__icontains=value) |
            Q(assigned_project__name__icontains=value) |
            Q(assigned_subproject__name__icontains=value) |
            Q(assigned_assembly__name__icontains=value)
        )

    class Meta:
        model = Item
        fields = [
            'serial_number', 'quantity', 'storage_location__name', 'storage_sublocation__name',
            'storage_drawer__name', 'assigned_project__name', 'assigned_subproject__name',
            'assigned_assembly__name',
        ]


# AllItemsFilter class: A comprehensive filter for all Item-related attributes, 
# including relationships to Articles, Storage, and Projects.
class AllItemsFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    # Hidden fields for filtering by IDs
    article__manufacturer = django_filters.ModelChoiceFilter(
        queryset=Manufacturer.objects.all(),
        field_name='article__manufacturer',
        label='Manufacturer',
        widget=forms.HiddenInput(),
    )
    assigned_project = django_filters.ModelChoiceFilter(
        queryset=Project.objects.all(),
        field_name='assigned_project',
        label='Project',
        widget=forms.HiddenInput(),
    )
    assigned_subproject = django_filters.ModelChoiceFilter(
        queryset=Subproject.objects.all(),
        field_name='assigned_subproject',
        label='Subproject',
        widget=forms.HiddenInput(),
    )
    assigned_assembly = django_filters.ModelChoiceFilter(
        queryset=Assembly.objects.all(),
        field_name='assigned_assembly',
        label='Assembly',
        widget=forms.HiddenInput(),
    )
    storage_location = django_filters.ModelChoiceFilter(
        queryset=StorageLocation.objects.all(),
        field_name='storage_location',
        label='Storage Location',
        widget=forms.HiddenInput(),
    )
    storage_sublocation = django_filters.ModelChoiceFilter(
        queryset=StorageSublocation.objects.all(),
        field_name='storage_sublocation',
        label='Storage Sublocation',
        widget=forms.HiddenInput(),
    )
    storage_drawer = django_filters.ModelChoiceFilter(
        queryset=StorageDrawer.objects.all(),
        field_name='storage_drawer',
        label='Storage Drawer',
        widget=forms.HiddenInput(),
    )

    # Article fields
    article__name = autocomplete_field(
        'article__name', 'Article',
        autocomplete_field_name='name',
        placeholder='Type to search article name...'
    )
    article__manufacturer__name = autocomplete_field(
        'article__manufacturer__name',
        'Manufacturer',
        autocomplete_field_name='name',
        placeholder='Type to search manufacturer...'
    )
    article__type_description = autocomplete_field(
        'article__type_description',
        'Article',
        placeholder='Type to search type description...'
    )
    article__inventory_number = autocomplete_field(
        'article__inventory_number',
        'Article',
        placeholder='Type to search inventory number...'
    )
    article__catalog_number = autocomplete_field(
        'article__catalog_number',
        'Article',
        placeholder='Type to search catalog number...'
    )

    article__suppliers__name = autocomplete_field(
        'article__suppliers__name',
        'Supplier',  # Adjust the model name as needed
        autocomplete_field_name='name',
        placeholder='Type to search suppliers...'
    )

    # Item fields
    serial_number = autocomplete_field(
        'serial_number',
        'Item',
        placeholder='Type to search serial number...'
    )
    quantity = django_filters.NumberFilter(
        field_name='quantity', lookup_expr='exact',
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': 'Quantity'
        })
    )
    storage_location__name = autocomplete_field(
        'storage_location__name',
        'StorageLocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage location...'
    )
    storage_sublocation__name = autocomplete_field(
        'storage_sublocation__name',
        'StorageSublocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage sublocation...',
        depends_on='StorageLocation_name'
    )
    storage_drawer__name = autocomplete_field(
        'storage_drawer__name',
        'StorageDrawer',
        autocomplete_field_name='name',
        placeholder='Type to search storage drawer...',
        depends_on='StorageSublocation_name'
    )
    assigned_project__name = autocomplete_field(
        'assigned_project__name',
        'Project',
        autocomplete_field_name='name',
        placeholder='Type to search project...'
    )
    assigned_subproject__name = autocomplete_field(
        'assigned_subproject__name',
        'Subproject',
        autocomplete_field_name='name',
        placeholder='Type to search subproject...',
        depends_on='Project_name'
    )
    assigned_assembly__name = autocomplete_field(
        'assigned_assembly__name',
        'Assembly',
        autocomplete_field_name='name',
        placeholder='Type to search assembly...',
        depends_on='Subproject_name'
    )

    def filter_search(self, queryset, name, value):
        # Custom search logic: Searches across multiple related fields, 
        # including articles, storage, and project attributes, using a case-insensitive match.
        return queryset.filter(
            # Article fields
            Q(article__name__icontains=value) |
            Q(article__manufacturer__name__icontains=value) |
            Q(article__type_description__icontains=value) |
            Q(article__inventory_number__icontains=value) |
            Q(article__catalog_number__icontains=value) |
            Q(article__suppliers__name__icontains=value) |
            # Item fields
            Q(serial_number__icontains=value) |
            Q(quantity__icontains=value) |
            Q(storage_location__name__icontains=value) |
            Q(storage_sublocation__name__icontains=value) |
            Q(storage_drawer__name__icontains=value) |
            Q(assigned_project__name__icontains=value) |
            Q(assigned_subproject__name__icontains=value) |
            Q(assigned_assembly__name__icontains=value)
        )

    class Meta:
        model = Item
        fields = [
            # Article fields
            'article__name', 'article__manufacturer__name', 'article__type_description',
            'article__inventory_number', 'article__catalog_number', 'article__suppliers__name',
            # Item fields
            'serial_number', 'quantity', 'storage_location__name', 'storage_sublocation__name', 'storage_drawer__name',
            'assigned_project__name', 'assigned_subproject__name', 'assigned_assembly__name',
            # Hidden fields
            'article__manufacturer', 'assigned_project', 'assigned_subproject',
            'assigned_assembly', 'storage_location', 'storage_sublocation', 'storage_drawer'
        ]
        exclude = [
            'article__manufacturer', 'assigned_project', 'assigned_subproject',
            'assigned_assembly', 'storage_location', 'storage_sublocation', 'storage_drawer'
        ]
