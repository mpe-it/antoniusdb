document.addEventListener('DOMContentLoaded', function () {
    //console.log("Split Item JS loaded");

    const availableQuantity = parseFloat(document.getElementById('available_quantity').textContent);
    const quantityInput = document.querySelector('input[name="quantity"]');
    const serialNumberInput = document.querySelector('input[name="serial_number"]');
    const quantityError = document.getElementById('quantityError');
    const itemPreview = document.getElementById('itemPreview');
    const itemList = document.getElementById('itemList');

    function updatePreview() {
        //console.log("updatePreview called");

        const quantity = parseFloat(quantityInput.value);
        const serialNumbers = serialNumberInput.value.split(',').map(sn => sn.trim()).filter(sn => sn);
        const numSerialNumbers = serialNumbers.length;

        // Ensure elements exist before manipulating them
        if (quantityError && itemPreview) {
            // Reset preview and error states
            itemList.innerHTML = '';
            quantityError.classList.add('d-none');
            itemPreview.classList.add('d-none');

            let hasError = false;

            // Validation checks
            if (isNaN(quantity) || quantity <= 0) {
                //console.log("Invalid quantity input");
                quantityError.textContent = "Please enter a valid quantity greater than 0.";
                quantityError.classList.remove('d-none');
                hasError = true;
            }

            if (quantity > availableQuantity) {
                //console.log("Quantity exceeds available");
                quantityError.textContent = `Quantity exceeds available (${availableQuantity}).`;
                quantityError.classList.remove('d-none');
                hasError = true;
            }

            if (numSerialNumbers > 0 && numSerialNumbers > quantity) {
                //console.log("Serial numbers exceed quantity");
                quantityError.textContent = "The number of serial numbers cannot exceed the quantity.";
                quantityError.classList.remove('d-none');
                hasError = true;
            }

            if (new Set(serialNumbers).size !== serialNumbers.length) {
                //console.log("Duplicate serial numbers detected");
                quantityError.textContent = "Duplicate serial numbers detected.";
                quantityError.classList.remove('d-none');
                hasError = true;
            }

            // If there are no errors, show the item preview
            if (!hasError) {
                //console.log("Showing preview of items");
                itemPreview.classList.remove('d-none');  // Ensure preview is displayed

                // Add list of items with serial numbers
                serialNumbers.forEach(sn => {
                    const li = document.createElement('li');
                    li.textContent = `Create Item with Serial Number: ${sn} (Quantity: 1)`;
                    itemList.appendChild(li);
                });

                // If there's a remainder, show the bulk item with the remaining quantity
                const remainingQuantity = quantity - numSerialNumbers;
                if (remainingQuantity > 0) {
                    const li = document.createElement('li');
                    li.textContent = `Create Item with Quantity: ${remainingQuantity}`;
                    itemList.appendChild(li);
                }
            }
        }
    }

    // Event listeners for real-time validation and preview
    if (quantityInput && serialNumberInput) {
        quantityInput.addEventListener('input', updatePreview);
        serialNumberInput.addEventListener('input', updatePreview);
    }
});
