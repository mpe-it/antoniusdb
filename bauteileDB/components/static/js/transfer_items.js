document.addEventListener("DOMContentLoaded", function () {
    const assemblyInput = document.querySelector('#id_assembly_input');
    const projectInput = document.querySelector('#id_project_input');
    const subprojectInput = document.querySelector('#id_subproject_input');
    const locationInput = document.querySelector('#id_storage_location_input');
    const sublocationInput = document.querySelector('#id_storage_sublocation_input');
    const locationInfo = document.getElementById('location-info'); // New element for info message

    let debounceTimer;

    // Debug logging to check if the fields are detected
    //console.log('Assembly Input:', assemblyInput);
    //console.log('Project Input:', projectInput);
    //console.log('Subproject Input:', subprojectInput);
    //console.log('Location Input:', locationInput);
    //console.log('Sublocation Input:', sublocationInput);

    // Ensure the location and sublocation fields are initially editable
    function enableLocationInputs() {
        if (locationInput) locationInput.removeAttribute('readonly');
        if (sublocationInput) sublocationInput.removeAttribute('readonly');
        if (locationInfo) locationInfo.classList.add('d-none'); // Hide info message
        //console.log('Enabled location and sublocation inputs');
    }

    // Make location and sublocation fields read-only
    function makeLocationInputsReadOnly() {
        if (locationInput) locationInput.setAttribute('readonly', 'readonly');
        if (sublocationInput) sublocationInput.setAttribute('readonly', 'readonly');
        if (locationInfo) locationInfo.classList.remove('d-none'); // Show info message
        //console.log('Made location and sublocation inputs read-only');
    }

    // Fetch location and sublocation for a selected assembly
    function fetchAssemblyLocation(project, subproject, assemblyName) {
        //console.log('Fetching location for assembly:', { project, subproject, assemblyName });

        if (!assemblyName || !project || !subproject) {
            enableLocationInputs();  // No valid data, enable fields
            return;
        }

        const url = `/core/api/assembly-location/?project=${encodeURIComponent(project)}&subproject=${encodeURIComponent(subproject)}&assembly=${encodeURIComponent(assemblyName)}`;
        //console.log('API URL:', url); // Log the full URL

        fetch(url)
            .then(response => {
                //console.log('Response status:', response.status); // Log response status
                if (!response.ok) {
                    throw new Error('Network response was not OK');
                }
                return response.json();
            })
            .then(data => {
                //console.log('Data received:', data);
                if (data.location && locationInput) {
                    locationInput.value = data.location;
                    //console.log('Location set to:', data.location);
                }
                if (data.sublocation && sublocationInput) {
                    sublocationInput.value = data.sublocation;
                    //console.log('Sublocation set to:', data.sublocation);
                }
                makeLocationInputsReadOnly();  // Make fields read-only if assembly has location and sublocation
            })
            .catch(error => {
                console.error('Error fetching assembly location:', error);
                enableLocationInputs();  // Enable fields if an error occurs
            });
    }

    // Debounce function to avoid excessive API calls
    function debounceFetch() {
        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(() => {
            const assemblyName = assemblyInput.value.trim();
            const projectName = projectInput.value.trim();
            const subprojectName = subprojectInput.value.trim();
            //console.log('Debounced input change, values:', { assemblyName, projectName, subprojectName });

            if (assemblyName && projectName && subprojectName) {
                fetchAssemblyLocation(projectName, subprojectName, assemblyName);
            } else {
                enableLocationInputs();  // If any field is missing, enable the inputs
            }
        }, 300);  // Wait 300ms after the last input
    }

    // Listen for changes in the autocomplete inputs
    if (assemblyInput && projectInput && subprojectInput) {
        $(assemblyInput).on('autocompleteselect', debounceFetch);
        assemblyInput.addEventListener('input', debounceFetch);
    } else {
        //console.log('Required input fields not found.');
    }

    // Initialize: If assembly is already selected, fetch its location and sublocation
    if (assemblyInput && assemblyInput.value && projectInput && projectInput.value && subprojectInput && subprojectInput.value) {
        //console.log('Initial assembly selected:', assemblyInput.value);
        fetchAssemblyLocation(projectInput.value, subprojectInput.value, assemblyInput.value);
    }
});
