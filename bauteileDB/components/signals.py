from django.db.models.signals import post_migrate
from django.dispatch import receiver
from core.models.component_models import Unit, Item
from django.db.models.signals import post_save
from core.models.project_models import Project, Subproject, Assembly
from django.core.mail import send_mail
from django.conf import settings

@receiver(post_migrate)
def create_default_measurement(sender, **kwargs):
    # Check if "Stück" already exists to avoid duplicates
    if not Unit.objects.filter(unit_of_measurement="pcs").exists():
        Unit.objects.create(unit_of_measurement="pcs", description="Standard unit of measurement")
        
@receiver(post_save, sender=Assembly)
def update_items_on_assembly_change(sender, instance, **kwargs):
    """
    Signal to update items whenever the Assembly's storage_location or storage_sublocation is changed.
    """
    # Fetch the related items for this assembly
    related_items = Item.objects.filter(assigned_assembly=instance)

    for item in related_items:
        # Update the item's storage_location and storage_sublocation based on the assembly
        item.storage_location = instance.storage_location
        item.storage_sublocation = instance.storage_sublocation
        item.save()

    #print(f'Updated {related_items.count()} items for Assembly: {instance.name}')
    
@receiver(post_save, sender=Project)
def create_reserved_subproject(sender, instance, created, **kwargs):
    """
    Signal to automatically create a 'Reserved' subproject when a new project is created.
    """
    if created:  # Only run when a new project is created, not when it's updated
        # Check if the 'Reserved' subproject already exists for this project
        if not Subproject.objects.filter(name="Reserved", project=instance).exists():
            # Create the 'Reserved' subproject
            reserved_subproject = Subproject.objects.create(
                project=instance,
                name="Spare Parts",
                description=f"Subproject for spare parts of project {instance.name}."
            )
            #print(f"Spare subproject created for project: {instance.name}")

@receiver(post_save, sender=Subproject)
def create_reserved_assembly(sender, instance, created, **kwargs):
    """
    Signal to automatically create a 'Reserved' assembly when a new subproject is created.
    """
    if created:  # Only run when a new subproject is created, not when it's updated
        # Check if the 'Reserved' assembly already exists for this subproject
        if not Assembly.objects.filter(name="Reserved", subproject=instance).exists():
            # Create the 'Reserved' assembly
            reserved_assembly = Assembly.objects.create(
                subproject=instance,
                name="Spare Parts",
                description=f"Assembly for spare parts of subproject {instance.name} in project {instance.project.name}."
            )
            #print(f"Spare assembly created for subproject: {instance.name} in project: {instance.project.name}")


LOW_STOCK_THRESHOLD = 5  # Set your desired threshold

@receiver(post_save, sender=Item)
def check_low_stock(sender, instance, **kwargs):
    """Sends an email alert if an item's stock in a main storage is too low."""
    if instance.quantity < LOW_STOCK_THRESHOLD and instance.storage_sublocation.main_storage:
        subject = f" Niedriger Bestand: {instance.article.name}"
        message = (
            f"Item: {instance.article.name}\n"
            f"Current Stock: {instance.quantity}\n"
            f"Location: {instance.storage_sublocation.name} (Main Storage)\n\n"
        )

        recipient_list = settings.RECIPIENT_LIST
        #send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, recipient_list, fail_silently=False,)
