from django.urls import path
from .views.component_views import *


app_name = 'components'

urlpatterns = [
    #---------------Article views
    path('articles/', ArticleListView.as_view(), name='article_list'),  # List view for all articles
    path('add-articles/', ArticleAddView.as_view(), name='article-add'),
    path('articles/<int:pk>/edit/', ArticleUpdateView.as_view(), name='article_update'),  # Edit URL
    path('articles/<int:pk>/delete/', ArticleDeleteView.as_view(), name='article_delete'),
    path('articles/<int:pk>/stock/', ArticleStockView.as_view(), name='article_stock'),
    
    #---------------Item views
    path('items/', AllItemsView.as_view(), name='all_items_list'),
    path('articles/<int:article_id>/add-items/', ItemAddView.as_view(), name='item_add'),
    path('items/<int:pk>/edit/', ItemUpdateView.as_view(), name='item_update'),
    path('items/<int:pk>/delete/', ItemDeleteView.as_view(), name='item_delete'),
    path('items/<int:pk>/split/', ItemSplitView.as_view(), name='item_split'),
    path('items/<int:pk>/', ItemDetailView.as_view(), name='item_detail'),
    path('bulk_transfer/', BulkTransferView.as_view(), name='bulk_transfer'),
    #---------------Item-Order views
    path('items/<int:item_pk>/order/<int:order_pk>/delete/', 
         OrderDeleteView.as_view(), name='order_delete'),
    path('items/<int:item_pk>/order/<int:order_pk>/finish/', 
         OrderFinishView.as_view(), name='order_finish'),
    path('items/<int:pk>/order', ItemOrderView.as_view(), name='item_order'),
    path('items/<int:pk>/download_qr/', ItemQRDownloadView.as_view(), name='item_qr_download'),
    
    
    #---------------Export
    path('articles/<int:article_id>/export/', ArticleItemsExportView.as_view(), name='article_items_export'),
    path('item/export/', ItemExportView.as_view(), name='item_export'),
    path('articles/export/', ArticleExportView.as_view(), name='article_export'),
]

