from core.models.component_models import Item
from core.models.vendor_models import Manufacturer, Supplier
from core.filters import autocomplete_field

import django_filters
from django.db.models import Q
from django import forms


# FilterSet for Manufacturer objects
class ManufacturerFilter(django_filters.FilterSet):
    # General search field with custom filtering logic
    search = django_filters.CharFilter(
        method='filter_search',  # Method to handle the search logic
        label='Search',  # Label for the search input
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',  # Placeholder text for the input
        })
    )

    # Autocomplete-enabled field for filtering by Manufacturer name
    name = autocomplete_field(
        'name',  # Field name in the Manufacturer model
        'Manufacturer',  # Model to search against
        placeholder='Type to search for a manufacturer...'  # Placeholder text
    )

    # Autocomplete-enabled field for filtering by Manufacturer description
    description = autocomplete_field(
        'description',
        'Manufacturer',
        placeholder='Type to search for a description...'
    )

    # Custom filtering logic for the 'search' field
    def filter_search(self, queryset, name, value):
        # Search across the name and description fields using case-insensitive matching
        return queryset.filter(
            Q(name__icontains=value) |  # Matches substring in name
            Q(description__icontains=value)  # Matches substring in description
        )

    class Meta:
        model = Manufacturer  # The model this filter is based on
        # Fields to expose as filterable; 'search' is handled separately
        fields = ['name', 'description']


# FilterSet for Supplier objects
class SupplierFilter(django_filters.FilterSet):
    # General search field with custom filtering logic
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    # Autocomplete-enabled field for filtering by Supplier name
    name = autocomplete_field(
        'name',
        'Supplier',
        placeholder='Type to search for a Supplier...'
    )

    # Autocomplete-enabled field for filtering by Supplier description
    description = autocomplete_field(
        'description',
        'Supplier',
        placeholder='Type to search for a description...'
    )

    # Custom filtering logic for the 'search' field
    def filter_search(self, queryset, name, value):
        # Search across the name and description fields using case-insensitive matching
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = Supplier  # The model this filter is based on
        # Fields to expose as filterable; 'search' is handled separately
        fields = ['name', 'description']
