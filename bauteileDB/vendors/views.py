from django.shortcuts import  redirect, render, get_object_or_404
from django.urls import reverse_lazy
from core.views.generic_views import GenericExportView, GenericTableView, GenericAddView, GenericUpdateView
from core.models.component_models import Item
from core.models.vendor_models import Manufacturer, Supplier
from .forms import ManufacturerForm, SupplierForm
from .tables import ManufacturerTable, SupplierTable
from .filters import ManufacturerFilter, SupplierFilter#, ManufacturerItemsFilter
from django.contrib.auth.mixins import LoginRequiredMixin


#Manufacturer Views


class ManufacturerListView(GenericTableView):
    model = Manufacturer
    table_class = ManufacturerTable
    filterset_class = ManufacturerFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'manufacturer',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        context['add_button_url'] = reverse_lazy('vendors:manufacturer_add')
        context['add_button_label'] = 'Add Manufacturer'
        
        context['export_button_url'] = reverse_lazy('vendors:manufacturer_export')
        
        return context


class ManufacturerAddView(GenericAddView):
    model = Manufacturer
    form_class = ManufacturerForm
    page_title = "Add New Manufacturer"

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('vendors:manufacturer_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('vendors:manufacturer_list')

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(ManufacturerAddView, self).form_valid(form)


class ManufacturerUpdateView(GenericUpdateView):
    model = Manufacturer
    form_class = ManufacturerForm
    page_title = "Update Manufacturer"
    cancel_url_name = 'vendors:manufacturer_list'
    success_url_name = 'vendors:manufacturer_list'
    
  
class ManufacturerExportView(GenericExportView):
    model = Manufacturer
    table_class = ManufacturerTable
    filterset_class = ManufacturerFilter


#Supplier Views


class SupplierListView(GenericTableView):
    model = Supplier
    table_class = SupplierTable
    filterset_class = SupplierFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'supplier',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        context['add_button_url'] = reverse_lazy('vendors:supplier_add')
        context['add_button_label'] = 'Add Supplier'
        
        context['export_button_url'] = reverse_lazy('vendors:supplier_export')
        
        return context


class SupplierAddView(GenericAddView):
    model = Supplier
    form_class = SupplierForm
    page_title = "Add New Supplier"

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('vendors:supplier_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('vendors:supplier_list')

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(SupplierAddView, self).form_valid(form)


class SupplierUpdateView(GenericUpdateView):
    model = Supplier
    form_class = SupplierForm
    page_title = "Update Supplier"
    cancel_url_name = 'vendors:supplier_list'
    success_url_name = 'vendors:supplier_list'
    
  
class SupplierExportView(GenericExportView):
    model = Supplier
    table_class = SupplierTable
    filterset_class = SupplierFilter
