from core.models.component_models import Item
from core.models.vendor_models import Manufacturer, Supplier
from core.tables import BaseTable
import django_tables2 as tables


class ManufacturerTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='vendors/manufacturer_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = Manufacturer
        fields = ['selection', 'name', 'description', 'actions']


class SupplierTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='vendors/supplier_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = Supplier
        fields = ['selection', 'name', 'description', 'actions']