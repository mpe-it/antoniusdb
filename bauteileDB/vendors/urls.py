from django.urls import path
from .views import *

app_name = 'vendors'

urlpatterns = [
    #---------------Manufacturer URLs---------------
    path('manufacturers', ManufacturerListView.as_view(), name='manufacturer_list'),
    path('add-manufacturer/', ManufacturerAddView.as_view(), name='manufacturer_add'),
    path('<int:pk>/update-manufacturer/', ManufacturerUpdateView.as_view(), name='manufacturer_update'),
    path('item/export/', ManufacturerExportView.as_view(), name='manufacturer_export'),

    #---------------Supplier URLs---------------
    path('suppliers', SupplierListView.as_view(), name='supplier_list'),
    path('add-supplier/', SupplierAddView.as_view(), name='supplier_add'),
    path('<int:pk>/update-supplier/', SupplierUpdateView.as_view(), name='supplier_update'),
    path('item/export/', SupplierExportView.as_view(), name='supplier_export'),
]