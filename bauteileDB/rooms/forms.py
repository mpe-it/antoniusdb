from django import forms
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer

class StorageLocationForm(forms.ModelForm):
    class Meta:
        model = StorageLocation
        fields = ['name', 'description', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Building/Room e.g X2/307.6'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter description', 'rows': 3}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

class StorageSublocationForm(forms.ModelForm):
    class Meta:
        model = StorageSublocation
        fields = ['name', 'description', 'main_storage', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter storage sublocation name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter description', 'rows': 3}),
            'main_storage': forms.CheckboxInput(attrs={'class': 'form-control'}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

class StorageDrawerForm(forms.ModelForm):
    class Meta:
        model = StorageDrawer
        fields = ['name', 'description', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter storage drawer name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter description', 'rows': 3}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }
    sublocation = forms.ModelChoiceField(queryset=StorageSublocation.objects.all(), widget=forms.HiddenInput(), required=False)
