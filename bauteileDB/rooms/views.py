from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse_lazy
from core.views.generic_views import GenericAddView, GenericUpdateView, GenericTableView
from core.models.component_models import Item
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from .forms import StorageLocationForm, StorageSublocationForm, StorageDrawerForm
from .tables import StorageLocationTable, StorageSublocationTable, StorageDrawerTable
from .filters import StorageSublocationFilter, StorageLocationFilter, StorageDrawerFilter#, StorageSublocationItemsFilter
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin


#Location views


class LocationListView(GenericTableView):
    model = StorageLocation
    table_class = StorageLocationTable
    filterset_class = StorageLocationFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'storagelocation',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        context['add_button_url'] = reverse_lazy('rooms:location_add')
        context['add_button_label'] = 'Add Location'
        return context


class LocationAddView(GenericAddView):
    model = StorageLocation
    form_class = StorageLocationForm
    page_title = "Add New Location"

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('rooms:location_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('rooms:location_list')

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(LocationAddView, self).form_valid(form)


class LocationUpdateView(GenericUpdateView):
    model = StorageLocation
    form_class = StorageLocationForm
    page_title = "Update Location"
    cancel_url_name = 'rooms:location_list'
    success_url_name = 'rooms:location_list'


#Sublocation Views


class SublocationListView(GenericTableView):
    model = StorageSublocation
    table_class = StorageSublocationTable
    filterset_class = StorageSublocationFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'storagesublocation',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        location_id = self.kwargs.get('location_id')
        location = get_object_or_404(StorageLocation, pk=location_id)
        context['location'] = location
        context['verbose_name_plural'] = location.name + " - Sublocations"

        context['add_button_url'] = reverse_lazy('rooms:sublocation_add', kwargs={'location_id': location_id})
        context['add_button_label'] = 'Add Sublocation'

        return context

    def get_queryset(self):
        location_id = self.kwargs.get('location_id')
        return StorageSublocation.objects.filter(location_id=location_id).order_by('name')


class SublocationAddView(GenericAddView):
    model = StorageSublocation
    form_class = StorageSublocationForm
    page_title = "Add New Sublocation"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        location_id = self.kwargs.get('location_id')
        location = get_object_or_404(StorageLocation, pk=location_id)
        kwargs['initial'] = {'location': location}
        return kwargs

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        location_id = self.object.location.pk if self.object else self.kwargs.get('location_id')
        return next_url or reverse_lazy('rooms:sublocation_list', kwargs={'location_id': location_id})

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        location_id = self.object.location.pk if self.object else self.kwargs.get('location_id')
        return next_url or reverse_lazy('rooms:sublocation_list', kwargs={'location_id': location_id})
    
    def form_valid(self, form):
        location_id = self.kwargs.get('location_id')
        location = get_object_or_404(StorageLocation, pk=location_id)
        form.instance.location = location

        # Pass the request to the custom save method
        self.object = form.save(commit=False)
        self.object.save(request=self.request)

        action = self.request.POST.get('action')
        if action == 'save_and_create_another':
            self.log_addition(form.instance)
            return redirect(self.request.path)

        return super().form_valid(form)


class SublocationUpdateView(GenericUpdateView):
    model = StorageSublocation
    form_class = StorageSublocationForm
    page_title = "Update Sublocation"
    success_url_name = 'rooms:sublocation_list'


#Drawer Views


class DrawerListView(GenericTableView):
    model = StorageDrawer
    table_class = StorageDrawerTable
    filterset_class = StorageDrawerFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'storagedrawer',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        sublocation_id = self.kwargs.get('sublocation_id')  # Add this to identify the sublocation
        sublocation = get_object_or_404(StorageSublocation, pk=sublocation_id)

        context['sublocation'] = sublocation
        context['verbose_name_plural'] = f"{sublocation.location.name} - {sublocation.name} - Drawers"

        context['add_button_url'] = reverse_lazy('rooms:drawer_add', kwargs={'sublocation_id': sublocation_id})
        context['add_button_label'] = 'Add Drawer'
        
        return context

    def get_queryset(self):
        sublocation_id = self.kwargs.get('sublocation_id')  # Filter drawers by sublocation
        return StorageDrawer.objects.filter(sublocation_id=sublocation_id).order_by('name')


class DrawerAddView(GenericAddView):
    model = StorageDrawer
    form_class = StorageDrawerForm
    page_title = "Add New Drawer"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        sublocation_id = self.kwargs.get('sublocation_id')
        sublocation = get_object_or_404(StorageSublocation, pk=sublocation_id)
        kwargs['initial'] = kwargs.get('initial', {})
        kwargs['initial']['sublocation'] = sublocation  # Automatically set the sublocation
        return kwargs

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('rooms:sublocation_list', kwargs={'location_id': self.object.sublocation.location.pk})

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        sublocation_id = self.kwargs.get('sublocation_id')
        sublocation = get_object_or_404(StorageSublocation, pk=sublocation_id)
        return next_url or reverse_lazy('rooms:sublocation_list', kwargs={'location_id': sublocation.location.pk})

    def form_valid(self, form):
        # Ensure the drawer is tied to the correct sublocation
        sublocation_id = self.kwargs.get('sublocation_id')
        sublocation = get_object_or_404(StorageSublocation, pk=sublocation_id)
        form.instance.sublocation = sublocation

        # Save the drawer object
        self.object = form.save(commit=False)
        self.object.save(request=self.request)

        # Handle "Save and Create Another" action
        action = self.request.POST.get('action')
        if action == 'save_and_create_another':
            self.log_addition(form.instance)
            return redirect(self.request.path)

        return super().form_valid(form)




class DrawerUpdateView(GenericUpdateView):
    model = StorageDrawer
    form_class = StorageDrawerForm
    page_title = "Update Drawer"
    success_url_name = 'rooms:drawer_list'

    """def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        drawer_id = self.kwargs.get('pk')
        drawer = get_object_or_404(StorageDrawer, pk=drawer_id)
        
        # Set the initial sublocation value for the form
        kwargs['initial'] = kwargs.get('initial', {})
        kwargs['initial']['sublocation'] = drawer.sublocation
        return kwargs

    def get_success_url(self):
        # Get the sublocation ID for the redirect
        sublocation_id = self.object.sublocation.pk if self.object else self.kwargs.get('sublocation_id')
        return reverse_lazy('rooms:sublocation_list', kwargs={'sublocation_id': sublocation_id})

    def form_valid(self, form):
        # Ensure the drawer is linked to the correct sublocation
        sublocation_id = self.kwargs.get('sublocation_id')
        sublocation = get_object_or_404(StorageSublocation, pk=sublocation_id)
        form.instance.sublocation = sublocation  # Make sure the sublocation is set correctly
        
        # Call the base class form_valid to save the object and redirect
        return super().form_valid(form)"""
