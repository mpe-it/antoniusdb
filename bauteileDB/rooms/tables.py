from core.models.component_models import Item
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.tables import BaseTable
import django_tables2 as tables


class StorageLocationTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='rooms/location_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = StorageLocation
        fields = ['selection', 'name', 'description', 'actions']


class StorageSublocationTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='rooms/sublocation_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = StorageSublocation
        fields = ['selection','name', 'description', 'actions']


class StorageDrawerTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='rooms/drawer_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = StorageDrawer
        fields = ['selection','name', 'description', 'actions']
