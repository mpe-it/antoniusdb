from django.urls import path
from .views import *

app_name = 'rooms'

urlpatterns = [
    #---------------Location URLs---------------
    path('', LocationListView.as_view(), name='location_list'),
    path('add/', LocationAddView.as_view(), name='location_add'),
    path('<int:pk>/update/', LocationUpdateView.as_view(), name='location_update'),
    
    #---------------Sublocation URLs---------------
    path('<int:location_id>/sublocations/', SublocationListView.as_view(), name='sublocation_list'),
    path('<int:location_id>/sublocations/add/', SublocationAddView.as_view(), name='sublocation_add'),
    path('sublocations/<int:pk>/update/', SublocationUpdateView.as_view(), name='sublocation_update'),
    
    #---------------Drawer URLs---------------
    path('<int:sublocation_id>/drawers/', DrawerListView.as_view(), name='drawer_list'),
    path('<int:sublocation_id>/drawers/add/', DrawerAddView.as_view(), name='drawer_add'),
    path('drawers/<int:pk>/update/', DrawerUpdateView.as_view(), name='drawer_update'),
]