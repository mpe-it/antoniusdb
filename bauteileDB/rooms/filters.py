from core.models.component_models import Item
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.filters import autocomplete_field

import django_filters
from django import forms
from django.db.models import Q


# StorageLocationFilter:
# Provides filtering options for StorageLocation objects.
# Allows filtering by name and description through the 'search' field or directly by the name.
class StorageLocationFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    # Autocomplete field for the 'name' attribute of StorageLocation.
    name = autocomplete_field(
        'name',
        'StorageLocation',
        placeholder='Type to search a location...'
    )

    #Autocomplete field for the 'description' attribute of StorageLocation
    description = autocomplete_field(
        'description',
        'StorageLocation',
        placeholder='Type to search for a description...'
    )


    # Custom search logic: Filters StorageLocation by name and description.
    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
            # Add other fields if needed
        )

    class Meta:
        model = StorageLocation
        # Do not include 'search' in Meta.fields since it's a custom filter.
        fields = ['name', 'description']


# StorageSublocationFilter:
# Allows filtering of StorageSublocation objects based on their name, description, and associated location.
class StorageSublocationFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    # Autocomplete field for the 'name' attribute of StorageSublocation.
    name = autocomplete_field(
        'name',
        'StorageSublocation',
        placeholder='Type to search sublocation...'
    )
    # Autocomplete field for the 'description' attribute of StorageSublocation.
    description = autocomplete_field(
        'description',
        'StorageSublocation',
        placeholder='Type to search for a description...'
    )


    # Custom search logic: Filters by sublocation name, description, or associated location name.
    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = StorageSublocation
        fields = ['name', 'description']


# StorageDrawerFilter:
# Enables filtering of StorageDrawer objects by their name, description, and associated sublocation.
class StorageDrawerFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    # Autocomplete field for the drawer name.
    name = autocomplete_field(
        'name',
        'StorageDrawer',
        placeholder='Type to search drawer name...'
    )
    # Autocomplete field for the sublocation name.
    description = autocomplete_field(
        'description',
        'StorageDrawer',
        placeholder='Type to search description name...'
    )

    # Custom search logic: Filters by drawer name, description, or associated sublocation name.
    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = StorageDrawer
        fields = ['name', 'description']
