"""
URL configuration for bauteileDB project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.views.generic import RedirectView
from usermanagement import views as register_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('core/', include('core.urls')),
    path('components/', include('components.urls')),
    path('projects/', include('projects.urls')),
    path('rooms/', include('rooms.urls')),
    path('vendors/', include('vendors.urls')),
    path('import/', include('importexport.urls')),
    path('log/', include('log.urls')),
    path('tools/', include('tools.urls')),
    
    path('', include('django.contrib.auth.urls')),  # Auth URLs
    path('logout/', include('usermanagement.urls'), name='logout'),

    path('', RedirectView.as_view(url='/components/articles/', permanent=True)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
