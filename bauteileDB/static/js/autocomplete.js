function initializeAutocomplete() {
    $(".autocomplete-input").each(function () {
        const inputField = $(this);
        const autocompleteUrl = inputField.data('autocomplete-url');

        // Check for dependency fields (optional)
        const dependsOnClass = inputField.attr('class').match(/depends-on\s+([A-Za-z]+)_(\w+)/);
        let dependsOnModel = null;
        let dependsOnField = null;

        if (dependsOnClass) {
            dependsOnModel = dependsOnClass[1];
            dependsOnField = dependsOnClass[2];
        }

        inputField.autocomplete({
            source: function (request, response) {
                if (inputField.prop('readonly')) {
                    return; // Don't perform autocomplete if the field is readonly
                }

                const value = inputField.val();
                const terms = value.split(',').map(term => term.trim()); // Split input by commas
                let currentTerm = terms[terms.length - 1]; // Get the last term for autocomplete

                if (!currentTerm.length) {
                    currentTerm = '';
                    //return; // If no term to search, exit
                }

                const extraParams = {};
                if (dependsOnModel && dependsOnField) {
                    const parentField = $(`.autocomplete-input[data-autocomplete-url*='${dependsOnModel}']`);
                    const parentValue = parentField.val();
                    if (!parentValue) {
                        response([]); // No results if the parent field is empty
                        return;
                    }
                    extraParams[`${dependsOnModel.toLowerCase()}_${dependsOnField}`] = parentValue;
                }
                // AJAX call for autocomplete
                $.ajax({
                    url: autocompleteUrl,
                    dataType: "json",
                    data: Object.assign({ term: currentTerm }, extraParams),
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.value,
                                info: item.info
                            };
                        }));
                    }
                });
            },
            minLength: 0,
            select: function (event, ui) {
                let value = inputField.val();
                const terms = value.split(',').map(term => term.trim()); // Split input by commas
                terms[terms.length - 1] = ui.item.value; // Replace the last term with the selected value
                inputField.val(terms.join(', ')); //+ ', '); // Add a comma and space for the next entry
                return false; // Prevent the default behavior
            }
        })
        .data("ui-autocomplete")._renderItem = function (ul, item) {
            const listItem = $("<li>")
                .append(`
                    <div class="d-flex justify-content-between align-items-center">
                        <span>${item.label}</span>
                        <i class="info-icon bi bi-info-circle" 
                           data-bs-toggle="tooltip" 
                           data-bs-placement="top" 
                           title="${item.info}" 
                           style="margin-left: auto; cursor: pointer;"></i>
                    </div>
                `);
            return listItem.appendTo(ul);
        };

        // Trigger autocomplete on focus
        inputField.on('focus', function () {
            if (inputField.prop('readonly')) {
                return;
            }
            if (!$(this).val().length) {
                $(this).autocomplete("search", ""); // Trigger empty search to load all results
            }
        });

        // Initialize Bootstrap tooltips after rendering
        inputField.on('autocompleteselect autocompletesearch', function () {
            setTimeout(function () {
                const tooltipTriggerList = [].slice.call(document.querySelectorAll('.info-icon'));
                tooltipTriggerList.forEach(function (tooltipTriggerEl) {
                    const existingTooltip = bootstrap.Tooltip.getInstance(tooltipTriggerEl);
                    if (existingTooltip) {
                        existingTooltip.dispose();
                    }
                    new bootstrap.Tooltip(tooltipTriggerEl);
                });
            }, 100);
        });
    });
}

$(document).ready(function () {
    initializeAutocomplete();
});