// static/js/bulk_actions.js

document.addEventListener('DOMContentLoaded', function() {
    // Get the main container element and data attributes
    const mainContainer = document.getElementById('main-container');
    const verboseName = mainContainer ? mainContainer.dataset.verboseName : 'item';
    const verboseNamePlural = mainContainer ? mainContainer.dataset.verboseNamePlural : 'items';

    // Use the current pathname as part of the storage key
    const STORAGE_KEY = `selectedItems_${window.location.pathname}`;

    let isPaginatorNavigation = false;

    // Attach event listeners to paginator links
    document.querySelectorAll('a[data-paginator-link]').forEach(function(link) {
        link.addEventListener('click', function(event) {
            isPaginatorNavigation = true;
            //console.log('Paginator navigation detected');
        });
    });

    // Handle beforeunload event to clear selections when not navigating via paginator
    window.addEventListener('beforeunload', function() {
        setTimeout(function() {
            if (!isPaginatorNavigation) {
                localStorage.removeItem(STORAGE_KEY);
                //console.log('Cleared selected IDs on page unload (not paginator navigation)');
            } else {
                //console.log('Paginator navigation, selections preserved');
                // Reset the flag for future navigations
                isPaginatorNavigation = false;
            }
        }, 0);
    });

    // Function to parse URL parameters
    function parseURLParameters() {
        const params = new URLSearchParams(window.location.search);
        const selectionsFromURL = params.getAll('selection');
        return selectionsFromURL;
    }

    // Function to get selected IDs from localStorage
    function getSelectedIds() {
        const selectedIds = localStorage.getItem(STORAGE_KEY);
        const ids = selectedIds ? JSON.parse(selectedIds) : [];
        //console.log('Retrieved selected IDs:', ids);
        return ids;
    }

    // Function to save selected IDs to localStorage
    function saveSelectedIds(ids) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(ids));
        //console.log('Saved selected IDs:', ids);
    }

    // Function to update the state of checkboxes based on selected IDs
    function updateCheckboxes() {
        const selectedIds = getSelectedIds();
        const rowCheckboxes = document.querySelectorAll('input[type="checkbox"][name="selection"]');
        rowCheckboxes.forEach(function(checkbox) {
            if (selectedIds.includes(checkbox.value)) {
                checkbox.checked = true;
            } else {
                checkbox.checked = false;
            }
        });
        updateBulkActionButtons();
        updateSelectedCount();
    }

    // Function to enable/disable bulk action buttons
    function updateBulkActionButtons() {
        const selectedIds = getSelectedIds();
        // Select all bulk action buttons, including those that open modals
        const bulkActionButtonsAll = document.querySelectorAll('.bulk-action-button, .bulk-action-button-modal');
        if (selectedIds.length > 0) {
            bulkActionButtonsAll.forEach(function(button) {
                button.disabled = false;
            });
        } else {
            bulkActionButtonsAll.forEach(function(button) {
                button.disabled = true;
            });
        }
    }

    // Function to update the display of the number of selected items
    function updateSelectedCount() {
        const selectedIds = getSelectedIds();
        const selectedCountElement = document.getElementById('selected-count');
        const clearSelectionButton = document.getElementById('clear-selection-button');
        const rowCheckboxes = document.querySelectorAll('input[type="checkbox"][name="selection"]');

        if (selectedCountElement) {
            const count = selectedIds.length;
            const name = count === 1 ? verboseName : verboseNamePlural;
            selectedCountElement.textContent = `${count} ${name} selected`;

            if (count > 0) {
                selectedCountElement.style.display = 'inline';

                // Check if selections exist on other pages
                const currentPageIds = Array.from(rowCheckboxes).map(cb => cb.value);
                const selectionsOnOtherPages = selectedIds.some(id => !currentPageIds.includes(id));

                // Show a notice if selections exist on other pages
                const otherPagesNotice = document.getElementById('other-pages-notice');
                if (otherPagesNotice) {
                    if (selectionsOnOtherPages) {
                        otherPagesNotice.style.display = 'inline';
                    } else {
                        otherPagesNotice.style.display = 'none';
                    }
                }

                // Show the "Clear Selection" button if it exists
                if (clearSelectionButton) {
                    clearSelectionButton.style.display = 'inline-block';
                }
            } else {
                selectedCountElement.style.display = 'none';

                // Hide the notice
                const otherPagesNotice = document.getElementById('other-pages-notice');
                if (otherPagesNotice) {
                    otherPagesNotice.style.display = 'none';
                }

                // Hide the "Clear Selection" button if it exists
                if (clearSelectionButton) {
                    clearSelectionButton.style.display = 'none';
                }
            }
        }
    }

    // Handle "Clear Selection" button
    const clearSelectionButton = document.getElementById('clear-selection-button');
    if (clearSelectionButton) {
        clearSelectionButton.addEventListener('click', function() {
            // Clear selections
            localStorage.removeItem(STORAGE_KEY);
            //console.log('Cleared selected IDs');
            updateCheckboxes();
            updateBulkActionButtons();
            updateSelectedCount();
        });
    }

    // Initialize checkboxes and selected count on page load
    function initializeSelections() {
        const selectedIds = getSelectedIds();

        // Parse selections from URL and merge with localStorage
        const selectionsFromURL = parseURLParameters();
        if (selectionsFromURL.length > 0) {
            selectionsFromURL.forEach(function(id) {
                if (!selectedIds.includes(id)) {
                    selectedIds.push(id);
                }
            });
            saveSelectedIds(selectedIds);

            // Optionally, remove 'selection' parameters from the URL to clean it up
            const url = new URL(window.location);
            selectionsFromURL.forEach(function(id) {
                url.searchParams.delete('selection');
            });
            window.history.replaceState({}, document.title, url.pathname + url.search);
        }

        updateCheckboxes();
    }

    initializeSelections();

    // Handle checkbox changes
    const rowCheckboxes = document.querySelectorAll('input[type="checkbox"][name="selection"]');
    rowCheckboxes.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            let selectedIds = getSelectedIds();
            if (checkbox.checked) {
                if (!selectedIds.includes(checkbox.value)) {
                    selectedIds.push(checkbox.value);
                }
            } else {
                const index = selectedIds.indexOf(checkbox.value);
                if (index > -1) {
                    selectedIds.splice(index, 1);
                }
            }
            saveSelectedIds(selectedIds);
            updateBulkActionButtons();
            updateSelectedCount();
        });
    });

    // Handle "Select All" checkbox functionality
    const selectAllCheckbox = document.getElementById('select-all');
    if (selectAllCheckbox) {
        selectAllCheckbox.addEventListener('change', function () {
            const isChecked = selectAllCheckbox.checked;
            let selectedIds = getSelectedIds();
            rowCheckboxes.forEach(function (checkbox) {
                checkbox.checked = isChecked;
                const checkboxValue = checkbox.value;
                if (isChecked) {
                    if (!selectedIds.includes(checkboxValue)) {
                        selectedIds.push(checkboxValue);
                    }
                } else {
                    const index = selectedIds.indexOf(checkboxValue);
                    if (index > -1) {
                        selectedIds.splice(index, 1);
                    }
                }
            });
            saveSelectedIds(selectedIds);
            updateBulkActionButtons();
            updateSelectedCount();
        });
    }

    // Function to redirect to bulk action via GET (e.g., Transfer Selected)
    function redirectToBulkAction(actionUrl) {
        const selectedIds = getSelectedIds();
        if (selectedIds.length === 0) {
            alert(`Please select at least one ${verboseName}.`);
            return;
        }

        // Build the URL with selected IDs as query parameters
        const url = new URL(actionUrl, window.location.origin);
        selectedIds.forEach(id => url.searchParams.append('selection', id));

        // Include 'next' parameter to redirect back after action
        url.searchParams.set('next', window.location.href);

        // Redirect to the URL
        window.location.href = url.toString();
    }

    // Function to submit the bulk action via POST (e.g., Delete Selected)
    function submitBulkAction(actionUrl) {
        const selectedIds = getSelectedIds();
        if (selectedIds.length === 0) {
            alert(`Please select at least one ${verboseName}.`);
            return;
        }

        // Create a form and submit it
        const form = document.createElement('form');
        form.method = 'POST';
        form.action = actionUrl;

        // Add CSRF token
        const csrfTokenInput = document.querySelector('input[name="csrfmiddlewaretoken"]');
        if (csrfTokenInput) {
            const csrfToken = csrfTokenInput.value;
            const csrfInput = document.createElement('input');
            csrfInput.type = 'hidden';
            csrfInput.name = 'csrfmiddlewaretoken';
            csrfInput.value = csrfToken;
            form.appendChild(csrfInput);
        } else {
            console.error('CSRF token not found in the page.');
            alert('An error occurred. Please reload the page and try again.');
            return;
        }

        // Add selected IDs as hidden inputs
        selectedIds.forEach(function(id) {
            const input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'selection';
            input.value = id;
            form.appendChild(input);
        });

        // Add 'next' input
        const nextInput = document.createElement('input');
        nextInput.type = 'hidden';
        nextInput.name = 'next';
        nextInput.value = window.location.href;
        form.appendChild(nextInput);

        document.body.appendChild(form);
        form.submit();
    }

    // Handle bulk action buttons that execute actions immediately (without confirmation)
    const bulkActionButtons = document.querySelectorAll('.bulk-action-button:not(.bulk-action-button-modal)[data-url]');
    bulkActionButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            const method = this.dataset.method ? this.dataset.method.toUpperCase() : 'POST';
            const url = this.dataset.url;

            if (method === 'GET') {
                // Redirect to the form page with selected IDs
                redirectToBulkAction(url);
            } else if (method === 'POST') {
                // Submit the bulk action via POST
                submitBulkAction(url);
            } else {
                console.warn(`Unsupported method "${method}" for bulk action.`);
            }
        });
    });

    // No need to attach click handlers to buttons that open modals (bulk-action-button-modal)
    // The action will be triggered when the user confirms in the modal

    // Handle confirm buttons in modals (e.g., Delete Confirmation)
    const confirmButtons = document.querySelectorAll('.bulk-action-confirm');
    confirmButtons.forEach(function(button) {
        button.addEventListener('click', function() {
            const url = this.dataset.url;
            const method = this.dataset.method ? this.dataset.method.toUpperCase() : 'POST';

            // Close the modal
            const modalElement = this.closest('.modal');
            if (modalElement) {
                const modalInstance = bootstrap.Modal.getInstance(modalElement);
                if (modalInstance) {
                    modalInstance.hide();
                }
            }

            if (method === 'GET') {
                redirectToBulkAction(url);
            } else if (method === 'POST') {
                submitBulkAction(url);
            } else {
                console.warn(`Unsupported method "${method}" for bulk action.`);
            }
        });
    });
});
