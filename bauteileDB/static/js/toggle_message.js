function toggleLog(event, element) {
    event.preventDefault();
    const row = element.closest('td');
    const shortMessage = row.querySelector('.log-short');
    const fullMessage = row.querySelector('.log-full');
    const isExpanded = fullMessage.style.display === 'inline';

    if (isExpanded) {
        fullMessage.style.display = 'none';
        shortMessage.style.display = 'inline';
        element.textContent = '[Show more]';
    } else {
        fullMessage.style.display = 'inline';
        shortMessage.style.display = 'none';
        element.textContent = '[Show less]';
    }
}