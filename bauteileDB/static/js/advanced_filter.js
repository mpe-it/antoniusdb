
document.addEventListener('DOMContentLoaded', function () {
    const advancedFilterForm = document.getElementById('advancedFilterForm');
    const applyFilterButton = document.getElementById('applyFilterButton');

    if (applyFilterButton) {
        //console.log(applyFilterButton);
        applyFilterButton.addEventListener('click', function(event) {
            event.preventDefault(); // Prevent the default form submission

            const formData = new FormData(advancedFilterForm);
            const queryParams = new URLSearchParams();
            const seenKeys = new Set(); // A Set to track keys we've already processed

            // Debug: Output the form data
            //console.log("Form Data:");
            //formData.forEach((value, key) => {
            //    console.log(`Key: ${key}, Value: ${value}`);
            //});

            // Clear old filters (add new ones only)
            formData.forEach(function(value, key) {
                //console.log("Processing filter:", key, "with value:", value); // Debugging: Show each key-value pair being processed
                
                if (!seenKeys.has(key)) { // Only process the key if it hasn't been seen before
                    if (value || value === '') { // Check if the value is present or empty
                        // Debugging: Show whether we are replacing an old value or adding a new one
                        if (value === '') {
                            //console.log("Value is empty, removing key:", key); // Debugging: Log when a filter is removed
                            queryParams.delete(key); // Remove filter if the value is empty
                        } else {
                            //console.log("Setting or replacing filter:", key, "with value:", value); // Debugging: Log when a new filter is set or an old one is replaced
                            queryParams.set(key, value); // Replace old value or add new filter value
                        }
                    }
                    seenKeys.add(key); // Mark this key as processed
                } else {
                    //console.log("Skipping key (already seen):", key); // Debugging: Log when a key is skipped
                }
            });

            // Debug: Output the current URL and the new query parameters
            //console.log("Current URL: " + window.location.href);
            //console.log("New Query Params: " + queryParams.toString());

            // Build the new URL (use current path with new query parameters)
            const newUrl = window.location.pathname + '?' + queryParams.toString();
            
            // Debug: Output the new URL before redirect
            //console.log("Redirecting to: " + newUrl);

            // Perform the redirect with the new URL (removes old filters and adds only the new ones)
            window.location.href = newUrl;
        });
    }
});