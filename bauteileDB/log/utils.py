from core.models.log_models import LogEntry
import logging

logger = logging.getLogger('myapp')

def log_to_database(level, message, user=None, additional_data=None):
    # Save to the database
    if level == 'DEBUG':
        return
    LogEntry.objects.create(level=level, message=message, user=user, additional_data=additional_data)
    # Optionally log to the standard logger
    log_method = getattr(logger, level.lower(), logger.info)
    log_method(f"{message} | Additional data: {additional_data}")
