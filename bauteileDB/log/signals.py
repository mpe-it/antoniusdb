from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.utils import timezone
from core.models.log_models import LogEntry
from log.utils import log_to_database

@receiver(user_logged_in)
def log_user_login(sender, request, user, **kwargs):
    # Define the log message
    log_message = f"User {user.username} logged in from IP {request.META.get('REMOTE_ADDR')}"
    log_to_database('INFO',log_message, user)
