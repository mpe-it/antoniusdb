from django.urls import path
from .views import LogListView

app_name = 'log'

urlpatterns = [
    path('', LogListView.as_view(), name='log_list'),
]