from django.urls import reverse_lazy
from core.views.generic_views import GenericExportView, GenericTableView
from core.models.log_models import LogEntry  # Adjust the import based on your log entry model
from .tables import LogEntryTable  # Make sure to create a table for your log entries
from .filters import LogEntryFilter  # Create a filter for log entries, similar to ManufacturerFilter
from django.contrib.auth.mixins import LoginRequiredMixin

class LogListView(LoginRequiredMixin,GenericTableView):
    model = LogEntry
    table_class = LogEntryTable
    filterset_class = LogEntryFilter
    ordering = ['-timestamp']
    login_url = '/login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context