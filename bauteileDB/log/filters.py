from core.models.log_models import LogEntry
from core.filters import autocomplete_field

import django_filters
from django.db.models import Q
from django import forms


class LogEntryFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    level = django_filters.ChoiceFilter(
        field_name='level',
        choices=LogEntry.LEVEL_CHOICES,  # Use the model's LEVEL_CHOICES
        label='Level',
        widget=forms.Select(attrs={
            'class': 'form-control',
            'placeholder': 'Select a log level...',
        })
    )

    user = autocomplete_field(
        'user__username',
        'LogEntry',
        autocomplete_field_name='username',
        placeholder='Type to search for a user...'
    )

    def filter_search(self, queryset, level, value):
        return queryset.filter(
            Q(level__icontains=value) |
            Q(user__username__icontains=value)
            # Add other fields if needed
        )

    class Meta:
        model = LogEntry
        # Do not include 'search' in Meta.fields
        fields = ['level', 'user']
