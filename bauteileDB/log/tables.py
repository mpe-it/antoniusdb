from core.models.log_models import LogEntry 
from core.tables import BaseTable
import django_tables2 as tables
from django.utils.safestring import mark_safe


class LogEntryTable(BaseTable):
    message = tables.Column(
        accessor='message',
        verbose_name='Log-Message',
        attrs={
            "td": {"class": "has-tooltip", "data-bs-toggle": "tooltip", "title": lambda record: record.info}
        }
    )
    class Meta(BaseTable.Meta):
        model = LogEntry
        template_name = 'django_tables2/bootstrap5.html'
        fields = ('selection','level', 'user', 'message','timestamp')

    def render_message(self, value):
        length = 100
        # Truncate the message to length characters
        truncated_message = value[:length] + "..." if len(value) > length else value
        # Return HTML for the truncated message with "Show more" functionality
        return mark_safe(f"""
            <span class="log-short">{truncated_message}</span>
            <span class="log-full" style="display: none;">{value}</span>
            {f'<a href="#" class="toggle-log" onclick="toggleLog(event, this)">[Show more]</a>' if len(value) > length else ''}
        """)