from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import CSVUploadForm
from .csv_import import importer  # Import the importer function you created
from core.models.component_models import Unit
from django.conf import settings
from django.contrib.auth.decorators import login_required
from log.utils import log_to_database

@login_required(login_url='/login')
def import_csv_view(request):
    if request.method == 'POST':
        form = CSVUploadForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = form.cleaned_data['csv_file']
            delimiter = form.cleaned_data.get('delimiter', ';')

            # Retrieve user-defined mappings from the form
            mappings = {
                form.cleaned_data['name']: {'model': 'Article', 'field': 'name'},
                form.cleaned_data['manufacturer']: {'model': 'Manufacturer', 'field': 'name'},
                form.cleaned_data['supplier']: {'model': 'Supplier', 'field': 'name'},
                form.cleaned_data['catalog_number']: {'model': 'Article', 'field': 'catalog_number'},
                form.cleaned_data['material_number']: {'model': 'Article', 'field': 'material_number'},
                form.cleaned_data['type_description']: {'model': 'Article', 'field': 'type_description'},
                form.cleaned_data['description']: {'model': 'Article', 'field': 'description'},
                form.cleaned_data['width']: {'model': 'Article', 'field': 'width'},
                form.cleaned_data['length']: {'model': 'Article', 'field': 'length'},
                form.cleaned_data['height']: {'model': 'Article', 'field': 'height'},
                form.cleaned_data['weight']: {'model': 'Article', 'field': 'weight'},  # Corrected field
                form.cleaned_data['waz']:{'model': 'Article', 'field': 'waz'},
                form.cleaned_data['location']: {'model': 'StorageLocation', 'field': 'name'},
                form.cleaned_data['sublocation']: {'model': 'StorageSublocation', 'field': 'name'},
                form.cleaned_data['drawer']: {'model': 'StorageDrawer', 'field': 'name'},
                # Add more mappings as needed...
            }

            unit = form.cleaned_data.get('unit')  # This is a Unit instance or None

            try:
                # Call the importer function with the file and the dynamic mappings
                importer(csv_file, mappings, delimiter, unit, request)
                messages.success(request, 'CSV file imported successfully.')
            except Exception as e:
                user = request.user if request.user.is_authenticated else None
                log_message = f"Import_CSV_View accessed by {user}: {e}"
                log_to_database('ERROR', log_message,user=user)
                messages.error(request, f'An error occurred: {e}')

            return redirect('importexport:import_export_csv')
    else:
        form = CSVUploadForm()

    user = request.user if request.user.is_authenticated else None
    log_message = f"Import_CSV_View accessed by {user}from IP {request.META.get('REMOTE_ADDR')}"
    log_to_database('DEBUG', log_message,user=user)

    return render(request, 'import-export-csv.html', {'form': form})
