import csv
from datetime import date
from django.http import HttpResponse
from core.models.component_models import Article, Item


def export_component_types():
    today = date.today()
    today = today.strftime("%d-%m-%Y")
    filename = "BauteileDB-"+today+".csv"
    # Create the HttpResponse object with CSV content type
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = f'attachment; filename="{filename}"'

    # Create a CSV writer
    writer = csv.writer(response)

    # Write the header row
    writer.writerow([
        'Name', 'Manufacturer', 'Catalog Number', 'Description', 'Serial Number', 'Quantitiy', 'Location', 'Assembly'
    ])


    # Write data rows
    for component in ComponentType.objects.all():
        for instance in ComponentInstance.objects.filter(component_type =component):
            writer.writerow([
                component.name,
                component.manufacturer.name if component.manufacturer else 'N/A',
                component.catalog_number if component.catalog_number else 'N/A',
                component.description if component.description else 'N/A',
                instance.serial_number,
                instance.quantity,
                instance.storage_location,
                instance.assigned_subproject
            ])

    return response
