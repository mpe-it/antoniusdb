import csv
from django.db import transaction
from core.models.component_models import Article, Item, Unit
from core.models.vendor_models import Manufacturer
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.models.project_models import Project, Subproject, Assembly
from django.db.models import Model
from log.utils import log_to_database

def get_or_create_related(model: Model, name: str, cache: dict):
    """
    Helper function to get or create related objects.
    """
    if not name:
        return None

    if name in cache:
        return cache[name]

    obj, created = model.objects.get_or_create(name=name)
    cache[name] = obj
    return obj

def process_projects(rows, project_field, caches, request):
    """
    Process and cache Project data.
    """
    if project_field:
        cache = caches['projects']
        for row in rows:
            project_name = row.get(project_field)
            if project_name:
                get_or_create_related(Project, project_name, cache)
                # Log creation if it's a new object
                log_message = f"New Project imported: {project_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                log_to_database('INFO', log_message, user=request.user)

def process_subprojects(rows, subproject_field, project_field, caches, request):
    """
    Process and cache Subproject data and ensure Subproject has an associated Project.
    """
    if subproject_field and project_field:
        subproject_cache = caches['subprojects']
        project_cache = caches['projects']
        for row in rows:
            subproject_name = row.get(subproject_field)
            project_name = row.get(project_field)
            if subproject_name and project_name:
                # Get or create Project
                project = get_or_create_related(Project, project_name, project_cache)

                # Get or create Subproject
                subproject, created = subproject_cache.get(subproject_name, (None, False))
                if not subproject:
                    subproject, created = Subproject.objects.get_or_create(name=subproject_name, defaults={'project': project})
                    subproject_cache[subproject_name] = subproject
                    # Log when a new subproject is created
                    if created:
                        log_message = f"New Subproject imported: {subproject_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                        log_to_database('INFO', log_message, user=request.user)

def process_assemblies(rows, assembly_field, caches, request):
    """
    Process and cache Assembly data.
    """
    if assembly_field:
        cache = caches['assemblies']
        for row in rows:
            assembly_name = row.get(assembly_field)
            if assembly_name:
                assembly = get_or_create_related(Assembly, assembly_name, cache)
                if assembly:
                    log_message = f"New Assembly imported: {assembly_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                    log_to_database('INFO', log_message, user=request.user)

def process_storage_locations(rows, location_field, caches, request):
    """
    Process and cache StorageLocation data.
    """
    if location_field:
        cache = caches['locations']
        for row in rows:
            location_name = row.get(location_field)
            if location_name:
                location = get_or_create_related(StorageLocation, location_name, cache)
                # No unpacking required since get_or_create_related returns only the object
                log_message = f"New StorageLocation imported: {location_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                log_to_database('INFO', log_message, user=request.user)


def process_storage_sublocations(rows, sublocation_field, location_field, caches, request):
    """
    Process and cache StorageSublocation data and ensure StorageSublocation has an associated StorageLocation.
    """
    if sublocation_field and location_field:
        sublocation_cache = caches['sublocations']
        location_cache = caches['locations']
        for row in rows:
            sublocation_name = row.get(sublocation_field)
            location_name = row.get(location_field)
            if sublocation_name and location_name:
                # Get or create StorageLocation
                location = get_or_create_related(StorageLocation, location_name, location_cache)

                # Get or create StorageSublocation
                sublocation = sublocation_cache.get(sublocation_name)  # Directly access cache
                if not sublocation:
                    sublocation = StorageSublocation.objects.get_or_create(name=sublocation_name, defaults={'location': location})[0]  # Only the object

                    sublocation_cache[sublocation_name] = sublocation  # Add to cache

                    # Log when a new sublocation is created
                    log_message = f"New StorageSublocation imported: {sublocation_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                    log_to_database('INFO', log_message, user=request.user)



def process_storage_drawers(rows, drawer_field, sublocation_field, location_field, caches, request):
    """
    Process and cache StorageDrawer data and ensure StorageDrawer has an associated StorageSublocation.
    """
    if drawer_field and sublocation_field and location_field:
        drawer_cache = caches['drawers']  # Cache for StorageDrawer objects
        sublocation_cache = caches['sublocations']  # Cache for StorageSublocation objects
        location_cache = caches['locations']  # Cache for StorageLocation objects
        
        for row in rows:
            drawer_name = row.get(drawer_field)
            sublocation_name = row.get(sublocation_field)
            location_name = row.get(location_field)
            
            if drawer_name and sublocation_name and location_name:
                # Get or create StorageLocation
                location = get_or_create_related(StorageLocation, location_name, location_cache)

                # Get or create StorageSublocation
                sublocation = sublocation_cache.get(sublocation_name)  # Directly access cache
                if not sublocation:
                    sublocation = StorageSublocation.objects.get_or_create(name=sublocation_name, defaults={'location': location})[0]  # Only the object

                    sublocation_cache[sublocation_name] = sublocation  # Add to cache

                    # Log when a new sublocation is created
                    log_message = f"New StorageSublocation imported: {sublocation_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                    log_to_database('INFO', log_message, user=request.user)

                # Get or create StorageDrawer
                drawer = drawer_cache.get(drawer_name)  # Directly access cache
                if not drawer:
                    drawer = StorageDrawer.objects.get_or_create(name=drawer_name, defaults={'sublocation': sublocation})[0]  # Only the object

                    drawer_cache[drawer_name] = drawer  # Add to cache

                    # Log when a new drawer is created
                    log_message = f"New StorageDrawer imported: {drawer_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                    log_to_database('INFO', log_message, user=request.user)


def process_manufacturers(rows, manufacturer_field, manufacturers, request):
    """
    Process and cache Manufacturer data.
    """
    if manufacturer_field:
        for row in rows:
            manufacturer_name = row.get(manufacturer_field)
            if manufacturer_name:
                # Ensure we're handling Manufacturer object creation
                manufacturer, created = Manufacturer.objects.get_or_create(name=manufacturer_name)
                if created:
                    # Log the creation of the new manufacturer
                    log_message = f"New Manufacturer imported: {manufacturer_name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                    log_to_database('INFO', log_message, user=request.user)
                # Update the manufacturers cache (this might be optional)
                manufacturers[manufacturer_name] = manufacturer

def process_article_data(row, model_mappings):
    """
    Collect article data from a row based on mappings.
    """
    article_data = {}
    for header, mapping in model_mappings.items():
        model_name = mapping.get('model')
        field_name = mapping.get('field')
        value = row.get(header)

        if not value:
            continue

        if model_name == 'Article' and field_name:
            article_data[field_name] = value

    return article_data

def process_related_objects_for_item(row, model_mappings, caches, request):
    """
    Retrieve or create related objects for an item and return them.
    """
    related_objects = {}

    related_mappings = {
        'Project': 'project',
        'Subproject': 'subproject',
        'Assembly': 'assembly',
        'StorageLocation': 'location',
        'StorageSublocation': 'sublocation'
    }

    for model_name, field in related_mappings.items():
        if field in model_mappings:
            related_objects[model_name] = get_or_create_related(
                globals()[model_name],
                row.get(field),
                caches[model_name.lower() + 's']
            )

    return related_objects

def import_articles_and_items(rows, model_mappings, caches, manufacturers, unit, request):
    """
    Import articles and create associated items.
    """
    for row in rows:
        article_data = process_article_data(row, model_mappings)

        if not article_data.get('name'):
            continue  # Skip if name is missing

        # Set the manufacturer foreign key if the field is present
        manufacturer_field = None
        for key, mapping in model_mappings.items():
            if mapping.get('model') == 'Manufacturer' and mapping.get('field') == 'name':
                manufacturer_field = key
                break
        if manufacturer_field:
            manufacturer_name = row.get(manufacturer_field)
            if manufacturer_name:
                article_data['manufacturer'] = manufacturers.get(manufacturer_name)

        if unit:
            article_data['unit'] = unit
        # Create or update the article
        article, created = Article.objects.update_or_create(
            name=article_data.get('name'),
            defaults=article_data
        )
        if created:
            log_message = f"New Article imported: {article.name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
            log_to_database('INFO', log_message, user=request.user)

        # Process related objects
        related_objects = process_related_objects_for_item(row, model_mappings, caches, request)

        # Check if all related fields are not empty
        if any(related_objects.values()):
            item_data = {
                'article': article,
                **related_objects
            }

            # Create or update items based on the related fields
            existing_item = Item.objects.filter(
                article=article,
                **{f'assigned_{k.lower()}': v for k, v in related_objects.items()}
            ).first()

            if not existing_item:
                item = Item.objects.create(**item_data)
                # Log when a new item is created
                log_message = f"New Item created for Article {article.name} by {request.user} from IP {request.META.get('REMOTE_ADDR')}"
                log_to_database('INFO', log_message, user=request.user)

def importer(file, model_mappings, delimiter, unit, request):
    """
    Main importer function to handle CSV import.
    """
    file_data = file.read().decode('utf-8', errors='ignore').splitlines()
    csv_reader = csv.DictReader(file_data, delimiter=delimiter)

    rows = list(csv_reader)

    # Initialize caches for related objects
    caches = {
        'projects': {},
        'subprojects': {},
        'assemblies': {},
        'locations': {},
        'sublocations': {},
        'drawers': {}
    }
    manufacturers = {}
    suppliers = {}

    with transaction.atomic():
        # Determine the relevant fields based on model_mappings
        manufacturer_field = None
        project_field = None
        subproject_field = None
        assembly_field = None
        location_field = None
        sublocation_field = None
        drawer_field = None

        # Identify fields for related models
        for header, mapping in model_mappings.items():
            model = mapping.get('model')
            field = mapping.get('field')
            if model == 'Manufacturer' and field == 'name':
                manufacturer_field = header
            elif model == 'Project' and field == 'name':
                project_field = header
            elif model == 'Subproject' and field == 'name':
                subproject_field = header
            elif model == 'Assembly' and field == 'name':
                assembly_field = header
            elif model == 'StorageLocation' and field == 'name':
                location_field = header
            elif model == 'StorageSublocation' and field == 'name':
                sublocation_field = header
            elif model == 'StorageDrawer' and field == 'name':
                drawer_field = header

        # Process model-specific data
        process_manufacturers(rows, manufacturer_field, manufacturers, request)
        process_projects(rows, project_field, caches, request)
        process_subprojects(rows, subproject_field, project_field, caches, request)
        process_assemblies(rows, assembly_field, caches, request)
        process_storage_locations(rows, location_field, caches, request)
        process_storage_sublocations(rows, sublocation_field, location_field, caches, request)
        process_storage_drawers(rows, drawer_field, sublocation_field, location_field, caches, request)

        # Import articles and items
        import_articles_and_items(rows, model_mappings, caches, manufacturers, unit, request)
