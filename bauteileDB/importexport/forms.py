from django import forms
from django.core.exceptions import ObjectDoesNotExist
from core.models.component_models import Unit

class CSVUploadForm(forms.Form):
    csv_file = forms.FileField(
        label='Upload CSV',
        help_text='Select a CSV file to upload.'
    )
    delimiter = forms.CharField(
        max_length=1,
        required=False,
        initial=';',
        label='Delimiter',
        help_text='Specify the delimiter used in the CSV file (default is a Semicolon).'
    )
    
    # Unit of Measurement Dropdown
    unit = forms.ModelChoiceField(
        queryset=Unit.objects.all(),
        required=False,
        empty_label="Select Unit of Measurement",
        label='Unit of Measurement',
        help_text='Select the unit of measurement for the articles.'
    )
    
    # Fields for user to input mappings
    name = forms.CharField(
        label='Name Column',
        max_length=100,
        required=False,
        help_text='The column in your CSV file that corresponds to the Article Name in the database.'
    )
    manufacturer = forms.CharField(
        label='Manufacturer Column',
        max_length=100,
        required=False
    )
    supplier = forms.CharField(
        label='Supplier Column',
        max_length=100,
        required=False
    )
    catalog_number = forms.CharField(
        label='Catalog Number Column',
        max_length=100,
        required=False
    )
    material_number = forms.CharField(
        label='Material Number Column',
        max_length=100,
        required=False
    )
    type_description = forms.CharField(
        label='Type Description Column',
        max_length=100,
        required=False
    )
    description = forms.CharField(
        label='Description Column',
        max_length=100,
        required=False
    )
    width = forms.CharField(
        label='Width Column',
        max_length=100,
        required=False
    )
    length = forms.CharField(
        label='Length Column',
        max_length=100,
        required=False
    )
    height = forms.CharField(
        label='Height Column',
        max_length=100,
        required=False
    )
    weight = forms.CharField(
        label='Weight Column',
        max_length=100,
        required=False
    )
    waz = forms.CharField(
        label='WAZ Column',
        max_length=100,
        required=False
    )
    location = forms.CharField(
        label='Location Column',
        max_length=100,
        required=False
    )
    sublocation = forms.CharField(
        label='Sublocation Column',
        max_length=100,
        required=False
    )
    drawer = forms.CharField(
        label='Drawer Column',
        max_length=100,
        required=False
    )
    
    def __init__(self, *args, **kwargs):
        super(CSVUploadForm, self).__init__(*args, **kwargs)
        try:
            default_unit = Unit.objects.get(unit_of_measurement='pcs')
            self.fields['unit'].initial = default_unit
        except ObjectDoesNotExist:
            self.fields['unit'].initial = None  # Or set to another default if preferred
