from django.urls import path
from .views import import_csv_view

app_name = 'importexport'

urlpatterns = [
    path('', import_csv_view, name='import_export_csv'),#the first is to the html and the others to the function from views
]