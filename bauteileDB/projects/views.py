from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from core.views.generic_views import GenericAddView, GenericUpdateView, GenericDeleteView, GenericTableView
from core.models.component_models import Item
from core.models.project_models import Assembly, Project, Subproject
from .forms import AssemblyFormHiddenSubproject, ProjectForm, SubprojectFormHiddenProject
from .tables import AssemblyItemsTable, AssemblyTable, ProjectTable, SubprojectTable
from .filters import AssemblyFilter, ProjectFilter, SubprojectFilter#, AssemblyItemsFilter


# Project views


class ProjectListView(GenericTableView):
    model = Project
    table_class = ProjectTable
    filterset_class = ProjectFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'project',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        # Add 'Add Project' button information
        context['add_button_url'] = reverse_lazy('projects:project_add')
        context['add_button_label'] = 'Add Project'
        
        
        return context


class ProjectAddView(GenericAddView):
    model = Project
    form_class = ProjectForm
    page_title = "Add New Project"

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:project_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:project_list')
    
    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(ProjectAddView, self).form_valid(form)


class ProjectUpdateView(GenericUpdateView):
    model = Project
    form_class = ProjectForm
    page_title = "Update Project"
    cancel_url_name = 'projects:project_list'
    success_url_name = 'projects:project_list'


class ProjectDeleteView(GenericDeleteView):
    model = Project
    success_url_name = 'projects:project_list'
    page_title = "Delete Project"


# Subproject views


class SubprojectListView(GenericTableView):
    model = Subproject
    table_class = SubprojectTable
    filterset_class = SubprojectFilter
    ordering = ['name']
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'subproject',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        project_id = self.kwargs.get('pk')
        project = get_object_or_404(Project, pk=project_id)
        context['project'] = project
        context['verbose_name_plural'] = project.name + " - Subprojects"
        context['add_button_url'] = reverse_lazy('projects:subproject_add', kwargs={'project_id': project_id})
        context['add_button_label'] = 'Add Subproject'
        return context

    def get_queryset(self):
        project_id = self.kwargs.get('pk')
        return Subproject.objects.filter(project_id=project_id).order_by('name')


class SubprojectAddView(GenericAddView):
    model = Subproject
    form_class = SubprojectFormHiddenProject
    page_title = "Add New Subproject"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        project_id = self.kwargs.get('project_id')
        project = get_object_or_404(Project, pk=project_id)
        kwargs['initial'] = {'project': project}
        kwargs['project'] = project
        return kwargs

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:subproject_list', kwargs={'pk': self.object.project.pk})

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:subproject_list', kwargs={'pk': self.kwargs.get('project_id')})

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(SubprojectAddView, self).form_valid(form)


class SubprojectUpdateView(GenericUpdateView):
    model = Subproject
    form_class = SubprojectFormHiddenProject
    page_title = "Update Subproject"
    cancel_url_name = 'projects:subproject_list'
    success_url_name = 'projects:subproject_list'

    def get_success_url(self):
        return reverse_lazy('projects:subproject_list', kwargs={'pk': self.object.project.pk})


class SubprojectDeleteView(GenericDeleteView):
    model = Subproject
    page_title = "Delete Subproject"
    success_url_name = 'projects:subproject_list'
    cancel_url_name = 'projects:subproject_list'
    
    def get_success_url(self):
        return reverse_lazy('projects:subproject_list', kwargs={'pk': self.object.project.pk})


# Assembly views


class AssemblyListView(GenericTableView):
    model = Assembly
    table_class = AssemblyTable
    filterset_class = AssemblyFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'assembly',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        # Get the subproject ID from the URL
        subproject_id = self.kwargs.get('subproject_id')
        subproject = get_object_or_404(Subproject, pk=subproject_id)
        context['subproject'] = subproject
        context['verbose_name_plural'] = f"{subproject.project.name} - {subproject.name} - Assemblies"

        next_url = self.request.path
        # Add the 'Add Assembly' button information
        context['add_button_url'] = reverse_lazy('projects:assembly_add', kwargs={'subproject_id': subproject_id})
        context['add_button_label'] = 'Add Assembly'

        return context

    def get_queryset(self):
        subproject_id = self.kwargs.get('subproject_id')
        return Assembly.objects.filter(subproject_id=subproject_id).order_by('name')


class AssemblyAddView(GenericAddView):
    model = Assembly
    form_class = AssemblyFormHiddenSubproject
    page_title = "Add New Assembly"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        subproject_id = self.kwargs.get('subproject_id')
        subproject = get_object_or_404(Subproject, pk=subproject_id)
        kwargs['initial'] = {'subproject': subproject}
        kwargs['subproject'] = subproject
        return kwargs
   
    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:subproject_list', kwargs={'pk': self.object.project.pk})

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('projects:subproject_list', kwargs={'pk': self.kwargs.get('subproject_id')})

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        project = form.save(commit=False)
        project.save(request=self.request)
        return super(AssemblyAddView, self).form_valid(form)
    

class AssemblyUpdateView(GenericUpdateView):
    model = Assembly
    form_class = AssemblyFormHiddenSubproject
    page_title = "Update Assembly"
    success_url_name = 'projects:assembly_list'


class AssemblyDeleteView(GenericDeleteView):
    model = Assembly
    page_title = "Delete Assembly"
    success_url_name = 'projects:assembly_list'
