from django.urls import path
from .views import *

app_name = 'projects'

urlpatterns = [
    #---------------Project URLs---------------
    path('', ProjectListView.as_view(), name='project_list'),
    path('add/', ProjectAddView.as_view(), name='project_add'),
    path('<int:pk>/update/', ProjectUpdateView.as_view(), name='project_update'),
    path('<int:pk>/delete/', ProjectDeleteView.as_view(), name='project_delete'),

    #---------------Subproject URLs---------------
    path('<int:pk>/subprojects/', SubprojectListView.as_view(), name='subproject_list'),
    path('<int:project_id>/subprojects/add/', SubprojectAddView.as_view(), name='subproject_add'),
    path('subprojects/<int:pk>/update/', SubprojectUpdateView.as_view(), name='subproject_update'),
    path('subprojects/<int:pk>/delete/', SubprojectDeleteView.as_view(), name='subproject_delete'),
    
    #---------------Assembly URLs---------------
    path('<int:subproject_id>/assemblies/', AssemblyListView.as_view(), name='assembly_list'),
    path('<int:subproject_id>/assemblies/add/', AssemblyAddView.as_view(), name='assembly_add'),
    path('<int:pk>/assemblies/update/', AssemblyUpdateView.as_view(), name='assembly_update'),
    path('<int:pk>/assemblies/delete/', AssemblyDeleteView.as_view(), name='assembly_delete'),
]
