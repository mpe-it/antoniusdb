from django.urls import reverse_lazy
from core.models.storage_models import StorageLocation, StorageSublocation
from core.models.project_models import Assembly, Project, Subproject
from django import forms


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'description', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }


class BaseSubprojectForm(forms.ModelForm):
    class Meta:
        model = Subproject
        fields = ['project', 'name', 'description', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Enter Subproject Name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter Subproject Description'}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

    def __init__(self, *args, **kwargs):
        project = kwargs.pop('project', None)
        super().__init__(*args, **kwargs)

        # If a project is passed, handle any project-specific logic here
        if project:
            # In this case, we hide the project field since it's set programmatically
            self.fields['project'].widget = forms.HiddenInput()

    def clean(self):
        cleaned_data = super().clean()
        # Add any project-related validation if needed
        return cleaned_data


class SubprojectFormHiddenProject(BaseSubprojectForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Always hide the project field
        self.fields['project'].widget = forms.HiddenInput()


class BaseAssemblyForm(forms.ModelForm):
    # Input fields for autocomplete
    storage_location_input = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageLocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for location...',
        })
    )
    
    # Actual model fields (hidden)
    storage_location = forms.ModelChoiceField(
        queryset=StorageLocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    storage_sublocation_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input depends-on StorageLocation_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageSublocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for sublocation...',
        })
    )

    storage_sublocation = forms.ModelChoiceField(
        queryset=StorageSublocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    class Meta:
        model = Assembly
        fields = ['subproject', 'name', 'storage_location_input', 'storage_location', 'storage_sublocation_input', 'storage_sublocation', 'supervisor', 'description', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Enter Assembly Name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter Assembly Description'}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

    def __init__(self, *args, **kwargs):
        subproject = kwargs.pop('subproject', None)
        super().__init__(*args, **kwargs)

        if subproject:
            self.fields['subproject'].widget = forms.HiddenInput()

    def clean(self):
        cleaned_data = super().clean()

        # Clean storage_location_input
        storage_location_input = cleaned_data.get('storage_location_input')
        storage_location = None
        if storage_location_input:
            try:
                storage_location = StorageLocation.objects.get(name=storage_location_input)
            except StorageLocation.DoesNotExist:
                self.add_error('storage_location_input', f"Storage Location '{storage_location_input}' does not exist.")
        else:
            self.add_error('storage_location_input', "This field is required.")

        # Clean storage_sublocation_input
        storage_sublocation_input = cleaned_data.get('storage_sublocation_input')
        storage_sublocation = None
        if storage_sublocation_input:
            if storage_location is None:
                self.add_error('storage_sublocation_input', "A sublocation cannot be assigned without a valid location.")
            else:
                try:
                    storage_sublocation = StorageSublocation.objects.get(
                        name=storage_sublocation_input,
                        location=storage_location
                    )
                except StorageSublocation.DoesNotExist:
                    self.add_error(
                        'storage_sublocation_input',
                        f"Sublocation '{storage_sublocation_input}' does not exist in location '{storage_location.name}'."
                    )

        # Store the model instances in the cleaned_data to assign them to the actual model fields
        cleaned_data['storage_location'] = storage_location
        cleaned_data['storage_sublocation'] = storage_sublocation

        return cleaned_data


class AssemblyFormHiddenSubproject(BaseAssemblyForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['subproject'].widget = forms.HiddenInput()
