from core.models.component_models import Item
from core.models.project_models import Assembly, Project, Subproject
from core.tables import BaseTable
import django_tables2 as tables


class ProjectTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='projects/project_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = Project
        fields = ['selection', 'name', 'description', 'actions']


class SubprojectTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='projects/subproject_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = Subproject
        fields = ['selection','name', 'description', 'actions']


class AssemblyTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='projects/assembly_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    class Meta(BaseTable.Meta):
        model = Assembly
        fields = ['selection','name', 'description', 'storage_location','storage_sublocation','supervisor', 'actions']


class AssemblyItemsTable(BaseTable):
    # Item fields
    serial_number = tables.Column()
    storage_location = tables.Column()
    storage_unit = tables.Column()
    assigned_project = tables.Column()
    assigned_subproject = tables.Column()

    # Article fields
    article_name = tables.Column(
        accessor='article.name', verbose_name='Article Name')
    article_manufacturer = tables.Column(
        accessor='article.manufacturer.name', verbose_name='Manufacturer')

    class Meta:
        model = Item
        template_name = 'django_tables2/bootstrap5.html'
        fields = ['selection','article_name', 'article_manufacturer', 'serial_number',
                  'storage_location', 'storage_unit', 'assigned_project', 'assigned_subproject']
