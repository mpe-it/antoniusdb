from core.models.component_models import Item
from core.models.vendor_models import Manufacturer
from core.models.project_models import Assembly, Project, Subproject
from core.filters import autocomplete_field

import django_filters
from django import forms
from django.db.models import Q


# Project Filters
class ProjectFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    name = autocomplete_field(
        'name',
        'Project',
        placeholder='Type to search project name...'
    )
    
    #Autocomplete field for the 'description' attribute of Project
    description = autocomplete_field(
        'description',
        'Project',
        placeholder='Type to search for a description...'
    )

    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = Project
        fields = ['name', 'description']


class SubprojectFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    name = autocomplete_field(
        'name',
        'Subproject',
        placeholder='Type to search subproject name...'
    )

    #Autocomplete field for the 'description' attribute of Subproject
    description = autocomplete_field(
        'description',
        'Subproject',
        placeholder='Type to search for a description...'
    )

    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = Subproject
        fields = ['name', 'description']


class AssemblyFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )

    name = autocomplete_field(
        'name',
        'Assembly',
        placeholder='Type to search assembly name...'
    )

    #Autocomplete field for the 'description' attribute of Assembly
    description = autocomplete_field(
        'description',
        'Assembly',
        placeholder='Type to search for a description...'
    )

    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value)
        )

    class Meta:
        model = Assembly
        fields = ['name', 'description']
