from core.models.tool_models import Tool
from core.models.vendor_models import Manufacturer
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.filters import autocomplete_field

import django_filters
from django import forms
from django.db.models import Q


class ToolFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_search',
        label='Search',
        widget=forms.TextInput(attrs={
            'class': 'form-control me-2',
            'placeholder': 'Search...',
        })
    )
    
    name = autocomplete_field(
        'name',
        'Tool',
        placeholder='Type to search for a tool...'
    )

    #Autocomplete field for the 'description' attribute of Tool
    description = autocomplete_field(
        'description',
        'Tool',
        placeholder='Type to search for a description...'
    )

    #Autocomplete field for the 'borrowed_by' attribute of Tool
    borrowed_by = autocomplete_field(
        'borrowed_by',
        'Tool',
        placeholder='Type to search for a user who has borrowed a Tool...'
    )
    manufacturer__name = autocomplete_field(
        'manufacturer__name',
        'Manufacturer',
        autocomplete_field_name='name',
        placeholder='Type to search for a Manufacturer...'
    )
    location__name = autocomplete_field(
        'location__name',
        'StorageLocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage location...'
    )
    sublocation__name = autocomplete_field(
        'sublocation__name',
        'StorageSublocation',
        autocomplete_field_name='name',
        placeholder='Type to search storage sublocation...',
        depends_on='StorageLocation_name'
    )
    drawer__name = autocomplete_field(
        'drawer__name',
        'StorageDrawer',
        autocomplete_field_name='name',
        placeholder='Type to search storage drawer...',
        depends_on='StorageSublocation_name'
    )

    def filter_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(description__icontains=value) |
            Q(borrowed_by__icontains=value) |
            Q(manufacturer__name__icontains=value) |
            Q(location__name__icontains=value) |
            Q(sublocation__name__icontains=value) |
            Q(drawer__name__icontains=value)
        )

    class Meta:
        model = Tool
        # Do not include 'search' in Meta.fields
        fields = ['name', 'description', 'borrowed_by', 'manufacturer__name', 'location__name', 'sublocation__name', 'drawer__name']