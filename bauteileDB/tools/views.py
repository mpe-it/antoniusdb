from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse_lazy
from core.views.generic_views import GenericAddView, GenericUpdateView, GenericTableView
from django.views import View
from core.models.tool_models import Tool
from .tables import ToolTable
from .filters import ToolFilter
from .forms import ToolForm, BorrowForm
from django.shortcuts import get_object_or_404
from log.utils import log_to_database
from django.contrib.auth.mixins import LoginRequiredMixin
import datetime


#Tool Views


class ToolListView(GenericTableView):
    model = Tool
    table_class = ToolTable
    filterset_class = ToolFilter
    ordering = ['name']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            bulk_actions_modal=[
                {
                    'name': 'Delete Selected',
                    'modal_template': 'modals/bulk_delete_modal.html',
                    'modal_js': 'js/bulk_delete.js',
                    'class': 'btn btn-danger',
                    'modal_target': '#bulkDeleteModal',
                    'bulk_delete_url': reverse_lazy('core:bulk_delete', kwargs={
                        'model_name': 'tool',
                        'app_name': 'core' 
                    })
                },
            ],
            **kwargs
        )
        context['add_button_url'] = reverse_lazy('tools:tool_add')
        context['add_button_label'] = 'Add Tool'
        return context


class ToolAddView(GenericAddView):
    model = Tool
    form_class = ToolForm
    page_title = "Add New Tool"

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('tools:tool_list')

    def get_cancel_url(self):
        next_url = self.request.GET.get('next')
        return next_url or reverse_lazy('tools:tool_list')

    def form_valid(self, form):
        # Pass the request to the save method to set updated_by
        tool = form.save(commit=False)
        tool.save(request=self.request)
        return super(ToolAddView, self).form_valid(form)


class ToolUpdateView(GenericUpdateView):
    model = Tool
    form_class = ToolForm
    page_title = "Update Tool"
    cancel_url_name = 'tools:tool_list'
    success_url_name = 'tools:tool_list'

    def get_form_kwargs(self):
        # Get the default form kwargs
        kwargs = super().get_form_kwargs()

        # Get the current article instance
        tool = self.get_object()

        # If this is an update (i.e., the article instance exists)
        if tool.pk:
            # Set initial value for suppliers field as a comma-separated string of supplier names
            supplier_names = ', '.join(tool.suppliers.values_list('name', flat=True))
            kwargs['initial'] = kwargs.get('initial', {})
            kwargs['initial']['suppliers'] = supplier_names

        if tool.manufacturer:
            kwargs['initial']['manufacturer_input'] = tool.manufacturer.name

        return kwargs


class BorrowToolView(GenericUpdateView):
    model = Tool
    form_class = BorrowForm
    page_title = "Borrow Tool"
    cancel_url_name = 'tools:tool_list'
    success_url_name = 'tools:tool_list'


class ReturnToolView(View):
    def get(self, request, pk):
        # Retrieve the Tool instance using the primary key (pk)
        tool = get_object_or_404(Tool, pk=pk)

        # Clear the borrowed_by and return_by fields
        tool.borrowed_by = None
        tool.return_by = None
        tool.save()

        user = request.user if request.user.is_authenticated else None
        date = datetime.date.today()
        print(date)
        log_message = f"{tool} returned by {user} on {date}"
        log_to_database('INFO', log_message, user=user)

        # Redirect to the tool list or another page after the return action
        return redirect('tools:tool_list') 