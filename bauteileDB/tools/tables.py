from core.models.tool_models import Tool
from core.tables import BaseTable
import django_tables2 as tables


class ToolTable(BaseTable):
    actions = tables.TemplateColumn(
        template_name='tool_actions_column.html',
        verbose_name='Actions',
        orderable=False
    )

    location = tables.Column(
        accessor='location.name',
        verbose_name='Location',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.location else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.location else None,
                   "title": lambda record: record.location.info if record.location else None}
        }
    )
    sublocation = tables.Column(
        accessor='sublocation.name',
        verbose_name='Sublocation',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.sublocation else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.sublocation else None,
                   "title": lambda record: record.sublocation.info if record.sublocation else None}
        }
    )
    drawer = tables.Column(
        accessor='drawer.name',
        verbose_name='Drawer',
        attrs={
            "td": {"class": lambda record: "has-tooltip" if record.drawer else "",
                   "data-bs-toggle": lambda record: "tooltip" if record.drawer else None,
                   "title": lambda record: record.drawer.info if record.drawer else None}
        }
    )

    class Meta(BaseTable.Meta):
        model = Tool
        fields = ['selection', 'name', 'description', 'manufacturer', 'location', 'sublocation', 'drawer', 'borrowed_by', 'return_by','actions']
        attrs = {
            'class': 'table table-bordered table-hover',
        }