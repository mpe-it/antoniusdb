from django.urls import path
from .views import *

app_name = 'tools'

urlpatterns = [
    #---------------Location URLs---------------
    path('', ToolListView.as_view(), name='tool_list'),
    path('add-tool/', ToolAddView.as_view(), name='tool_add'),
    path('<int:pk>/update/', ToolUpdateView.as_view(), name='tool_update'),
    path('<int:pk>/borrow/', BorrowToolView.as_view(), name='borrow_tool'),
    path('<int:pk>/return/', ReturnToolView.as_view(), name='return_tool'),
]