from django import forms
from django.urls import reverse_lazy
from core.models.vendor_models import Manufacturer, Supplier
from core.models.storage_models import StorageLocation, StorageSublocation, StorageDrawer
from core.models.tool_models import Tool
from core.forms import create_autocomplete_widget
from core.forms import get_model_instance_from_input

class ToolForm(forms.ModelForm):

    manufacturer_input = forms.CharField(
        required=False,
        widget=create_autocomplete_widget(
            model_name='Manufacturer',
            field_name='name',
            placeholder='Start typing to search for manufacturer...'
        )
    )

    manufacturer = forms.ModelChoiceField(
        queryset=Manufacturer.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    suppliers = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'autocomplete-input',
            'data-autocomplete-url': reverse_lazy(
                'core:generic-autocomplete',
                kwargs={'model_name': 'Supplier', 'field_name': 'name'}
            ),
            'placeholder': 'Start typing to search and separate suppliers by commas...'
        }),
        label="Suppliers (comma-separated)"
    )

    location_input = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageLocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for location...',
        })
    )

    location = forms.ModelChoiceField(
        queryset=StorageLocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )
    
    sublocation_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on StorageLocation_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageSublocation', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for sublocation...',
        })
    )
    
    sublocation = forms.ModelChoiceField(
        queryset=StorageSublocation.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    drawer_input = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete-input depends-on StorageSublocation_name',
            'data-autocomplete-url': reverse_lazy('core:generic-autocomplete', kwargs={'model_name': 'StorageDrawer', 'field_name': 'name'}),
            'placeholder': 'Start typing to search for drawer...',
        })
    )
    
    drawer = forms.ModelChoiceField(
        queryset=StorageDrawer.objects.all(),
        required=False,
        widget=forms.HiddenInput()
    )

    class Meta:
        model = Tool
        fields = ['name', 'description', 'manufacturer_input', 'manufacturer', 'suppliers', 'location_input', 'location', 'sublocation_input', 'sublocation', 'drawer_input', 'drawer', 'modified_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': ''}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter description', 'rows': 3}),
            'modified_by':forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }
    
    def clean(self):
        cleaned_data = super().clean()

        # Process the manufacturer_input and set manufacturer
        manufacturer_name = cleaned_data.get('manufacturer_input')
        manufacturer, error = get_model_instance_from_input(manufacturer_name, Manufacturer)
        if error:
            self.add_error('manufacturer_input', error)
        else:
            cleaned_data['manufacturer'] = manufacturer
               
        # Process the suppliers_input and set suppliers
        suppliers_input = self.data.get('suppliers')  # Raw input from the form
        if suppliers_input:
            supplier_names = [name.strip() for name in suppliers_input.split(',') if name.strip()]
            suppliers = Supplier.objects.filter(name__in=supplier_names)
            
            # Validate suppliers
            missing_suppliers = set(supplier_names) - set(suppliers.values_list('name', flat=True))
            if missing_suppliers:
                self.add_error('suppliers', f"These suppliers do not exist: {', '.join(missing_suppliers)}")
            else:
                cleaned_data['suppliers'] = suppliers 

        # Process storage_location_input and set storage_location
        location_name = cleaned_data.get('location_input')
        location, error = get_model_instance_from_input(location_name, StorageLocation)
        if error:
            self.add_error('location_input', error)
        else:
            cleaned_data['location'] = location

        # Process storage_sublocation_input and set storage_sublocation
        sublocation_name = cleaned_data.get('sublocation_input')
        sublocation, error = get_model_instance_from_input(
            sublocation_name, StorageSublocation, location, 'location'
        )
        if error:
            self.add_error('sublocation_input', error)
        else:
            cleaned_data['sublocation'] = sublocation

        # Process storage_drawer_input and set drawer
        drawer_name = cleaned_data.get('drawer_input')
        drawer, error = get_model_instance_from_input(
            drawer_name, StorageDrawer, sublocation, 'sublocation'
        )
        if error:
            self.add_error('drawer_input', error)
        else:
            cleaned_data['drawer'] = drawer

        # Dependency validations
        if sublocation and not location:
            self.add_error('sublocation_input', "A sublocation cannot be assigned without a valid location.")
        if drawer and not sublocation:
            self.add_error('drawer_input', "A drawer cannot be assigned without a valid sublocation.")

        # Ensure relationships match
        if sublocation and sublocation.location != location:
            self.add_error(
                'sublocation_input',
                f"The sublocation '{sublocation_name}' does not belong to the location '{location_name}'."
            )

        if drawer and drawer.sublocation != sublocation:
            self.add_error(
                'drawer_input',
                f"The drawer '{drawer_name}' does not belong to the sublocation '{sublocation_name}'."
            )

        return cleaned_data

    def __init__(self, *args, **kwargs):
        self.article = kwargs.pop('article', None)
        super().__init__(*args, **kwargs)

        # Initialize the autocomplete fields with instance data when editing
        if self.instance and self.instance.pk:
            if self.instance.location:
                self.fields['location_input'].initial = self.instance.location.name
            if self.instance.sublocation:
                self.fields['sublocation_input'].initial = self.instance.sublocation.name
            if self.instance.drawer:
                self.fields['drawer_input'].initial = self.instance.drawer.name

class BorrowForm(forms.ModelForm):
    class Meta:
        model = Tool
        fields = ['borrowed_by', 'return_by']
        widgets = {
            'borrowed_by': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Enter borrower's name"}),
            'return_by': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Select return date'}),
        }